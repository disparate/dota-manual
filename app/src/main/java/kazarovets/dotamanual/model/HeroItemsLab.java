package kazarovets.dotamanual.model;

import java.util.ArrayList;

import kazarovets.dotamanual.contracts.HeroContract;
import kazarovets.dotamanual.contracts.ItemContract;


public class HeroItemsLab {
    public static ArrayList<Item>[] getItemsForHero(int id){
        switch (id){
            case HeroContract.ALCHEMIC_ID:{
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.CIRCLET_OF_NOBILITY_ID, ItemContract.CIRCLET_OF_NOBILITY_ID},
                        new int[]{ItemContract.QUELLING_BLADE_ID,ItemContract.RING_OF_HEALTH_ID, ItemContract.STOUT_SHIELD_ID},
                        new int[]{ItemContract.VANGUARD_ID, ItemContract.VLADMIRS_OFFERING_ID, ItemContract.BOOTS_OF_SPEED_ID,
                                ItemContract.BRACER_ID, ItemContract.BRACER_ID},
                        new int[]{ItemContract.RADIANCE_ID, ItemContract.STYGIAN_DESOLATOR_ID,ItemContract.SANGE_AND_YASHA_ID,
                                ItemContract.BASHER_ID, ItemContract.ASSAULT_CUIRAS_ID, ItemContract.HEART_OF_TARRASQUE_ID,
                                ItemContract.SATANIC_ID, ItemContract.HEAVENS_HALLBERD_ID});
            }

            case HeroContract.BRISTLEBACK_ID:{
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                                ItemContract.CLARITY_POTION_ID, ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                                ItemContract.STOUT_SHIELD_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.RING_OF_HEALTH_ID,
                                ItemContract.RING_OF_AQUILA_ID, ItemContract.MAGIC_WAND_ID},
                        new int[]{ItemContract.POWER_THREADS_ID, ItemContract.VANGUARD_ID},
                        new int[]{ItemContract.BLADE_MAIL_ID, ItemContract.HEAVENS_HALLBERD_ID, ItemContract.ASSAULT_CUIRAS_ID,
                                ItemContract.KHADGARS_PIPE_OF_INSIGHT_ID, ItemContract.SHIVAS_GUARD_ID,
                                ItemContract.HEART_OF_TARRASQUE_ID, ItemContract.RADIANCE_ID}
                        );
            }

            case HeroContract.BREWMASTER_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                                ItemContract.STOUT_SHIELD_ID, ItemContract.IRONWOOD_BRANCH_ID,
                                ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_STICK_ID, ItemContract.BRACER_ID},
                        new int[]{ItemContract.PHASE_BOOTS_ID, ItemContract.MAGIC_WAND_ID, ItemContract.ANCIENT_JANGGO_OF_ENDURANCE_ID,
                                ItemContract.KELENS_DAGGER_OF_ESCAPE_ID},
                        new int[]{ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.ASSAULT_CUIRAS_ID, ItemContract.SHIVAS_GUARD_ID,
                                ItemContract.HEART_OF_TARRASQUE_ID, ItemContract.RADIANCE_ID, ItemContract.VLADMIRS_OFFERING_ID});
            }

            case HeroContract.WISP_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID, ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID,
                                ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                                ItemContract.CLARITY_POTION_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_STICK_ID, ItemContract.URN_OF_SHADOWS_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.MEKANSM_ID, ItemContract.ARCANE_BOOTS_ID},
                        new int[]{ItemContract.HEART_OF_TARRASQUE_ID, ItemContract.SHIVAS_GUARD_ID, ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID,
                                ItemContract.HEAVENS_HALLBERD_ID, ItemContract.NECROMICON_ID, ItemContract.ANCIENT_JANGGO_OF_ENDURANCE_ID,
                                ItemContract.DAGON_ID});
            }

            case HeroContract.TREANT_PROTECTOR_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.STOUT_SHIELD_ID, ItemContract.CLARITY_POTION_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_STICK_ID, ItemContract.BRACER_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.ARCANE_BOOTS_ID, ItemContract.ANCIENT_JANGGO_OF_ENDURANCE_ID},
                        new int[]{ItemContract.REFRESHER_ORB_ID, ItemContract.KELENS_DAGGER_OF_ESCAPE_ID, ItemContract.HEART_OF_TARRASQUE_ID,
                            ItemContract.SHIVAS_GUARD_ID, ItemContract.ASSAULT_CUIRAS_ID, ItemContract.KHADGARS_PIPE_OF_INSIGHT_ID,
                            ItemContract.HEAVENS_HALLBERD_ID, ItemContract.RADIANCE_ID, ItemContract.NECROMICON_ID});
            }

            case HeroContract.DRAGON_KNIGHT_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID, ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID,
                            ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_STICK_ID, ItemContract.BRACER_ID},
                        new int[]{ItemContract.POWER_THREADS_ID, ItemContract.MAGIC_WAND_ID, ItemContract.HELM_OF_THE_DOMINATOR_ID},
                        new int[]{ItemContract.BLACK_KING_BAR_ID, ItemContract.ABYSSAL_BLADE_ID, ItemContract.HEART_OF_TARRASQUE_ID,
                            ItemContract.ASSAULT_CUIRAS_ID, ItemContract.SATANIC_ID, ItemContract.HEAVENS_HALLBERD_ID,
                            ItemContract.BURIZE_ID, ItemContract.SANGE_AND_YASHA_ID}
                );
            }

            case HeroContract.CAOLIN_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.CIRCLET_OF_NOBILITY_ID, ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID,
                            ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID, ItemContract.CLARITY_POTION_ID},
                        new int[]{ItemContract.BRACER_ID, ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.POWER_THREADS_ID, ItemContract.PERSEVERANCE_ID,
                            ItemContract.HELM_OF_THE_DOMINATOR_ID, ItemContract.BLACK_KING_BAR_ID},
                        new int[]{ItemContract.BLOODSTONE_ID, ItemContract.BATTLE_FURY_ID, ItemContract.ASSAULT_CUIRAS_ID,
                            ItemContract.SATANIC_ID, ItemContract.HEART_OF_TARRASQUE_ID}
                );
            }

            case HeroContract.CENTAUR_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.HEALING_SALVE_ID, ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.RING_OF_PROTECTION_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_STICK_ID},
                        new int[]{ItemContract.TRANQUIL_BOOTS_ID, ItemContract.HOOD_OF_DEFIANCE_ID, ItemContract.KELENS_DAGGER_OF_ESCAPE_ID,
                            ItemContract.MAGIC_WAND_ID, ItemContract.BLADE_MAIL_ID},
                        new int[]{ItemContract.KHADGARS_PIPE_OF_INSIGHT_ID, ItemContract.HEART_OF_TARRASQUE_ID,
                            ItemContract.RADIANCE_ID, ItemContract.BOOTS_OF_TRAVEL_ID}
                );
            }

            case HeroContract.CLOCKWERK_ID: {
                return getItemsFromIds(
                        new int[]{ ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.STOUT_SHIELD_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ ItemContract.BOOTS_OF_SPEED_ID, ItemContract.BRACER_ID, ItemContract.EMPTY_BOTTLE_ID},
                        new int[]{ ItemContract.PHASE_BOOTS_ID, ItemContract.MAGIC_WAND_ID, ItemContract.BLADE_MAIL_ID},
                        new int[]{ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.ANCIENT_JANGGO_OF_ENDURANCE_ID,
                            ItemContract.KHADGARS_PIPE_OF_INSIGHT_ID, ItemContract.HEART_OF_TARRASQUE_ID,
                            ItemContract.RADIANCE_ID, ItemContract.SHIVAS_GUARD_ID}
                );
            }

            case HeroContract.CUNKA_ID: {
                return getItemsFromIds(
                        new int[] {ItemContract.CLARITY_POTION_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.HEALING_SALVE_ID, ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID,
                            ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[] {ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_STICK_ID,
                            ItemContract.BRACER_ID, ItemContract.RING_OF_HEALTH_ID},
                        new int[] {ItemContract.PHASE_BOOTS_ID, ItemContract.MAGIC_WAND_ID,
                            ItemContract.VANGUARD_ID, ItemContract.LOTHARS_EDGE_ID},
                        new int[] {ItemContract.BLACK_KING_BAR_ID, ItemContract.BURIZE_ID, ItemContract.ASSAULT_CUIRAS_ID,
                            ItemContract.HEART_OF_TARRASQUE_ID, ItemContract.BATTLE_FURY_ID}
                );
            }

            case HeroContract.LEGION_COMMANDER_ID: {
                return getItemsFromIds(
                        new int[] {ItemContract.SOUL_RING_ID, ItemContract.QUELLING_BLADE_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[] {ItemContract.BOOTS_OF_SPEED_ID, ItemContract.RING_OF_BASILIUS_ID},
                        new int[] {ItemContract.PHASE_BOOTS_ID, ItemContract.CRYSTALYS_ID, ItemContract.KELENS_DAGGER_OF_ESCAPE_ID},
                        new int[] {ItemContract.BURIZE_ID, ItemContract.STYGIAN_DESOLATOR_ID, ItemContract.HEAVENS_HALLBERD_ID,
                            ItemContract.ABYSSAL_BLADE_ID, ItemContract.HEART_OF_TARRASQUE_ID, ItemContract.ASSAULT_CUIRAS_ID,
                            ItemContract.SATANIC_ID, ItemContract.BLADE_MAIL_ID}
                );
            }

            case HeroContract.OMNIKNIGHT_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.CLARITY_POTION_ID, ItemContract.CLARITY_POTION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.RING_OF_PROTECTION_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_STICK_ID, ItemContract.RING_OF_BASILIUS_ID},
                        new int[]{ItemContract.SOUL_RING_ID, ItemContract.ARCANE_BOOTS_ID, ItemContract.MAGIC_WAND_ID,
                            ItemContract.OBSERVER_WARDS_ID, ItemContract.SCROLL_OF_TOWN_PORTAL_ID},
                        new int[]{ItemContract.MEKANSM_ID, ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID, ItemContract.SHIVAS_GUARD_ID,
                            ItemContract.BOOTS_OF_TRAVEL_ID, ItemContract.VLADMIRS_OFFERING_ID, ItemContract.ANCIENT_JANGGO_OF_ENDURANCE_ID,
                            ItemContract.SANGE_AND_YASHA_ID, ItemContract.HEART_OF_TARRASQUE_ID, ItemContract.REFRESHER_ORB_ID,
                            ItemContract.RADIANCE_ID}
                );
            }

            case HeroContract.REZAK_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.STOUT_SHIELD_ID, ItemContract.CLARITY_POTION_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.HEALING_SALVE_ID, ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.ARCANE_BOOTS_ID, ItemContract.RING_OF_HEALTH_ID, ItemContract.MAGIC_WAND_ID},
                        new int[]{ItemContract.VANGUARD_ID, ItemContract.SHIVAS_GUARD_ID},
                        new int[]{ItemContract.KHADGARS_PIPE_OF_INSIGHT_ID, ItemContract.BLOODSTONE_ID, ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID,
                            ItemContract.HEART_OF_TARRASQUE_ID, ItemContract.BLADE_MAIL_ID}
                );
            }

            case HeroContract.REXAR_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID,
                            ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID, ItemContract.BRACER_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.POWER_THREADS_ID, ItemContract.ANCIENT_JANGGO_OF_ENDURANCE_ID},
                        new int[]{ItemContract.KHADGARS_PIPE_OF_INSIGHT_ID, ItemContract.NECROMICON_ID,
                            ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.KELENS_DAGGER_OF_ESCAPE_ID,
                            ItemContract.HEART_OF_TARRASQUE_ID, ItemContract.ASSAULT_CUIRAS_ID}
                );
            }

            case HeroContract.SVEN_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.CLARITY_POTION_ID,
                                ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.EMPTY_BOTTLE_ID, ItemContract.BRACER_ID, ItemContract.BOOTS_OF_SPEED_ID},
                        new int[]{ItemContract.POWER_THREADS_ID, ItemContract.SCROLL_OF_TOWN_PORTAL_ID, ItemContract.KELENS_DAGGER_OF_ESCAPE_ID},
                        new int[]{ItemContract.BLACK_KING_BAR_ID, ItemContract.BURIZE_ID, ItemContract.HEART_OF_TARRASQUE_ID,
                            ItemContract.SATANIC_ID, ItemContract.ASSAULT_CUIRAS_ID, ItemContract.MASK_OF_MADNESS_ID,
                            ItemContract.BOOTS_OF_TRAVEL_ID}
                );
            }

            case HeroContract.TAUREN_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID, ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_STICK_ID, ItemContract.URN_OF_SHADOWS_ID},
                        new int[]{ItemContract.POWER_THREADS_ID, ItemContract.MAGIC_WAND_ID, ItemContract.HOOD_OF_DEFIANCE_ID},
                        new int[]{ItemContract.KHADGARS_PIPE_OF_INSIGHT_ID, ItemContract.ASSAULT_CUIRAS_ID, ItemContract.SHIVAS_GUARD_ID,
                            ItemContract.BLADE_MAIL_ID, ItemContract.ANCIENT_JANGGO_OF_ENDURANCE_ID, ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID}
                );
            }

            case HeroContract.TINI_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                                ItemContract.MANTLE_OF_INTELLIGENCE_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID},
                        new int[]{ItemContract.EMPTY_BOTTLE_ID, ItemContract.BOOTS_OF_SPEED_ID},
                        new int[]{ItemContract.PHASE_BOOTS_ID, ItemContract.KELENS_DAGGER_OF_ESCAPE_ID},
                        new int[]{ItemContract.BLACK_KING_BAR_ID, ItemContract.KHADGARS_PIPE_OF_INSIGHT_ID, ItemContract.GHOST_SCEPTER_ID,
                            ItemContract.SHIVAS_GUARD_ID, ItemContract.BLADE_MAIL_ID, ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID,
                            ItemContract.DAGON_ID}
                );
            }

            case HeroContract.TUSKAR_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.CIRCLET_OF_NOBILITY_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_WAND_ID, ItemContract.BRACER_ID,
                            ItemContract.EMPTY_BOTTLE_ID},
                        new int[]{ItemContract.PHASE_BOOTS_ID, ItemContract.ANCIENT_JANGGO_OF_ENDURANCE_ID,
                            ItemContract.MEDALLION_OF_COURAGE_ID},
                        new int[]{ItemContract.ORB_OF_VENOM_ID, ItemContract.STYGIAN_DESOLATOR_ID, ItemContract.BATTLE_FURY_ID,
                            ItemContract.LOTHARS_EDGE_ID, ItemContract.HEART_OF_TARRASQUE_ID}
                );
            }

            case HeroContract.PHOENIX_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID, ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID,
                            ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_STICK_ID, ItemContract.URN_OF_SHADOWS_ID},
                        new int[]{ItemContract.TRANQUIL_BOOTS_ID, ItemContract.MAGIC_WAND_ID, ItemContract.REFRESHER_ORB_ID},
                        new int[]{ItemContract.NECROMICON_ID, ItemContract.SHIVAS_GUARD_ID,
                            ItemContract.HEART_OF_TARRASQUE_ID, ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID}
                );
            }

            case HeroContract.HUSCKAR_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.HEALING_SALVE_ID, ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID, ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_STICK_ID},
                        new int[]{ItemContract.POWER_THREADS_ID, ItemContract.MAGIC_WAND_ID,
                                ItemContract.ARMLET_OF_MORDIGGIAN_ID, ItemContract.SCROLL_OF_TOWN_PORTAL_ID},
                        new int[]{ItemContract.BLACK_KING_BAR_ID, ItemContract.HEART_OF_TARRASQUE_ID, ItemContract.KHADGARS_PIPE_OF_INSIGHT_ID,
                            ItemContract.ASSAULT_CUIRAS_ID, ItemContract.SATANIC_ID, ItemContract.AGHANIMS_SCEPTER_ID,
                            ItemContract.LOTHARS_EDGE_ID}
                );
            }

            case HeroContract.SHEIKER_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.IRONWOOD_BRANCH_ID, ItemContract.HEALING_SALVE_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                                ItemContract.CLARITY_POTION_ID, ItemContract.CLARITY_POTION_ID, ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID,
                                ItemContract.BRACER_ID, ItemContract.ENERGY_BOOSTER_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.ARCANE_BOOTS_ID, ItemContract.KELENS_DAGGER_OF_ESCAPE_ID},
                        new int[]{ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.VEIL_OF_DISCORD_ID, ItemContract.SHIVAS_GUARD_ID,
                                ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID, ItemContract.HEART_OF_TARRASQUE_ID}
                );
            }

            case HeroContract.BOUNTY_HUNTER_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID, ItemContract.STOUT_SHIELD_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.POOR_MANS_SHIELD_ID, ItemContract.MAGIC_STICK_ID},
                        new int[]{ItemContract.PHASE_BOOTS_ID, ItemContract.MAGIC_WAND_ID, ItemContract.BATTLE_FURY_ID},
                        new int[]{ItemContract.MEDALLION_OF_COURAGE_ID, ItemContract.STYGIAN_DESOLATOR_ID, ItemContract.MONKEY_KING_BAR_ID,
                            ItemContract.VLADMIRS_OFFERING_ID, ItemContract.VLADMIRS_OFFERING_ID, ItemContract.ASSAULT_CUIRAS_ID, ItemContract.DIFFUSAL_BLADE_ID,
                            ItemContract.LINKENS_SPHERE_ID, ItemContract.ORCHID_MALEVOLENCE_ID, ItemContract.SANGE_AND_YASHA_ID,
                            ItemContract.BLACK_KING_BAR_ID, ItemContract.URN_OF_SHADOWS_ID, ItemContract.ANCIENT_JANGGO_OF_ENDURANCE_ID}
                );
            }

            case HeroContract.VENGEFUL_SPIRIT_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.SLIPPERS_OF_AGILITY_ID, ItemContract.SLIPPERS_OF_AGILITY_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.HEALING_SALVE_ID, ItemContract.CLARITY_POTION_ID, ItemContract.CLARITY_POTION_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID, ItemContract.RING_OF_AQUILA_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.POWER_THREADS_ID, ItemContract.MEKANSM_ID},
                        new int[]{ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.FORCE_STAFF_ID, ItemContract.STYGIAN_DESOLATOR_ID,
                            ItemContract.BUTTERFLY_ID, ItemContract.MANTA_STYLE_ID, ItemContract.HEART_OF_TARRASQUE_ID}
                );
            }

            case HeroContract.GHYROCOPTER_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.SLIPPERS_OF_AGILITY_ID, ItemContract.SLIPPERS_OF_AGILITY_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.HEALING_SALVE_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID},
                        new int[]{ItemContract.RING_OF_BASILIUS_ID, ItemContract.BOOTS_OF_SPEED_ID,
                            ItemContract.WRAITH_BAND_ID, ItemContract.WRAITH_BAND_ID, ItemContract.MAGIC_STICK_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.POWER_THREADS_ID, ItemContract.VANGUARD_ID},
                        new int[]{ItemContract.MONKEY_KING_BAR_ID, ItemContract.BUTTERFLY_ID, ItemContract.BOOTS_OF_TRAVEL_ID,
                            ItemContract.BLACK_KING_BAR_ID, ItemContract.ASSAULT_CUIRAS_ID, ItemContract.BURIZE_ID, ItemContract.SATANIC_ID}
                );
            }

            case HeroContract.JAGGERNAUT_ID:{
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.SLIPPERS_OF_AGILITY_ID, ItemContract.SLIPPERS_OF_AGILITY_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.POOR_MANS_SHIELD_ID,
                            ItemContract.MAGIC_STICK_ID, ItemContract.RING_OF_HEALTH_ID},
                        new int[]{ItemContract.PHASE_BOOTS_ID, ItemContract.MAGIC_WAND_ID, ItemContract.BATTLE_FURY_ID},
                        new int[]{ItemContract.SANGE_AND_YASHA_ID, ItemContract.BUTTERFLY_ID, ItemContract.STYGIAN_DESOLATOR_ID,
                            ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.VLADMIRS_OFFERING_ID, ItemContract.BLACK_KING_BAR_ID}
                );
            }

            case HeroContract.EMBER_SPIRIT_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.SLIPPERS_OF_AGILITY_ID, ItemContract.SLIPPERS_OF_AGILITY_ID,
                            ItemContract.HEALING_SALVE_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_STICK_ID},
                        new int[]{ItemContract.PHASE_BOOTS_ID, ItemContract.MAGIC_WAND_ID, ItemContract.VANGUARD_ID},
                        new int[]{ItemContract.BATTLE_FURY_ID, ItemContract.DIFFUSAL_BLADE_ID, ItemContract.SANGE_AND_YASHA_ID,
                            ItemContract.SATANIC_ID, ItemContract.MJOLNIR_ID, ItemContract.RADIANCE_ID, ItemContract.BURIZE_ID,
                            ItemContract.BLADE_MAIL_ID, ItemContract.VLADMIRS_OFFERING_ID, ItemContract.MANTA_STYLE_ID,
                            ItemContract.KELENS_DAGGER_OF_ESCAPE_ID, ItemContract.MONKEY_KING_BAR_ID, ItemContract.BUTTERFLY_ID,
                            ItemContract.ASSAULT_CUIRAS_ID}
                );
            }

            case HeroContract.LANAYA_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.HEALING_SALVE_ID, ItemContract.CLARITY_POTION_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.EMPTY_BOTTLE_ID, ItemContract.MAGIC_STICK_ID},
                        new int[]{ItemContract.POWER_THREADS_ID, ItemContract.MAGIC_WAND_ID, ItemContract.KELENS_DAGGER_OF_ESCAPE_ID,
                            ItemContract.STYGIAN_DESOLATOR_ID},
                        new int[]{ItemContract.BLACK_KING_BAR_ID, ItemContract.ASSAULT_CUIRAS_ID, ItemContract.HEART_OF_TARRASQUE_ID,
                            ItemContract.BUTTERFLY_ID, ItemContract.MANTA_STYLE_ID, ItemContract.BURIZE_ID}
                );
            }

            case HeroContract.SYLLABEAR_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.STOUT_SHIELD_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID},
                        new int[]{ItemContract.QUELLING_BLADE_ID, ItemContract.ORB_OF_VENOM_ID, ItemContract.BOOTS_OF_SPEED_ID,
                            ItemContract.BOOTS_OF_SPEED_ID, ItemContract.RING_OF_PROTECTION_ID},
                        new int[]{ItemContract.PHASE_BOOTS_ID, ItemContract.TRANQUIL_BOOTS_ID, ItemContract.MAGIC_WAND_ID},
                        new int[]{ItemContract.VANGUARD_ID, ItemContract.RADIANCE_ID, ItemContract.VLADMIRS_OFFERING_ID,
                                ItemContract.BOOTS_OF_TRAVEL_ID, ItemContract.MJOLNIR_ID, ItemContract.STYGIAN_DESOLATOR_ID,
                                ItemContract.MONKEY_KING_BAR_ID, ItemContract.BURIZE_ID, ItemContract.ABYSSAL_BLADE_ID,
                                ItemContract.HEART_OF_TARRASQUE_ID, ItemContract.BUTTERFLY_ID}
                );
            }

            case HeroContract.LUNA_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID, ItemContract.POOR_MANS_SHIELD_ID,
                            ItemContract.RING_OF_BASILIUS_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.RING_OF_AQUILA_ID, ItemContract.POWER_THREADS_ID,
                            ItemContract.HELM_OF_THE_DOMINATOR_ID, ItemContract.BLACK_KING_BAR_ID, ItemContract.SCROLL_OF_TOWN_PORTAL_ID},
                        new int[]{ItemContract.FORCE_STAFF_ID, ItemContract.HEART_OF_TARRASQUE_ID, ItemContract.LOTHARS_EDGE_ID,
                            ItemContract.SATANIC_ID, ItemContract.MANTA_STYLE_ID, ItemContract.BUTTERFLY_ID, ItemContract.AGHANIMS_SCEPTER_ID}
                );
            }

            case HeroContract.MAGINA_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID, ItemContract.STOUT_SHIELD_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_STICK_ID, ItemContract.VANGUARD_ID},
                        new int[]{ItemContract.POWER_THREADS_ID, ItemContract.MAGIC_WAND_ID, ItemContract.YASHA_ID, ItemContract.SCROLL_OF_TOWN_PORTAL_ID},
                        new int[]{ItemContract.MANTA_STYLE_ID, ItemContract.BASHER_ID, ItemContract.BUTTERFLY_ID,
                                ItemContract.HEART_OF_TARRASQUE_ID, ItemContract.VLADMIRS_OFFERING_ID}
                );
            }

            case HeroContract.MIRANA_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.SLIPPERS_OF_AGILITY_ID, ItemContract.SLIPPERS_OF_AGILITY_ID,
                            ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.EMPTY_BOTTLE_ID, ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_STICK_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.POWER_THREADS_ID, ItemContract.MANTA_STYLE_ID},
                        new int[]{ItemContract.DIFFUSAL_BLADE_ID, ItemContract.BUTTERFLY_ID, ItemContract.STYGIAN_DESOLATOR_ID,
                            ItemContract.MONKEY_KING_BAR_ID}
                );
            }

            case HeroContract.MORPHLING_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.SLIPPERS_OF_AGILITY_ID, ItemContract.SLIPPERS_OF_AGILITY_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.EMPTY_BOTTLE_ID, ItemContract.BOOTS_OF_SPEED_ID,
                            ItemContract.WRAITH_BAND_ID, ItemContract.WRAITH_BAND_ID},
                        new int[]{ItemContract.RING_OF_BASILIUS_ID, ItemContract.SCROLL_OF_TOWN_PORTAL_ID, ItemContract.MANTA_STYLE_ID},
                        new int[]{ItemContract.LINKENS_SPHERE_ID, ItemContract.EHTHEREAL_BLADE_ID, ItemContract.SANGE_AND_YASHA_ID,
                            ItemContract.DIFFUSAL_BLADE_ID}
                );
            }

            case HeroContract.NAGA_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.STOUT_SHIELD_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.HEALING_SALVE_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.RING_OF_AQUILA_ID, ItemContract.MAGIC_STICK_ID},
                        new int[]{ItemContract.POWER_THREADS_ID, ItemContract.VANGUARD_ID,
                            ItemContract.MAGIC_WAND_ID, ItemContract.DIFFUSAL_BLADE_ID},
                        new int[]{ItemContract.HEART_OF_TARRASQUE_ID, ItemContract.BUTTERFLY_ID, ItemContract.BURIZE_ID,
                            ItemContract.MANTA_STYLE_ID, ItemContract.ASSAULT_CUIRAS_ID}
                );
            }

            case HeroContract.RIKIMARU_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.HEALING_SALVE_ID, ItemContract.SLIPPERS_OF_AGILITY_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID, ItemContract.POOR_MANS_SHIELD_ID},
                        new int[]{ItemContract.POWER_THREADS_ID, ItemContract.MAGIC_WAND_ID,
                            ItemContract.DIFFUSAL_BLADE_ID, ItemContract.SCROLL_OF_TOWN_PORTAL_ID},
                        new int[]{ItemContract.BUTTERFLY_ID, ItemContract.VLADMIRS_OFFERING_ID, ItemContract.BLACK_KING_BAR_ID,
                            ItemContract.ASSAULT_CUIRAS_ID, ItemContract.SANGE_AND_YASHA_ID}
                );
            }

            case HeroContract.SNIPER_ID: {
                return getItemsFromIds(
                        new int[]{ ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                                ItemContract.HEALING_SALVE_ID, ItemContract.SLIPPERS_OF_AGILITY_ID,
                                ItemContract.SLIPPERS_OF_AGILITY_ID},
                        new int[]{ ItemContract.BOOTS_OF_SPEED_ID, ItemContract.WRAITH_BAND_ID},
                        new int[]{ItemContract.POWER_THREADS_ID, ItemContract.LOTHARS_EDGE_ID},
                        new int[]{ItemContract.STYGIAN_DESOLATOR_ID, ItemContract.BURIZE_ID, ItemContract.HEART_OF_TARRASQUE_ID,
                                ItemContract.SATANIC_ID, ItemContract.MANTA_STYLE_ID, ItemContract.EHTHEREAL_BLADE_ID,
                                ItemContract.DIFFUSAL_BLADE_ID, ItemContract.DIVINE_RAPIRE_ID}
                );
            }

            case HeroContract.TRAKSES_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.HEALING_SALVE_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.SLIPPERS_OF_AGILITY_ID, ItemContract.SLIPPERS_OF_AGILITY_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_STICK_ID,
                            ItemContract.WRAITH_BAND_ID, ItemContract.WRAITH_BAND_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.POWER_THREADS_ID, ItemContract.MANTA_STYLE_ID},
                        new int[]{ItemContract.LOTHARS_EDGE_ID, ItemContract.BUTTERFLY_ID, ItemContract.BURIZE_ID,
                            ItemContract.BLACK_KING_BAR_ID, ItemContract.SATANIC_ID}
                );
            }

            case HeroContract.TROLL_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.RING_OF_PROTECTION_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID, ItemContract.RING_OF_AQUILA_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.PHASE_BOOTS_ID,
                            ItemContract.HELM_OF_THE_DOMINATOR_ID, ItemContract.BLACK_KING_BAR_ID},
                        new int[]{ItemContract.BURIZE_ID, ItemContract.BUTTERFLY_ID, ItemContract.SATANIC_ID,
                            ItemContract.MONKEY_KING_BAR_ID, ItemContract.MANTA_STYLE_ID, ItemContract.LOTHARS_EDGE_ID,
                            ItemContract.SANGE_AND_YASHA_ID}
                );
            }

            case HeroContract.URSA_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.STOUT_SHIELD_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID, ItemContract.HEALING_SALVE_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.VLADMIRS_OFFERING_ID},
                        new int[]{ItemContract.PHASE_BOOTS_ID, ItemContract.VANGUARD_ID, ItemContract.KELENS_DAGGER_OF_ESCAPE_ID},
                        new int[]{ItemContract.BLACK_KING_BAR_ID, ItemContract.BUTTERFLY_ID, ItemContract.HEART_OF_TARRASQUE_ID,
                            ItemContract.ARMLET_OF_MORDIGGIAN_ID, ItemContract.ABYSSAL_BLADE_ID, ItemContract.HEAVENS_HALLBERD_ID,
                            ItemContract.MONKEY_KING_BAR_ID, ItemContract.BURIZE_ID, ItemContract.BATTLE_FURY_ID, ItemContract.DIVINE_RAPIRE_ID}
                );
            }

            case HeroContract.PHANTOM_LANCER_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.HEALING_SALVE_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.QUELLING_BLADE_ID, ItemContract.EMPTY_BOTTLE_ID, ItemContract.RING_OF_BASILIUS_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.VANGUARD_ID, ItemContract.POWER_THREADS_ID},
                        new int[]{ItemContract.RADIANCE_ID, ItemContract.DIFFUSAL_BLADE_ID, ItemContract.BUTTERFLY_ID, ItemContract.HEART_OF_TARRASQUE_ID}
                );
            }

            case HeroContract.WINDRUNNER_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.HEALING_SALVE_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_STICK_ID, ItemContract.EMPTY_BOTTLE_ID},
                        new int[]{ItemContract.PHASE_BOOTS_ID, ItemContract.MAGIC_WAND_ID},
                        new int[]{ItemContract.STYGIAN_DESOLATOR_ID, ItemContract.MONKEY_KING_BAR_ID, ItemContract.BURIZE_ID,
                            ItemContract.MJOLNIR_ID, ItemContract.AGHANIMS_SCEPTER_ID}
                );
            }

            case HeroContract.JAKIRO_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANIMAL_COURIER_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_STICK_ID, ItemContract.RING_OF_BASILIUS_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.MEKANSM_ID, ItemContract.ARCANE_BOOTS_ID,
                            ItemContract.SENTRY_WARDS_ID},
                        new int[]{ItemContract.VEIL_OF_DISCORD_ID, ItemContract.SHIVAS_GUARD_ID, ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID,
                            ItemContract.KHADGARS_PIPE_OF_INSIGHT_ID, ItemContract.EULS_SCEPTER_OF_DIVINITY_ID, ItemContract.AGHANIMS_SCEPTER_ID,
                            ItemContract.KELENS_DAGGER_OF_ESCAPE_ID, ItemContract.FORCE_STAFF_ID, ItemContract.NECROMICON_ID}
                );
            }

            case HeroContract.DRAGONUS_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.CLARITY_POTION_ID, ItemContract.HEALING_SALVE_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_STICK_ID, ItemContract.VOID_STONE_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.BLOODSTONE_ID, ItemContract.FORCE_STAFF_ID, ItemContract.ROD_OF_ATOS_ID},
                        new int[]{ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.EULS_SCEPTER_OF_DIVINITY_ID, ItemContract.VEIL_OF_DISCORD_ID,
                            ItemContract.EHTHEREAL_BLADE_ID, ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID, ItemContract.DAGON_ID}
                );
            }

            case HeroContract.ZEUS_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.CLARITY_POTION_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.EMPTY_BOTTLE_ID, ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_WAND_ID},
                        new int[]{ItemContract.ARCANE_BOOTS_ID, ItemContract.VEIL_OF_DISCORD_ID},
                        new int[]{ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.REFRESHER_ORB_ID,
                            ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID, ItemContract.BLOODSTONE_ID, ItemContract.EULS_SCEPTER_OF_DIVINITY_ID}
                );
            }

            case HeroContract.LINA_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.CLARITY_POTION_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.HEALING_SALVE_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID, ItemContract.ANIMAL_COURIER_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID, ItemContract.BRACER_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.ARCANE_BOOTS_ID,
                            ItemContract.KELENS_DAGGER_OF_ESCAPE_ID, ItemContract.SENTRY_WARDS_ID},
                        new int[]{ItemContract.ANCIENT_JANGGO_OF_ENDURANCE_ID, ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID,
                            ItemContract.BLOODSTONE_ID, ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.SHIVAS_GUARD_ID,
                            ItemContract.EULS_SCEPTER_OF_DIVINITY_ID}
                );
            }

            case HeroContract.MINER_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.SOUL_RING_ID, ItemContract.SOBI_MASK_ID, ItemContract.CLARITY_POTION_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.CLARITY_POTION_ID},
                        new int[]{ItemContract.SOUL_RING_ID, ItemContract.BOOTS_OF_SPEED_ID},
                        new int[]{ItemContract.ARCANE_BOOTS_ID, ItemContract.EULS_SCEPTER_OF_DIVINITY_ID},
                        new int[]{ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID, ItemContract.SHIVAS_GUARD_ID,
                                 ItemContract.BOOTS_OF_TRAVEL_ID, ItemContract.LINKENS_SPHERE_ID, ItemContract.NECROMICON_ID,
                                ItemContract.BLOODSTONE_ID, ItemContract.EYE_OF_SCADI_ID}
                );
            }

            case HeroContract.ORACKLE_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANIMAL_COURIER_ID, ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_STICK_ID, ItemContract.RING_OF_REGENERATION_ID},
                        new int[]{ItemContract.ARCANE_BOOTS_ID, ItemContract.MAGIC_WAND_ID, ItemContract.MEKANSM_ID},
                        new int[]{ItemContract.KELENS_DAGGER_OF_ESCAPE_ID, ItemContract.FORCE_STAFF_ID, ItemContract.DAGON_ID,
                            ItemContract.BLOODSTONE_ID, ItemContract.BOOTS_OF_TRAVEL_ID, ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID,
                            ItemContract.EHTHEREAL_BLADE_ID, ItemContract.EULS_SCEPTER_OF_DIVINITY_ID}
                );
            }

            case HeroContract.OGRE_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID, ItemContract.CLARITY_POTION_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.ANIMAL_COURIER_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.ARCANE_BOOTS_ID, ItemContract.AGHANIMS_SCEPTER_ID,
                            ItemContract.SENTRY_WARDS_ID},
                        new int[]{ItemContract.MEKANSM_ID, ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID, ItemContract.SHIVAS_GUARD_ID,
                            ItemContract.GHOST_SCEPTER_ID, ItemContract.ANCIENT_JANGGO_OF_ENDURANCE_ID, ItemContract.BLOODSTONE_ID,
                            ItemContract.VEIL_OF_DISCORD_ID, ItemContract.EULS_SCEPTER_OF_DIVINITY_ID, ItemContract.FORCE_STAFF_ID}
                );
            }

            case HeroContract.PUCK_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID,
                            ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID, ItemContract.EMPTY_BOTTLE_ID,
                            ItemContract.BRACER_ID, ItemContract.BRACER_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.POWER_THREADS_ID, ItemContract.KELENS_DAGGER_OF_ESCAPE_ID},
                        new int[]{ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID, ItemContract.BLOODSTONE_ID, ItemContract.AGHANIMS_SCEPTER_ID,
                            ItemContract.DAGON_ID, ItemContract.SHIVAS_GUARD_ID, ItemContract.LINKENS_SPHERE_ID, ItemContract.EULS_SCEPTER_OF_DIVINITY_ID}
                );
            }

            case HeroContract.RHASTA_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.CLARITY_POTION_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID, ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.ARCANE_BOOTS_ID, ItemContract.KELENS_DAGGER_OF_ESCAPE_ID},
                        new int[]{ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.REFRESHER_ORB_ID, ItemContract.BLOODSTONE_ID,
                            ItemContract.SHIVAS_GUARD_ID, ItemContract.NECROMICON_ID}
                );
            }

            case HeroContract.RUBICK_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.CLARITY_POTION_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID},
                        new int[]{ItemContract.ARCANE_BOOTS_ID, ItemContract.MAGIC_WAND_ID, ItemContract.BLOODSTONE_ID},
                        new int[]{ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID, ItemContract.BLACK_KING_BAR_ID, ItemContract.SHIVAS_GUARD_ID,
                            ItemContract.BOOTS_OF_TRAVEL_ID, ItemContract.ORCHID_MALEVOLENCE_ID, ItemContract.KHADGARS_PIPE_OF_INSIGHT_ID,
                            ItemContract.EULS_SCEPTER_OF_DIVINITY_ID, ItemContract.FORCE_STAFF_ID}
                );
            }

            case HeroContract.RULAY_ID:{
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.ANIMAL_COURIER_ID},
                        new int[]{ItemContract.SENTRY_WARDS_ID, ItemContract.URN_OF_SHADOWS_ID,
                            ItemContract.URN_OF_SHADOWS_ID, ItemContract.SCROLL_OF_TOWN_PORTAL_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.OBSERVER_WARDS_ID,
                            ItemContract.CIRCLET_OF_NOBILITY_ID, ItemContract.TRANQUIL_BOOTS_ID},
                        new int[]{ItemContract.MEKANSM_ID, ItemContract.KELENS_DAGGER_OF_ESCAPE_ID, ItemContract.EULS_SCEPTER_OF_DIVINITY_ID,
                            ItemContract.LOTHARS_EDGE_ID, ItemContract.NECROMICON_ID, ItemContract.FORCE_STAFF_ID,
                            ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.BLOODSTONE_ID, ItemContract.LINKENS_SPHERE_ID}
                );
            }

            case HeroContract.SILENCER_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.CIRCLET_OF_NOBILITY_ID, ItemContract.MANTLE_OF_INTELLIGENCE_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_STICK_ID,
                            ItemContract.NULL_TALISMAN_ID, ItemContract.SCROLL_OF_TOWN_PORTAL_ID},
                        new int[]{ItemContract.POWER_THREADS_ID, ItemContract.FORCE_STAFF_ID, ItemContract.MAGIC_WAND_ID,
                            ItemContract.MEKANSM_ID, ItemContract.SCROLL_OF_TOWN_PORTAL_ID},
                        new int[]{ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID, ItemContract.ROD_OF_ATOS_ID, ItemContract.SHIVAS_GUARD_ID,
                            ItemContract.EULS_SCEPTER_OF_DIVINITY_ID, ItemContract.ORCHID_MALEVOLENCE_ID, ItemContract.BLACK_KING_BAR_ID}
                );
            }

            case HeroContract.TINKER_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.CLARITY_POTION_ID, ItemContract.MANTLE_OF_INTELLIGENCE_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID, ItemContract.NULL_TALISMAN_ID,
                            ItemContract.EMPTY_BOTTLE_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.BOOTS_OF_TRAVEL_ID, ItemContract.SOUL_RING_ID},
                        new int[]{ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID, ItemContract.SHIVAS_GUARD_ID,
                            ItemContract.EULS_SCEPTER_OF_DIVINITY_ID, ItemContract.FORCE_STAFF_ID,
                            ItemContract.KELENS_DAGGER_OF_ESCAPE_ID, ItemContract.DAGON_ID,
                            ItemContract.MANTA_STYLE_ID, ItemContract.BLACK_KING_BAR_ID}
                );
            }

            case HeroContract.TRALL_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.CLARITY_POTION_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID},
                        new int[]{ItemContract.PERSEVERANCE_ID, ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_WAND_ID},
                        new int[]{ItemContract.LINKENS_SPHERE_ID, ItemContract.BOOTS_OF_TRAVEL_ID},
                        new int[]{ItemContract.MONKEY_KING_BAR_ID, ItemContract.ASSAULT_CUIRAS_ID, ItemContract.BLACK_KING_BAR_ID,
                            ItemContract.SATANIC_ID, ItemContract.NECROMICON_ID, ItemContract.REFRESHER_ORB_ID, ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID,
                            ItemContract.SHIVAS_GUARD_ID}
                );
            }

            case HeroContract.PHURION_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.RING_OF_PROTECTION_ID, ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID},
                        new int[]{ItemContract.RING_OF_BASILIUS_ID, ItemContract.BOOTS_OF_SPEED_ID},
                        new int[]{ItemContract.POWER_THREADS_ID, ItemContract.RING_OF_BASILIUS_ID, ItemContract.VLADMIRS_OFFERING_ID,
                            ItemContract.NECROMICON_ID, ItemContract.ANCIENT_JANGGO_OF_ENDURANCE_ID},
                        new int[]{ItemContract.STYGIAN_DESOLATOR_ID, ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID,
                            ItemContract.ASSAULT_CUIRAS_ID, ItemContract.MONKEY_KING_BAR_ID, ItemContract.ORCHID_MALEVOLENCE_ID,
                            ItemContract.EYE_OF_SCADI_ID, ItemContract.MEKANSM_ID, ItemContract.DAGON_ID, ItemContract.FORCE_STAFF_ID}
                );
            }

            case HeroContract.CHEN_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.RING_OF_PROTECTION_ID, ItemContract.ANIMAL_COURIER_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.RING_OF_BASILIUS_ID, ItemContract.HEADDRESS_OF_REJUVENATION_ID},
                        new int[]{ItemContract.ARCANE_BOOTS_ID, ItemContract.MEKANSM_ID, ItemContract.SENTRY_WARDS_ID},
                        new int[]{ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.VLADMIRS_OFFERING_ID, ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID,
                            ItemContract.NECROMICON_ID, ItemContract.ANCIENT_JANGGO_OF_ENDURANCE_ID, ItemContract.GHOST_SCEPTER_ID,
                            ItemContract.SENTRY_WARDS_ID, ItemContract.FLYING_COURIER_ID}
                );
            }

            case HeroContract.SHTORM_SPIRIT_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.CLARITY_POTION_ID,
                            ItemContract.MANTLE_OF_INTELLIGENCE_ID, ItemContract.MANTLE_OF_INTELLIGENCE_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID,
                            ItemContract.NULL_TALISMAN_ID, ItemContract.NULL_TALISMAN_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.POWER_THREADS_ID, ItemContract.EMPTY_BOTTLE_ID},
                        new int[]{ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID, ItemContract.BLOODSTONE_ID, ItemContract.LINKENS_SPHERE_ID,
                            ItemContract.BLACK_KING_BAR_ID, ItemContract.DAGON_ID, ItemContract.SHIVAS_GUARD_ID,
                            ItemContract.ORCHID_MALEVOLENCE_ID}
                );
            }

            case HeroContract.EZALOR_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID, ItemContract.IRONWOOD_BRANCH_ID, ItemContract.ANIMAL_COURIER_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_STICK_ID,
                            ItemContract.BRACER_ID, ItemContract.SENTRY_WARDS_ID},
                        new int[]{ItemContract.TRANQUIL_BOOTS_ID, ItemContract.MAGIC_WAND_ID, ItemContract.MEKANSM_ID, ItemContract.SENTRY_WARDS_ID},
                        new int[]{ItemContract.BOOTS_OF_TRAVEL_ID, ItemContract.SHIVAS_GUARD_ID, ItemContract.ROD_OF_ATOS_ID,
                            ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID, ItemContract.KHADGARS_PIPE_OF_INSIGHT_ID, ItemContract.EULS_SCEPTER_OF_DIVINITY_ID,
                            ItemContract.FORCE_STAFF_ID, ItemContract.NECROMICON_ID, ItemContract.VEIL_OF_DISCORD_ID}
                );
            }

            case HeroContract.ENCHANTRES_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID,
                            ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.POWER_THREADS_ID, ItemContract.URN_OF_SHADOWS_ID,
                            ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.SENTRY_WARDS_ID},
                        new int[]{ItemContract.BLOODSTONE_ID, ItemContract.SHIVAS_GUARD_ID, ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID,
                            ItemContract.ORCHID_MALEVOLENCE_ID, ItemContract.HEART_OF_TARRASQUE_ID, ItemContract.ASSAULT_CUIRAS_ID,
                            ItemContract.FORCE_STAFF_ID}
                );
            }

            case HeroContract.ABBADON_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.RING_OF_BASILIUS_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_STICK_ID, ItemContract.HEADDRESS_OF_REJUVENATION_ID},
                        new int[]{ItemContract.POWER_THREADS_ID, ItemContract.VLADMIRS_OFFERING_ID, ItemContract.MAGIC_WAND_ID,
                                ItemContract.MEKANSM_ID, ItemContract.KHADGARS_PIPE_OF_INSIGHT_ID},
                        new int[]{ItemContract.SANGE_AND_YASHA_ID, ItemContract.BLADE_MAIL_ID, ItemContract.RADIANCE_ID,
                            ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.VANGUARD_ID, ItemContract.HEART_OF_TARRASQUE_ID,
                            ItemContract.ARMLET_OF_MORDIGGIAN_ID}
                );
            }

            case HeroContract.AXE_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.STOUT_SHIELD_ID, ItemContract.HEALING_SALVE_ID, ItemContract.RING_OF_PROTECTION_ID,
                            ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.TRANQUIL_BOOTS_ID, ItemContract.MAGIC_STICK_ID},
                        new int[]{ItemContract.KELENS_DAGGER_OF_ESCAPE_ID, ItemContract.VANGUARD_ID, ItemContract.BLADE_MAIL_ID,
                            ItemContract.MAGIC_WAND_ID},
                        new int[]{ItemContract.HEART_OF_TARRASQUE_ID, ItemContract.KHADGARS_PIPE_OF_INSIGHT_ID, ItemContract.SHIVAS_GUARD_ID,
                            ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.BOOTS_OF_TRAVEL_ID, ItemContract.ASSAULT_CUIRAS_ID,
                            ItemContract.HEAVENS_HALLBERD_ID}
                );
            }

            case HeroContract.BALANAR_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID, ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID,
                            ItemContract.CIRCLET_OF_NOBILITY_ID, ItemContract.HEALING_SALVE_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.BRACER_ID, ItemContract.BRACER_ID},
                        new int[]{ItemContract.POWER_THREADS_ID, ItemContract.HELM_OF_THE_DOMINATOR_ID},
                        new int[]{ItemContract.URN_OF_SHADOWS_ID, ItemContract.BASHER_ID, ItemContract.MONKEY_KING_BAR_ID,
                            ItemContract.BURIZE_ID, ItemContract.ARMLET_OF_MORDIGGIAN_ID, ItemContract.AGHANIMS_SCEPTER_ID,
                            ItemContract.SATANIC_ID, ItemContract.ASSAULT_CUIRAS_ID, ItemContract.BLACK_KING_BAR_ID}
                );
            }

            case HeroContract.BARATHRUM_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.STOUT_SHIELD_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.RING_OF_HEALTH_ID},
                        new int[]{ItemContract.POWER_THREADS_ID, ItemContract.VANGUARD_ID, ItemContract.MASK_OF_MADNESS_ID},
                        new int[]{ItemContract.BLACK_KING_BAR_ID, ItemContract.HOOD_OF_DEFIANCE_ID, ItemContract.KHADGARS_PIPE_OF_INSIGHT_ID,
                            ItemContract.ASSAULT_CUIRAS_ID, ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.BLADE_MAIL_ID,
                            ItemContract.MONKEY_KING_BAR_ID, ItemContract.RADIANCE_ID, ItemContract.HEART_OF_TARRASQUE_ID,
                            ItemContract.DIVINE_RAPIRE_ID}
                );
            }

            case HeroContract.WOLF_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.RING_OF_BASILIUS_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.QUELLING_BLADE_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MASK_OF_DEATH_ID},
                        new int[]{ItemContract.NECROMICON_ID, ItemContract.VLADMIRS_OFFERING_ID, ItemContract.BLACK_KING_BAR_ID},
                        new int[]{ItemContract.STYGIAN_DESOLATOR_ID, ItemContract.HEART_OF_TARRASQUE_ID, ItemContract.ASSAULT_CUIRAS_ID,
                            ItemContract.BATTLE_FURY_ID, ItemContract.VANGUARD_ID, ItemContract.SATANIC_ID,
                            ItemContract.BASHER_ID}
                );
            }

            case HeroContract.DOOM_ID:{
                return getItemsFromIds(
                        new int[]{ItemContract.IRONWOOD_BRANCH_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.HEALING_SALVE_ID, ItemContract.HEALING_SALVE_ID, ItemContract.STOUT_SHIELD_ID},
                        new int[]{ItemContract.RING_OF_HEALTH_ID, ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID,
                            ItemContract.RING_OF_BASILIUS_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.VANGUARD_ID, ItemContract.PHASE_BOOTS_ID},
                        new int[]{ItemContract.RADIANCE_ID, ItemContract.HEART_OF_TARRASQUE_ID, ItemContract.ASSAULT_CUIRAS_ID,
                            ItemContract.SHIVAS_GUARD_ID, ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.REFRESHER_ORB_ID,
                            ItemContract.HEAVENS_HALLBERD_ID}
                );
            }

            case HeroContract.ZOMBI_ID:{
                return getItemsFromIds(
                        new int[]{ItemContract.STOUT_SHIELD_ID, ItemContract.IRONWOOD_BRANCH_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.HEALING_SALVE_ID, ItemContract.CLARITY_POTION_ID, ItemContract.CLARITY_POTION_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.RING_OF_BASILIUS_ID, ItemContract.MAGIC_STICK_ID},
                        new int[]{ItemContract.ARCANE_BOOTS_ID, ItemContract.MAGIC_WAND_ID, ItemContract.VANGUARD_ID, ItemContract.HOOD_OF_DEFIANCE_ID},
                        new int[]{ItemContract.KHADGARS_PIPE_OF_INSIGHT_ID, ItemContract.SHIVAS_GUARD_ID, ItemContract.HEART_OF_TARRASQUE_ID,
                            ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID, ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.NECROMICON_ID,
                            ItemContract.ASSAULT_CUIRAS_ID, ItemContract.VEIL_OF_DISCORD_ID, ItemContract.BLADE_MAIL_ID}
                );
            }

            case HeroContract.LEORIK_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID, ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID},
                        new int[]{ItemContract.QUELLING_BLADE_ID, ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.POWER_THREADS_ID, ItemContract.ARMLET_OF_MORDIGGIAN_ID},
                        new int[]{ItemContract.BLACK_KING_BAR_ID, ItemContract.STYGIAN_DESOLATOR_ID, ItemContract.ASSAULT_CUIRAS_ID,
                            ItemContract.SATANIC_ID, ItemContract.BATTLE_FURY_ID, ItemContract.HEART_OF_TARRASQUE_ID,
                            ItemContract.VLADMIRS_OFFERING_ID, ItemContract.KHADGARS_PIPE_OF_INSIGHT_ID, ItemContract.MONKEY_KING_BAR_ID,
                            ItemContract.FORCE_STAFF_ID, ItemContract.KELENS_DAGGER_OF_ESCAPE_ID, ItemContract.MJOLNIR_ID}
                );
            }

            case HeroContract.MAGNUS_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEADDRESS_OF_REJUVENATION_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.CLARITY_POTION_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.STOUT_SHIELD_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_WAND_ID, ItemContract.BRACER_ID,
                            ItemContract.EMPTY_BOTTLE_ID},
                        new int[]{ItemContract.POWER_THREADS_ID, ItemContract.KELENS_DAGGER_OF_ESCAPE_ID, ItemContract.ANCIENT_JANGGO_OF_ENDURANCE_ID},
                        new int[]{ItemContract.BATTLE_FURY_ID, ItemContract.BURIZE_ID, ItemContract.HEART_OF_TARRASQUE_ID,
                            ItemContract.ASSAULT_CUIRAS_ID, ItemContract.MASK_OF_MADNESS_ID, ItemContract.REFRESHER_ORB_ID}
                );
            }

            case HeroContract.NIXE_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.STOUT_SHIELD_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID},
                        new int[]{ItemContract.ARMLET_OF_MORDIGGIAN_ID, ItemContract.POWER_THREADS_ID},
                        new int[]{ItemContract.MJOLNIR_ID, ItemContract.STYGIAN_DESOLATOR_ID,
                            ItemContract.BASHER_ID, ItemContract.MONKEY_KING_BAR_ID, ItemContract.ASSAULT_CUIRAS_ID,
                            ItemContract.HEART_OF_TARRASQUE_ID, ItemContract.BLACK_KING_BAR_ID,
                            ItemContract.BUTTERFLY_ID, ItemContract.MANTA_STYLE_ID}
                );
            }

            case HeroContract.PIT_LORD_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANIMAL_COURIER_ID, ItemContract.SOBI_MASK_ID, ItemContract.HEALING_SALVE_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.SOUL_RING_ID},
                        new int[]{ItemContract.HOOD_OF_DEFIANCE_ID, ItemContract.VITALITY_BOOSTER_ID,
                            ItemContract.BOOTS_OF_TRAVEL_ID},
                        new int[]{ItemContract.SHIVAS_GUARD_ID, ItemContract.HEART_OF_TARRASQUE_ID}
                );
            }

            case HeroContract.PUDGE_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID, ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.EMPTY_BOTTLE_ID, ItemContract.BOOTS_OF_SPEED_ID,
                            ItemContract.BRACER_ID, ItemContract.BRACER_ID},
                        new int[]{ItemContract.PHASE_BOOTS_ID, ItemContract.HOOD_OF_DEFIANCE_ID, ItemContract.SENTRY_WARDS_ID},
                        new int[]{ItemContract.KHADGARS_PIPE_OF_INSIGHT_ID, ItemContract.HEART_OF_TARRASQUE_ID,
                            ItemContract.VANGUARD_ID, ItemContract.AGHANIMS_SCEPTER_ID}
                );
            }

            case HeroContract.SLARDAR_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.STOUT_SHIELD_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID, ItemContract.RING_OF_HEALTH_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.POWER_THREADS_ID, ItemContract.VANGUARD_ID},
                        new int[]{ItemContract.BLACK_KING_BAR_ID, ItemContract.KELENS_DAGGER_OF_ESCAPE_ID, ItemContract.HEART_OF_TARRASQUE_ID,
                            ItemContract.STYGIAN_DESOLATOR_ID, ItemContract.ASSAULT_CUIRAS_ID, ItemContract.SATANIC_ID});
            }

            case HeroContract.SCORPION_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID, ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID,
                            ItemContract.HEALING_SALVE_ID, ItemContract.HEALING_SALVE_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.CLARITY_POTION_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID, ItemContract.BRACER_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.ARCANE_BOOTS_ID, ItemContract.KELENS_DAGGER_OF_ESCAPE_ID},
                        new int[]{ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.VEIL_OF_DISCORD_ID, ItemContract.BLADE_MAIL_ID,
                            ItemContract.SHIVAS_GUARD_ID, ItemContract.BLACK_KING_BAR_ID, ItemContract.HEART_OF_TARRASQUE_ID}
                );
            }

            case HeroContract.TIDEHUNTER_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.CLARITY_POTION_ID,
                            ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID, ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID, ItemContract.BRACER_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.ARCANE_BOOTS_ID, ItemContract.KELENS_DAGGER_OF_ESCAPE_ID},
                        new int[]{ItemContract.HEART_OF_TARRASQUE_ID, ItemContract.RADIANCE_ID, ItemContract.ASSAULT_CUIRAS_ID,
                            ItemContract.SHIVAS_GUARD_ID, ItemContract.REFRESHER_ORB_ID, ItemContract.ANCIENT_JANGGO_OF_ENDURANCE_ID}
                );
            }

            case HeroContract.CHAOS_KNIGHT: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_STICK_ID, ItemContract.BRACER_ID},
                        new int[]{ItemContract.POWER_THREADS_ID, ItemContract.MAGIC_WAND_ID,
                            ItemContract.ARMLET_OF_MORDIGGIAN_ID, ItemContract.ANCIENT_JANGGO_OF_ENDURANCE_ID},
                        new int[]{ItemContract.MANTA_STYLE_ID, ItemContract.ASSAULT_CUIRAS_ID, ItemContract.SATANIC_ID,
                            ItemContract.HEART_OF_TARRASQUE_ID, ItemContract.SANGE_AND_YASHA_ID,
                            ItemContract.BLACK_KING_BAR_ID, ItemContract.HEAVENS_HALLBERD_ID}
                );
            }

            case HeroContract.NERUBIAN_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.RING_OF_BASILIUS_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_STICK_ID},
                        new int[]{ItemContract.POWER_THREADS_ID, ItemContract.MAGIC_WAND_ID, ItemContract.RING_OF_AQUILA_ID},
                        new int[]{ItemContract.DAGON_ID, ItemContract.STYGIAN_DESOLATOR_ID, ItemContract.HEART_OF_TARRASQUE_ID,
                            ItemContract.ASSAULT_CUIRAS_ID}
                );
            }

            case HeroContract.BLOODSEECKER_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.STOUT_SHIELD_ID,
                            ItemContract.QUELLING_BLADE_ID},
                        new int[]{ItemContract.POOR_MANS_SHIELD_ID, ItemContract.BOOTS_OF_SPEED_ID,
                            ItemContract.MAGIC_WAND_ID, ItemContract.SCROLL_OF_TOWN_PORTAL_ID},
                        new int[]{ItemContract.POWER_THREADS_ID, ItemContract.WRAITH_BAND_ID, ItemContract.SANGE_AND_YASHA_ID},
                        new int[]{ItemContract.RADIANCE_ID, ItemContract.BUTTERFLY_ID, ItemContract.BLACK_KING_BAR_ID,
                            ItemContract.MONKEY_KING_BAR_ID, ItemContract.ASSAULT_CUIRAS_ID, ItemContract.HEART_OF_TARRASQUE_ID}
                );
            }

            case HeroContract.BONE_PHLETCHER_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.HEALING_SALVE_ID, ItemContract.SLIPPERS_OF_AGILITY_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.RING_OF_AQUILA_ID,
                            ItemContract.MAGIC_WAND_ID, ItemContract.OBLIVION_STAFF_ID},
                        new int[]{ItemContract.POWER_THREADS_ID, ItemContract.ORCHID_MALEVOLENCE_ID,
                            ItemContract.MONKEY_KING_BAR_ID},
                        new int[]{ItemContract.BURIZE_ID, ItemContract.BUTTERFLY_ID, ItemContract.EYE_OF_SCADI_ID,
                            ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID, ItemContract.BLACK_KING_BAR_ID}
                );
            }

            case HeroContract.BROODMOTHER_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.RING_OF_BASILIUS_ID},
                        new int[]{ItemContract.PHASE_BOOTS_ID, ItemContract.VLADMIRS_OFFERING_ID},
                        new int[]{ItemContract.BLACK_KING_BAR_ID, ItemContract.BUTTERFLY_ID, ItemContract.BURIZE_ID,
                            ItemContract.MONKEY_KING_BAR_ID, ItemContract.POWER_THREADS_ID, ItemContract.RADIANCE_ID,
                            ItemContract.BASHER_ID, ItemContract.HEART_OF_TARRASQUE_ID, ItemContract.LINKENS_SPHERE_ID,
                            ItemContract.BLADE_MAIL_ID, ItemContract.HOOD_OF_DEFIANCE_ID, ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID}
                );
            }

            case HeroContract.VIPER_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.SLIPPERS_OF_AGILITY_ID,
                            ItemContract.SLIPPERS_OF_AGILITY_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID,
                            ItemContract.WRAITH_BAND_ID, ItemContract.WRAITH_BAND_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.POWER_THREADS_ID, ItemContract.VANGUARD_ID},
                        new int[]{ItemContract.MANTA_STYLE_ID, ItemContract.BLACK_KING_BAR_ID, ItemContract.MONKEY_KING_BAR_ID,
                            ItemContract.LOTHARS_EDGE_ID, ItemContract.BUTTERFLY_ID, ItemContract.ORCHID_MALEVOLENCE_ID,
                            ItemContract.HEART_OF_TARRASQUE_ID, ItemContract.AGHANIMS_SCEPTER_ID}
                );
            }

            case HeroContract.VENOMANCER_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.RING_OF_PROTECTION_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.SLIPPERS_OF_AGILITY_ID, ItemContract.HEALING_SALVE_ID, ItemContract.CLARITY_POTION_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID, ItemContract.RING_OF_AQUILA_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.POWER_THREADS_ID, ItemContract.AGHANIMS_SCEPTER_ID},
                        new int[]{ItemContract.EULS_SCEPTER_OF_DIVINITY_ID, ItemContract.KELENS_DAGGER_OF_ESCAPE_ID,
                            ItemContract.LOTHARS_EDGE_ID, ItemContract.BUTTERFLY_ID, ItemContract.ORCHID_MALEVOLENCE_ID,
                            ItemContract.MANTA_STYLE_ID}
                );
            }

            case HeroContract.VIVER_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.IRONWOOD_BRANCH_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                                ItemContract.HEALING_SALVE_ID, ItemContract.CLARITY_POTION_ID,
                                ItemContract.SLIPPERS_OF_AGILITY_ID, ItemContract.SLIPPERS_OF_AGILITY_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID,
                            ItemContract.WRAITH_BAND_ID, ItemContract.WRAITH_BAND_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.POWER_THREADS_ID, ItemContract.RADIANCE_ID},
                        new int[]{ItemContract.VANGUARD_ID, ItemContract.MONKEY_KING_BAR_ID, ItemContract.LINKENS_SPHERE_ID, ItemContract.BUTTERFLY_ID}
                );
            }

            case HeroContract.VOID_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.HEALING_SALVE_ID, ItemContract.QUELLING_BLADE_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID, ItemContract.WRAITH_BAND_ID},
                        new int[]{ItemContract.POWER_THREADS_ID, ItemContract.SCROLL_OF_TOWN_PORTAL_ID, ItemContract.MASK_OF_MADNESS_ID,
                            ItemContract.YASHA_ID},
                        new int[]{ItemContract.BATTLE_FURY_ID, ItemContract.MONKEY_KING_BAR_ID, ItemContract.BUTTERFLY_ID,
                            ItemContract.BUTTERFLY_ID, ItemContract.ASSAULT_CUIRAS_ID, ItemContract.VANGUARD_ID}
                );
            }

            case HeroContract.GEOMANCER_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.RING_OF_PROTECTION_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.HEALING_SALVE_ID, ItemContract.HEALING_SALVE_ID},
                        new int[]{ItemContract.RING_OF_BASILIUS_ID, ItemContract.BOOTS_OF_SPEED_ID},
                        new int[]{ItemContract.POWER_THREADS_ID, ItemContract.MEKANSM_ID, ItemContract.VLADMIRS_OFFERING_ID},
                        new int[]{ItemContract.KHADGARS_PIPE_OF_INSIGHT_ID, ItemContract.AGHANIMS_SCEPTER_ID,
                            ItemContract.KELENS_DAGGER_OF_ESCAPE_ID, ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID,
                            ItemContract.ASSAULT_CUIRAS_ID, ItemContract.HEART_OF_TARRASQUE_ID}
                );
            }

            case HeroContract.MEDUSA_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.RING_OF_PROTECTION_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.RING_OF_BASILIUS_ID, ItemContract.BOOTS_OF_SPEED_ID, ItemContract.RING_OF_HEALTH_ID},
                        new int[]{ItemContract.PHASE_BOOTS_ID, ItemContract.RING_OF_AQUILA_ID, ItemContract.LINKENS_SPHERE_ID},
                        new int[]{ItemContract.MANTA_STYLE_ID, ItemContract.BUTTERFLY_ID, ItemContract.EYE_OF_SCADI_ID,
                            ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID, ItemContract.BURIZE_ID, ItemContract.DIVINE_RAPIRE_ID}
                );
            }

            case HeroContract.MORTRED_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.SLIPPERS_OF_AGILITY_ID, ItemContract.SLIPPERS_OF_AGILITY_ID},
                        new int[]{ItemContract.HELM_OF_IRON_WILL_ID, ItemContract.BOOTS_OF_SPEED_ID},
                        new int[]{ItemContract.HELM_OF_THE_DOMINATOR_ID, ItemContract.POWER_THREADS_ID},
                        new int[]{ItemContract.BLACK_KING_BAR_ID, ItemContract.BATTLE_FURY_ID, ItemContract.MONKEY_KING_BAR_ID,
                            ItemContract.MANTA_STYLE_ID, ItemContract.ASSAULT_CUIRAS_ID, ItemContract.SATANIC_ID,
                            ItemContract.STYGIAN_DESOLATOR_ID, ItemContract.DIVINE_RAPIRE_ID}
                );
            }

            case HeroContract.NEVERMORE_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.HEALING_SALVE_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.SLIPPERS_OF_AGILITY_ID, ItemContract.SLIPPERS_OF_AGILITY_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID, ItemContract.RING_OF_AQUILA_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.POWER_THREADS_ID,
                            ItemContract.KELENS_DAGGER_OF_ESCAPE_ID, ItemContract.BLACK_KING_BAR_ID},
                        new int[]{ItemContract.LOTHARS_EDGE_ID, ItemContract.ASSAULT_CUIRAS_ID,
                            ItemContract.BURIZE_ID, ItemContract.STYGIAN_DESOLATOR_ID, ItemContract.BUTTERFLY_ID,
                            ItemContract.MONKEY_KING_BAR_ID}
                );
            }

            case HeroContract.RAZORE_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.HEALING_SALVE_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                                ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.IRONWOOD_BRANCH_ID,
                                ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID, ItemContract.VANGUARD_ID},
                        new int[]{ItemContract.POWER_THREADS_ID, ItemContract.MAGIC_WAND_ID, ItemContract.DIFFUSAL_BLADE_ID},
                        new int[]{ItemContract.MANTA_STYLE_ID, ItemContract.BLACK_KING_BAR_ID, ItemContract.HEART_OF_TARRASQUE_ID,
                            ItemContract.BUTTERFLY_ID, ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID, ItemContract.KELENS_DAGGER_OF_ESCAPE_ID}
                        );
            }

            case HeroContract.SLARK_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.SLIPPERS_OF_AGILITY_ID, ItemContract.SLIPPERS_OF_AGILITY_ID},
                        new int[]{ItemContract.POOR_MANS_SHIELD_ID, ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_STICK_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.POWER_THREADS_ID, ItemContract.ORB_OF_VENOM_ID,
                            ItemContract.DIFFUSAL_BLADE_ID},
                        new int[]{ItemContract.MANTA_STYLE_ID, ItemContract.ASSAULT_CUIRAS_ID, ItemContract.BLACK_KING_BAR_ID,
                            ItemContract.MASK_OF_MADNESS_ID, ItemContract.MONKEY_KING_BAR_ID, ItemContract.STYGIAN_DESOLATOR_ID,
                            ItemContract.EHTHEREAL_BLADE_ID, ItemContract.DAGON_ID, ItemContract.BUTTERFLY_ID,
                            ItemContract.BLADE_MAIL_ID, ItemContract.DIVINE_RAPIRE_ID}
                );
            }

            case HeroContract.SOUL_KEEPER: {
                return getItemsFromIds(
                        new int[]{ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.RING_OF_PROTECTION_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.RING_OF_BASILIUS_ID, ItemContract.BOOTS_OF_SPEED_ID},
                        new int[]{ItemContract.POWER_THREADS_ID, ItemContract.BRACER_ID, ItemContract.YASHA_ID},
                        new int[]{ItemContract.MANTA_STYLE_ID, ItemContract.SANGE_AND_YASHA_ID, ItemContract.SATANIC_ID,
                            ItemContract.BUTTERFLY_ID, ItemContract.MONKEY_KING_BAR_ID, ItemContract.BURIZE_ID,
                            ItemContract.HEART_OF_TARRASQUE_ID, ItemContract.LOTHARS_EDGE_ID}
                );
            }

            case HeroContract.SPECTER_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.RING_OF_BASILIUS_ID,
                            ItemContract.HEALING_SALVE_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MASK_OF_DEATH_ID, ItemContract.RING_OF_REGENERATION_ID},
                        new int[]{ItemContract.POWER_THREADS_ID, ItemContract.VLADMIRS_OFFERING_ID, ItemContract.YASHA_ID},
                        new int[]{ItemContract.RADIANCE_ID, ItemContract.MANTA_STYLE_ID, ItemContract.DIFFUSAL_BLADE_ID,
                            ItemContract.BUTTERFLY_ID}
                );
            }

            case HeroContract.ZET_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.BLADES_OF_ATTACK_ID,
                            ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.YASHA_ID},
                        new int[]{ItemContract.PHASE_BOOTS_ID, ItemContract.STYGIAN_DESOLATOR_ID, ItemContract.SANGE_AND_YASHA_ID},
                        new int[]{ItemContract.BURIZE_ID, ItemContract.BUTTERFLY_ID, ItemContract.HEART_OF_TARRASQUE_ID,
                            ItemContract.ASSAULT_CUIRAS_ID, ItemContract.MONKEY_KING_BAR_ID,
                            ItemContract.BLOODSTONE_ID, ItemContract.BLACK_KING_BAR_ID}
                );
            }

            case HeroContract.AKASHA_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.HEALING_SALVE_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.MANTLE_OF_INTELLIGENCE_ID, ItemContract.MANTLE_OF_INTELLIGENCE_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.EMPTY_BOTTLE_ID,
                            ItemContract.MAGIC_STICK_ID, ItemContract.NULL_TALISMAN_ID, ItemContract.NULL_TALISMAN_ID},
                        new int[]{ItemContract.POWER_THREADS_ID, ItemContract.MAGIC_WAND_ID, ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID},
                        new int[]{ItemContract.EYE_OF_SCADI_ID, ItemContract.MJOLNIR_ID, ItemContract.SHIVAS_GUARD_ID,
                            ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.VEIL_OF_DISCORD_ID, ItemContract.NECROMICON_ID,
                            ItemContract.LINKENS_SPHERE_ID, ItemContract.MONKEY_KING_BAR_ID, ItemContract.STYGIAN_DESOLATOR_ID,
                            ItemContract.ORCHID_MALEVOLENCE_ID}
                );
            }

            case HeroContract.ANCIENT_APPARATION_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANIMAL_COURIER_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.FLYING_COURIER_ID, ItemContract.EMPTY_BOTTLE_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID},
                        new int[]{ItemContract.BOOTS_OF_TRAVEL_ID, ItemContract.SHIVAS_GUARD_ID,
                            ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID, ItemContract.EYE_OF_SCADI_ID,
                            ItemContract.EMPTY_BOTTLE_ID, ItemContract.HEART_OF_TARRASQUE_ID}
                );
            }

            case HeroContract.ATROPOS_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.CLARITY_POTION_ID,
                            ItemContract.MANTLE_OF_INTELLIGENCE_ID, ItemContract.MANTLE_OF_INTELLIGENCE_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_STICK_ID,
                            ItemContract.SOUL_RING_ID, ItemContract.NULL_TALISMAN_ID},
                        new int[]{ItemContract.ARCANE_BOOTS_ID, ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.SENTRY_WARDS_ID},
                        new int[]{ItemContract.NECROMICON_ID, ItemContract.BLACK_KING_BAR_ID, ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID,
                            ItemContract.SHIVAS_GUARD_ID, ItemContract.BOOTS_OF_TRAVEL_ID, ItemContract.GHOST_SCEPTER_ID,
                            ItemContract.KELENS_DAGGER_OF_ESCAPE_ID}
                );
            }

            case HeroContract.KROBELUS_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.CLARITY_POTION_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.MANTLE_OF_INTELLIGENCE_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_STICK_ID,
                            ItemContract.NULL_TALISMAN_ID},
                        new int[]{ItemContract.PHASE_BOOTS_ID, ItemContract.MAGIC_WAND_ID, ItemContract.BLOODSTONE_ID},
                        new int[]{ItemContract.ANCIENT_JANGGO_OF_ENDURANCE_ID, ItemContract.SHIVAS_GUARD_ID,
                            ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID, ItemContract.HEART_OF_TARRASQUE_ID,
                            ItemContract.BLACK_KING_BAR_ID, ItemContract.MEKANSM_ID, ItemContract.KHADGARS_PIPE_OF_INSIGHT_ID,
                            ItemContract.BOOTS_OF_TRAVEL_ID, ItemContract.EULS_SCEPTER_OF_DIVINITY_ID}
                );
            }

            case HeroContract.BATRIDER_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.STOUT_SHIELD_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_STICK_ID, ItemContract.URN_OF_SHADOWS_ID},
                        new int[]{ItemContract.POWER_THREADS_ID, ItemContract.KELENS_DAGGER_OF_ESCAPE_ID, ItemContract.VANGUARD_ID,
                            ItemContract.MAGIC_WAND_ID, ItemContract.SCROLL_OF_TOWN_PORTAL_ID},
                        new int[]{ItemContract.HOOD_OF_DEFIANCE_ID, ItemContract.EULS_SCEPTER_OF_DIVINITY_ID,
                            ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID, ItemContract.KHADGARS_PIPE_OF_INSIGHT_ID,
                            ItemContract.SHIVAS_GUARD_ID, ItemContract.FORCE_STAFF_ID, ItemContract.LINKENS_SPHERE_ID,
                            ItemContract.BOOTS_OF_TRAVEL_ID}
                );
            }

            case HeroContract.WARLOCK_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANIMAL_COURIER_ID, ItemContract.RING_OF_PROTECTION_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.HEALING_SALVE_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID,
                            ItemContract.BRACER_ID, ItemContract.RING_OF_BASILIUS_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.ARCANE_BOOTS_ID,
                            ItemContract.MEKANSM_ID, ItemContract.SENTRY_WARDS_ID},
                        new int[]{ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID, ItemContract.SHIVAS_GUARD_ID,
                            ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.REFRESHER_ORB_ID,
                            ItemContract.NECROMICON_ID, ItemContract.ANCIENT_JANGGO_OF_ENDURANCE_ID,
                            ItemContract.KHADGARS_PIPE_OF_INSIGHT_ID, ItemContract.VEIL_OF_DISCORD_ID}
                );
            }

            case HeroContract.VISAGE_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.CLARITY_POTION_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID, ItemContract.RING_OF_PROTECTION_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_STICK_ID, ItemContract.RING_OF_BASILIUS_ID},
                        new int[]{ItemContract.POWER_THREADS_ID, ItemContract.MAGIC_WAND_ID,
                            ItemContract.MEKANSM_ID, ItemContract.MEDALLION_OF_COURAGE_ID},
                        new int[]{ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID,
                            ItemContract.SHIVAS_GUARD_ID, ItemContract.ASSAULT_CUIRAS_ID, ItemContract.ROD_OF_ATOS_ID,
                            ItemContract.HEART_OF_TARRASQUE_ID, ItemContract.MJOLNIR_ID}
                );
            }

            case HeroContract.WINTER_VIVERN_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.IRONWOOD_BRANCH_ID, ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID,
                            ItemContract.CIRCLET_OF_NOBILITY_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.CLARITY_POTION_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BRACER_ID, ItemContract.BOOTS_OF_SPEED_ID},
                        new int[]{ItemContract.ARCANE_BOOTS_ID, ItemContract.MAGIC_WAND_ID,
                            ItemContract.ANCIENT_JANGGO_OF_ENDURANCE_ID, ItemContract.MEKANSM_ID},
                        new int[]{ItemContract.FORCE_STAFF_ID, ItemContract.ROD_OF_ATOS_ID,
                            ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID, ItemContract.DAGON_ID,
                            ItemContract.SHIVAS_GUARD_ID, ItemContract.EYE_OF_SCADI_ID,
                            ItemContract.NECROMICON_ID}
                );
            }

            case HeroContract.WITCH_DOCTOR_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.CLARITY_POTION_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID, ItemContract.ANIMAL_COURIER_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID,
                            ItemContract.BRACER_ID, ItemContract.SENTRY_WARDS_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.PHASE_BOOTS_ID},
                        new int[]{ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID, ItemContract.BLACK_KING_BAR_ID,
                            ItemContract.SHIVAS_GUARD_ID, ItemContract.BLOODSTONE_ID,
                            ItemContract.DAGON_ID, ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.FLYING_COURIER_ID}
                );
            }

            case HeroContract.DUZZLE_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.CLARITY_POTION_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.ANIMAL_COURIER_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID,
                            ItemContract.URN_OF_SHADOWS_ID, ItemContract.SOUL_RING_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.ARCANE_BOOTS_ID,
                            ItemContract.SENTRY_WARDS_ID, ItemContract.MEKANSM_ID},
                        new int[]{ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID, ItemContract.SHIVAS_GUARD_ID,
                            ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.KHADGARS_PIPE_OF_INSIGHT_ID,
                            ItemContract.EULS_SCEPTER_OF_DIVINITY_ID, ItemContract.MEDALLION_OF_COURAGE_ID,
                            ItemContract.ANCIENT_JANGGO_OF_ENDURANCE_ID}
                );
            }

            case HeroContract.DARK_SEER_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.STOUT_SHIELD_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID,
                            ItemContract.SOUL_RING_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.ARCANE_BOOTS_ID,
                            ItemContract.VANGUARD_ID, ItemContract.HOOD_OF_DEFIANCE_ID},
                        new int[]{ItemContract.KHADGARS_PIPE_OF_INSIGHT_ID, ItemContract.AGHANIMS_SCEPTER_ID,
                            ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID, ItemContract.SHIVAS_GUARD_ID,
                            ItemContract.HEART_OF_TARRASQUE_ID, ItemContract.REFRESHER_ORB_ID}
                );
            }

            case HeroContract.DESTROYER_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                                ItemContract.MANTLE_OF_INTELLIGENCE_ID, ItemContract.MANTLE_OF_INTELLIGENCE_ID,
                                ItemContract.HEALING_SALVE_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_STICK_ID,
                            ItemContract.NULL_TALISMAN_ID, ItemContract.NULL_TALISMAN_ID, ItemContract.EMPTY_BOTTLE_ID},
                        new int[]{ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID, ItemContract.BLACK_KING_BAR_ID, ItemContract.MAGIC_WAND_ID,
                            ItemContract.POWER_THREADS_ID},
                        new int[]{ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.SHIVAS_GUARD_ID, ItemContract.KELENS_DAGGER_OF_ESCAPE_ID,
                            ItemContract.FORCE_STAFF_ID, ItemContract.REFRESHER_ORB_ID, ItemContract.FORCE_STAFF_ID, ItemContract.LINKENS_SPHERE_ID,
                                ItemContract.ORCHID_MALEVOLENCE_ID, ItemContract.HYPERSTONE_ID, ItemContract.MYSTIC_STAFF_ID}
                        );
            }

            case HeroContract.INVOCKER_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.CIRCLET_OF_NOBILITY_ID, ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.BOOTS_OF_SPEED_ID, ItemContract.MAGIC_WAND_ID,
                            ItemContract.BRACER_ID},
                        new int[]{ItemContract.PHASE_BOOTS_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID},
                        new int[]{ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.BLACK_KING_BAR_ID,
                            ItemContract.HEART_OF_TARRASQUE_ID, ItemContract.SHIVAS_GUARD_ID}
                );
            }

            case HeroContract.LESHRACK_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.CLARITY_POTION_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID,
                            ItemContract.HEALING_SALVE_ID, ItemContract.ANIMAL_COURIER_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID,
                            ItemContract.BRACER_ID, ItemContract.SENTRY_WARDS_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.PHASE_BOOTS_ID, ItemContract.BLOODSTONE_ID},
                        new int[]{ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.BLACK_KING_BAR_ID,
                            ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID, ItemContract.KELENS_DAGGER_OF_ESCAPE_ID,
                            ItemContract.SHIVAS_GUARD_ID, ItemContract.FLYING_COURIER_ID}
                );
            }

            case HeroContract.LION_ID:{
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.HEALING_SALVE_ID, ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID,
                            ItemContract.ANIMAL_COURIER_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID,
                            ItemContract.BRACER_ID, ItemContract.SENTRY_WARDS_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.TRANQUIL_BOOTS_ID,
                            ItemContract.MEKANSM_ID, ItemContract.SENTRY_WARDS_ID},
                        new int[]{ItemContract.KELENS_DAGGER_OF_ESCAPE_ID, ItemContract.BOOTS_OF_TRAVEL_ID,
                            ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID, ItemContract.DAGON_ID,
                            ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.FLYING_COURIER_ID}
                );
            }

            case HeroContract.LICH_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.HEALING_SALVE_ID, ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID,
                            ItemContract.ANIMAL_COURIER_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID,
                            ItemContract.BRACER_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.TRANQUIL_BOOTS_ID,
                            ItemContract.MEKANSM_ID, ItemContract.SENTRY_WARDS_ID},
                        new int[]{ItemContract.BOOTS_OF_TRAVEL_ID, ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID,
                            ItemContract.SHIVAS_GUARD_ID, ItemContract.AGHANIMS_SCEPTER_ID,
                            ItemContract.FLYING_COURIER_ID}
                );
            }

            case HeroContract.NECKROLIT_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.MANTLE_OF_INTELLIGENCE_ID,
                            ItemContract.HEALING_SALVE_ID, ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID,
                            ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID,
                            ItemContract.HOOD_OF_DEFIANCE_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.POWER_THREADS_ID,
                            ItemContract.MEKANSM_ID, ItemContract.POINT_BOOSTER_ID},
                        new int[]{ItemContract.BLOODSTONE_ID, ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID,
                            ItemContract.SHIVAS_GUARD_ID, ItemContract.AGHANIMS_SCEPTER_ID,
                            ItemContract.KHADGARS_PIPE_OF_INSIGHT_ID, ItemContract.HEART_OF_TARRASQUE_ID,
                            ItemContract.BOOTS_OF_SPEED_ID}
                );
            }

            case HeroContract.PUGNA_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.CLARITY_POTION_ID,
                            ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID,
                            ItemContract.BRACER_ID, ItemContract.RING_OF_BASILIUS_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.PHASE_BOOTS_ID,
                            ItemContract.MEKANSM_ID},
                        new int[]{ItemContract.AGHANIMS_SCEPTER_ID, ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID,
                            ItemContract.SHIVAS_GUARD_ID, ItemContract.DAGON_ID,
                            ItemContract.BLACK_KING_BAR_ID, ItemContract.ANCIENT_JANGGO_OF_ENDURANCE_ID}
                );
            }

            case HeroContract.SHADOW_DEMON_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.HEALING_SALVE_ID,
                            ItemContract.MANTLE_OF_INTELLIGENCE_ID, ItemContract.IRONWOOD_BRANCH_ID,
                            ItemContract.IRONWOOD_BRANCH_ID, ItemContract.IRONWOOD_BRANCH_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID,
                            ItemContract.NULL_TALISMAN_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.ARCANE_BOOTS_ID,
                            ItemContract.FORCE_STAFF_ID, ItemContract.SCROLL_OF_TOWN_PORTAL_ID},
                        new int[]{ItemContract.KHADGARS_PIPE_OF_INSIGHT_ID, ItemContract.SHIVAS_GUARD_ID,
                            ItemContract.NECROMICON_ID, ItemContract.MEKANSM_ID, ItemContract.EULS_SCEPTER_OF_DIVINITY_ID,
                            ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID}
                );
            }

            case HeroContract.ENIGMA_ID: {
                return getItemsFromIds(
                        new int[]{ItemContract.RING_OF_PROTECTION_ID, ItemContract.CLARITY_POTION_ID,
                            ItemContract.CLARITY_POTION_ID, ItemContract.CLARITY_POTION_ID,
                            ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, ItemContract.ANIMAL_COURIER_ID},
                        new int[]{ItemContract.MAGIC_STICK_ID, ItemContract.BOOTS_OF_SPEED_ID,
                            ItemContract.RING_OF_BASILIUS_ID, ItemContract.SOUL_RING_ID},
                        new int[]{ItemContract.MAGIC_WAND_ID, ItemContract.POWER_THREADS_ID,
                            ItemContract.KELENS_DAGGER_OF_ESCAPE_ID, ItemContract.BRACER_ID},
                        new int[]{ItemContract.MEKANSM_ID, ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID,
                            ItemContract.SHIVAS_GUARD_ID, ItemContract.RADIANCE_ID,
                            ItemContract.BLACK_KING_BAR_ID}
                );
            }

            default:{
                return null;
            }
        }
    }

    private static ArrayList<Item>[] getItemsFromIds( int[] startIds, int[] primaryIds, int[] mainIds, int[] situationalIds){
        ArrayList<Item>[] items =  new ArrayList[4];
        items[0] = ItemLab.getItemsByIds(startIds);
        items[1] = ItemLab.getItemsByIds(primaryIds);
        items[2] = ItemLab.getItemsByIds(mainIds);
        items[3] = ItemLab.getItemsByIds(situationalIds);
        return items;
    }
}
