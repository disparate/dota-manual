package kazarovets.dotamanual.model;

import java.util.ArrayList;

public class Hero {
    private int id;
    private String firstName;
    private String secondName;
    private int pictureResource;
    private int gifResource;
    private int[] skillsPictureIds;
    private ArrayList<Item> startItems;
    private ArrayList<Item> primaryItems;
    private ArrayList<Item> mainItems;
    private ArrayList<Item> situationalItems;

    public Hero(int id, String firstName, String secondName, int pictureResource,
                int[] skillsPictureIds, int gifResource){
        this.id = id;
        this.firstName = firstName;
        this.secondName = secondName;
        this.pictureResource = pictureResource;
        this.skillsPictureIds = skillsPictureIds;

        ArrayList<Item>[] items = HeroItemsLab.getItemsForHero(id);
        this.startItems = items[0];
        this.primaryItems = items[1];
        this.mainItems = items[2];
        this.situationalItems = items[3];

        this.gifResource = gifResource;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public int getPictureResource() {
        return pictureResource;
    }

    public void setPictureResource(int pictureResource) {
        this.pictureResource = pictureResource;
    }

    public int[] getSkillsPictureIds() {
        return skillsPictureIds;
    }

    public void setSkillsPictureIds(int[] skillsPictureIds) {
        this.skillsPictureIds = skillsPictureIds;
    }

    public ArrayList<Item> getStartItems() {
        return startItems;
    }

    public void setStartItems(ArrayList<Item> startItems) {
        this.startItems = startItems;
    }

    public ArrayList<Item> getPrimaryItems() {
        return primaryItems;
    }

    public void setPrimaryItems(ArrayList<Item> primaryItems) {
        this.primaryItems = primaryItems;
    }

    public ArrayList<Item> getMainItems() {
        return mainItems;
    }

    public void setMainItems(ArrayList<Item> mainItems) {
        this.mainItems = mainItems;
    }

    public ArrayList<Item> getSituationalItems() {
        return situationalItems;
    }

    public int getGifResource() {
        return gifResource;
    }

    public void setGifResource(int gifResource) {
        this.gifResource = gifResource;
    }
}
