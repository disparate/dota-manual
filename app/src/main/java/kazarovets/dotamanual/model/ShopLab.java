package kazarovets.dotamanual.model;

import java.util.ArrayList;

public class ShopLab {

    private static ArrayList<Shop> shops = null;

    public static void createShopsList(){
        if(shops == null) {
            shops = new ArrayList<>();
        }

    }

    public static void addShop(Shop shop){
        shop.setId(shops.size());
        shops.add(shop);
    }

    public static ArrayList<Shop> getShops(){
        return shops;
    }

}
