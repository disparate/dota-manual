package kazarovets.dotamanual.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import kazarovets.dotamanual.R;
import kazarovets.dotamanual.contracts.ItemContract;


public class ItemLab {
    private static ArrayList<Item> items = null;

    public static ArrayList<Item> getItems() {
        if (items == null) {
            items = createItems();
        }
        return items;
    }



    public static Item getItemById(int id) {
        Item result = null;
        if(getItems().size() >= id && getItems().get(id - 1).getId() == id ){ //If sorted array by id, starting from 1
            result = getItems().get(id - 1);
        } else {
            for (Item i : getItems()) {
                if (i.getId() == id) {
                    result = i;
                    break;
                }
            }
        }
        return result;
    }

    public static ArrayList<Item> getItemsByIds(int[] ids) {
        ArrayList<Item> results = new ArrayList<>();
        for (int id : ids) {
            if(getItems().get(id - 1).getId() == id){ //Sorted array by id, starting from 1
                results.add(getItems().get(id - 1));
            } else {
                for (Item i : getItems()) {
                    if (i.getId() == id) {
                        results.add(i);
                        break;
                    }
                }
            }
        }
        return results;
    }

    private static ArrayList<Item> createItems() {
        items = new ArrayList<Item>();

        ShopLab.createShopsList();

        items.addAll(createItemsFromSecretShop());
        items.addAll(createItemsFromCacheOfTheQuelthelan());
        items.addAll(createItemsFromGraveyard());
        items.addAll(createItemsFromSenaTheAccessorizer());
        items.addAll(createItemsFromBeazelTheWeaponsDealer());

        items.addAll(createItemsFromEnchantedArtifacts());
        items.addAll(createItemsFromGatewayRelics());//Need to be after Enchanted Artifacts
        items.addAll(createItemsFromProtactorate());//Need to be after Enchanted Artifacts
        items.addAll(createItemsFromSupportiveVestments()); //Need to be after Protactorate
        items.addAll(createItemsFromArcaneSanctrum());
        items.addAll(createItemsFromAncientWeaponary());

        Collections.sort(items, new Comparator<Item>() {
            @Override
            public int compare(Item lhs, Item rhs) {
                return lhs.getId() < rhs.getId() ? -1 : (lhs.getId() == rhs.getId() ? 0 : 1);
            }
        });

        for (Item i : items){
            for (Item item : i.getNeedForRecipeItems()){
                item.addUsedInItem(i);
            }
        }

        return items;
    }

    private static ArrayList<Item> createItemsFromSecretShop() {

        Shop shop = new Shop("Secret Shop", R.drawable.leragas_rad, R.drawable.leragas_dire);

        ArrayList<Item> items = new ArrayList<Item>();
        items.add(new Item(ItemContract.DEMON_EDGE_ID, R.drawable.demon_edge_lezvie_demona_dota, "Demon Edge", "Лезвие Демона", "2400")
                .addProperty("Урон: +46"));

        items.add(new Item(ItemContract.EAGLEHORN_ID, R.drawable.eaglehorn_luk_dota, "Eaglehorne", "Лук", "3300")
                .addProperty("Ловкость: +25")
                .addProperty("Броня: +3.6 (17%)")
                .addProperty("Скорость атаки: +25"));

        items.add(new Item(ItemContract.MESSERSHMIDTS_REAVER_ID, R.drawable.messerschmidt_s_reaver_messershmidt_dota, "Messerschmidts Reaver", "Мессершмидт", "3200")
                .addProperty("Сила: +25")
                .addProperty("HP: +475")
                .addProperty("Реген. HP: +0.75"));

        items.add(new Item(ItemContract.SACRED_RELIC_ID, R.drawable.sacred_relic_svyashchennaya_relikviya_dota, "Sacred Relic", "Священная реликвия", "3800")
                .addProperty("Урон: +46"));

        items.add(new Item(ItemContract.HYPERSTONE_ID, R.drawable.hyperstone_giperstoun_dota, "Hyperstone", "Гиперстоун", "2000")
                .addProperty("Скорость атаки: +55"));

        items.add(new Item(ItemContract.RING_OF_HEALTH_ID, R.drawable.ring_of_health_koltso_zdorovya_dota, "Ring of Health", "Кольцо здоровья", "875")
                .addProperty("Реген. HP: +5"));

        items.add(new Item(ItemContract.VOID_STONE_ID, R.drawable.void_stone_kamen_pustoti_dota, "Void Stone", "Камень пустоты", "875")
                .addProperty("Реген. MP: +100%"));

        items.add(new Item(ItemContract.MYSTIC_STAFF_ID, R.drawable.mystic_staff_misticheskiy_posoh_dota, "Mystic Staff", "Мистический посох", "2700")
                .addProperty("Интеллект: +25")
                .addProperty("MP: +325")
                .addProperty("Реген. MP: +1"));

        items.add(new Item(ItemContract.ENERGY_BOOSTER_ID, R.drawable.energy_booster_energeticheskiy_buster_dota, "Energy Booster", "Энергетический бустер", "1000")
                .addProperty("MP: +250"));

        items.add(new Item(ItemContract.POINT_BOOSTER_ID, R.drawable.point_booster_point_buster_dota, "Point Booster", "Поинт бустер", "1200")
                .addProperty("HP: +200")
                .addProperty("MP: +150"));

        items.add(new Item(ItemContract.VITALITY_BOOSTER_ID, R.drawable.vitality_booster_buster_zhizni_dota, "Vitality Booster", "Жизненный бустер", "1100")
                .addProperty("HP: +250"));

        items.add(new Item(ItemContract.ORB_OF_VENOM_ID, R.drawable.orb_of_venom_shar_yada_dota, "Orb of Venom", "Шар Яда", "275")
                .addProperty("Урон в секунду: +3")
                .addProperty("Замедление на 4 секунды: 4% для дальнего, 12% для ближнего боя"));

        for(Item i : items){
            i.setShop(shop);
            shop.addItem(i);
        }

        ShopLab.addShop(shop);

        return items;

    }

    private static ArrayList<Item> createItemsFromCacheOfTheQuelthelan() {

        Shop shop = new Shop("Gateway of Demonic Artifacts",R.drawable.cache_rad, R.drawable.cache_dire);

        ArrayList<Item> items = new ArrayList<Item>();

        items.add(new Item(ItemContract.GLOVES_OF_HASTE_ID, R.drawable.gloves_of_haste_perchatki_toroplivosti_dota, "Gloves of Haste", "Перчатки торопливости", "500")
                .addProperty("Скорость атаки: +15"));

        items.add(new Item(ItemContract.MASK_OF_DEATH_ID, R.drawable.mask_of_death_maska_smerti_dota, "Mask of Death", "Маска смерти", "900")
                .addProperty("Вампиризм: 15%")
                .addProperty("Орб эффект!"));

        items.add(new Item(ItemContract.RING_OF_REGENERATION_ID, R.drawable.ring_of_regeneration_koltso_regeneratsii_dota, "Ring of Regeneration", "Кольцо регенерации", "350")
                .addProperty("Реген. HP: +2"));

        items.add(new Item(ItemContract.KELENS_DAGGER_OF_ESCAPE_ID, R.drawable.kelen_s_dagger_of_escape_kinzhal_spaseniya_dota, "Kelens Dagger of Escape", "Даггер", "2100")
                .addProperty("Телепорт на 1200, 75 MP, 14 сек КД"));

        items.add(new Item(ItemContract.SOBI_MASK_ID, R.drawable.sobi_mask_maska_sobi_dota, "Sobi Mask",  "Маска Соби", "325")
                .addProperty("Реген. МР: +50%"));

        items.add(new Item(ItemContract.BOOTS_OF_SPEED_ID, R.drawable.boots_of_speed_sapogi_skorosti_dota, "Boots of Speed", "Сапоги скорости", "450")
                .addProperty("Скорость движения: +55"));

        items.add(new Item(ItemContract.GEM_OF_TRUE_SIGHT_ID, R.drawable.gem_of_true_sight_samotsvet_dota, "Gem of True Sight", "Гем", "900")
                .addProperty("Открывает скрытые объекты в зоне видимости героя")
                .addProperty("Выпадает при смерти героя"));

        items.add(new Item(ItemContract.PLANESWALKERS_CLOAK_ID, R.drawable.planeswalker_s_cloak_klobuk_maga_dota, "Planeswalkers Cloak", "Клобук мага", "550")
                .addProperty("Сопротивление магии: +15%"));

        items.add(new Item(ItemContract.SHADOW_AMULET_ID, R.drawable.shadow_amulet_amulet_nevidimosti_dota, "Shadow Amulet", "Амулет невидимости", "1600")
                .addProperty("Скорость атаки: +30")
                .addProperty("Невидимости, пока не начнешь двигаться (Активная)"));

        items.add(new Item(ItemContract.MAGIC_STICK_ID, R.drawable.magic_stick_volshebnaya_palochka_dota, "Magic Stick", "Волшебная палочка", "200")
                .addProperty("Собирает заряды, восстанавливающие 15 HP и MP"));

        items.add(new Item(ItemContract.TALISMAN_OF_EVASION_ID, R.drawable.talisman_of_evasion_talisman_ukloneniya_dota, "Talisman of Evasion", "Талисман уклонения", "1800")
                .addProperty("Шанс уклонения от атаки: +25%"));

        items.add(new Item(ItemContract.GHOST_SCEPTER_ID, R.drawable.ghost_scepter_prizrachniy_skipetr_dota, "Ghost Scepter", "Призрачный скипетр", "1600")
                .addProperty("Сила: +7")
                .addProperty("Ловкость: +7")
                .addProperty("Intelligence: +7"));

        for(Item i : items){
            i.setShop(shop);
            shop.addItem(i);
        }

        ShopLab.addShop(shop);

        return items;
    }

    private static ArrayList<Item> createItemsFromGraveyard(){

        Shop shop = new Shop("Chimaera Roost / Graveyard",R.drawable.chimaera_rad, R.drawable.chimaera_dier);

        ArrayList<Item> items = new ArrayList<Item>();

        items.add(new Item(ItemContract.CLARITY_POTION_ID, R.drawable.clarity_potion_otvar_prosvetlenya_dota, "Clarity Potion", "Отвар просветленья", "50")
            .addProperty("Реген. МР: 100MP за 30 сек"));

        items.add(new Item(ItemContract.HEALING_SALVE_ID, R.drawable.healing_salve_balzam_istseleniya_dota, "Healing Salve", "Бальзам исцеления", "100")
            .addProperty("Реген. HP: 400HP за 10 сек"));

        items.add(new Item(ItemContract.ANCIENT_TANGO_OF_ESSIFATION_ID, R.drawable.ancient_tango_of_essifation_drevnee_tango_dota, "Ancient Tango of Essifation", "Древнее танго", "90")
            .addProperty("Реген НР: 115НР за 16 сек, 3 заряда"));

        items.add(new Item(ItemContract.EMPTY_BOTTLE_ID, R.drawable.empty_bottle_pustaya_butil_dota, "Empty Bottle", "Пустая бутыль", "650")
            .addProperty("Может хранить заряды с 135HP и 70MP с фонтана, 3 штуки")
            .addProperty("Можно хранить маг. руны в течении 2 минут"));

        items.add(new Item(ItemContract.OBSERVER_WARDS_ID, R.drawable.observer_wards_nablyudatel_dota, "Observer Wards", "Обзервер варды", "150")
            .addProperty("Даёт видимость, обзор 1600, 6 мин, 2 заряда")
            .addProperty("Палится гемом и сентри вардом"));

        items.add(new Item(ItemContract.SENTRY_WARDS_ID, R.drawable.sentry_wards_karaul_dota, "Sentry Wards", "Сентри варды", "200")
            .addProperty("Даёт видимость скрытых объектов, радиус 950, 3 мин")
            .addProperty("Палится другим сентри или гемом"));

        items.add(new Item(ItemContract.DUST_OF_APPEARANCE_ID, R.drawable.dust_of_appearance_poroshok_dota, "Dust of Appearance", "Порошок", "180")
            .addProperty("Позволяет видеть невидимых героев")
            .addProperty("12 сек действует, перезарядка 60 сек, 2 заряда"));

        Item animalCourier = new Item(ItemContract.ANIMAL_COURIER_ID, R.drawable.animal_courier_kuritsa_dota, "Animal Courier", "Курьер", "150")
            .addProperty("Переносит вещи")
            .addProperty("При смерти вещи выпадают")
            .addProperty("Иммунитет к магии");
        items.add(animalCourier);

        items.add(new Item(ItemContract.SCROLL_OF_TOWN_PORTAL_ID, R.drawable.scroll_of_town_portal_teleport_dota, "Scroll of Town Portal", "Телепорт", "135")
            .addProperty("Телепорт к дружественному зданию, КД 65 сек")
            .addProperty("Двойной клик телепортирует к фонтану"));

        items.add(new Item(ItemContract.SMOKE_OF_DECEIT_ID, R.drawable.smoke_of_deceit_dim_obmana_dota, "Smoke of Deceit", "Дым обмана", "100")
            .addProperty("Невидимость в радиусе 1200")
            .addProperty("Скорость передвижения: +15%")
            .addProperty("Не работает вблизи вражеских героев и башен")
            .addProperty("КД 90 сек,  длится 40 сек")
            .addProperty("3 заряда, заряд пополняетс через 12 мин"));

        items.add(new Item(ItemContract.FLYING_COURIER_ID, R.drawable.flying_courier_letayushchiy_kurer_dota,
                "Flying Courier", "Летающая кура", "220(370)", true)
            .addProperty("Летает")
            .addNeedForRecipeItem(animalCourier));

        for(Item i : items){
            i.setShop(shop);
            shop.addItem(i);
        }
        ShopLab.addShop(shop);

        return items;
    }

    private static ArrayList<Item> createItemsFromSenaTheAccessorizer(){

        Shop shop = new Shop("Sena The Accessorizer",R.drawable.sena_rad, R.drawable.sena_dier);

        ArrayList<Item> items = new ArrayList<Item>();

        items.add(new Item(ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID, R.drawable.gauntlets_of_ogre_strength_rukavitsi_ogra_dota, "Gauntlets of Ogre Strength", "Рукавицы", "150")
            .addProperty("Сила: +3"));

        items.add(new Item(ItemContract.SLIPPERS_OF_AGILITY_ID, R.drawable.slippers_of_agility_tufli_lovkosti_dota, "Slippers of Agility", "Туфли ловкости", "150")
            .addProperty("Ловкость: +3"));

        items.add(new Item(ItemContract.MANTLE_OF_INTELLIGENCE_ID, R.drawable.mantle_of_intelligence_mantiya_uchenogo_dota, "Mantle of Intelligence", "Мантия учёного", "150")
            .addProperty("Интеллект: +3"));

        items.add(new Item(ItemContract.IRONWOOD_BRANCH_ID, R.drawable.ironwood_branch_zheleznaya_vetv_dota, "Ironwood Branch", "Ветка", "53")
            .addProperty("Сила: +1")
            .addProperty("Ловкость: +1")
            .addProperty("Интеллигент + 1"));

        items.add(new Item(ItemContract.BELT_OF_GIANT_STRENGTH_ID, R.drawable.belt_of_giant_strength_poyas_gigantskoy_sili_dota, "Belt of Giant Strength", "Пояс гигантской силы", "450")
            .addProperty("Сила: +6"));

        items.add(new Item(ItemContract.BOOTS_OF_ELVENSKIN_ID, R.drawable.boots_of_elvenskin_sapogi_elfa_dota, "Boots of Elvenskin", "Сапоги эльфа", "450")
            .addProperty("Ловкость: +6"));

        items.add(new Item(ItemContract.ROBE_OF_THE_MAGI_ID, R.drawable.robe_of_the_magi_mantiya_maga_dota, "Robe of the magi", "Мантия мага", "450")
            .addProperty("Интеллект: +6"));

        items.add(new Item(ItemContract.CIRCLET_OF_NOBILITY_ID, R.drawable.circlet_of_nobility_venets_blagorodiya_dota, "Circlet of Nobility", "Венец благородия", "185")
             .addProperty("Сила: +2")
             .addProperty("Ловкость: +2")
             .addProperty("Интеллигент +2"));

        items.add(new Item(ItemContract.OGRE_AXE_ID, R.drawable.ogre_axe_topor_ogra_dota, "Ogre Axe", "Топор огра", "1000")
            .addProperty("Сила: +10"));

        items.add(new Item(ItemContract.BLADE_OF_ALACRITY_ID, R.drawable.blade_of_alacrity_klinok_provorstva_dota, "Blade of Alacrity", "Клинок проворства", "1000")
            .addProperty("Ловкость: +10"));

        items.add(new Item(ItemContract.STAFF_OF_WIZARDY_ID, R.drawable.staff_of_wizardry_posoh_volshebstva_dota, "Staff of Wizardy", "Посох волщебства", "1000")
            .addProperty("Интеллект: +10"));

        items.add(new Item(ItemContract.ULTIMATE_ORB_ID, R.drawable.ultimate_orb_ult_shar_dota, "Ultimate Orb", "Ульт-шар", "2100")
                .addProperty("Сила: +10")
                .addProperty("Ловкость: +10")
                .addProperty("Интеллигент +10"));

        for(Item i : items) {
            i.setShop(shop);
            shop.addItem(i);
        }

        ShopLab.addShop(shop);

        return items;
    }

    private static ArrayList<Item> createItemsFromBeazelTheWeaponsDealer(){

        Shop shop = new Shop("Beazel The Weapons Dealer", R.drawable.beazel_rad, R.drawable.beazel_dier);

        ArrayList<Item> items = new ArrayList<Item>();

        items.add(new Item(ItemContract.BLADES_OF_ATTACK_ID, R.drawable.blades_of_attack_nozhi_dota, "Blades of Attack", "Ножи", "450")
            .addProperty("Урон: +9"));

        items.add(new Item(ItemContract.BROADSWORD_ID, R.drawable.broadsword_mech_dota, "Broadsword", "Меч", "1200")
            .addProperty("Урон: +18"));

        items.add(new Item(ItemContract.QUARTERSTAFF_ID, R.drawable.quarterstaff_dubina_dota, "Quarterstaff", "Дубина", "900")
            .addProperty("Урон: +10")
            .addProperty("Скорость атаки: +10"));

        items.add(new Item(ItemContract.CLAYMORE_ID, R.drawable.claymore_palash_dota, "Claymore", "Палаш", "1400")
            .addProperty("Урон: +21"));

        items.add(new Item(ItemContract.RING_OF_PROTECTION_ID, R.drawable.ring_of_protection_koltso_zashchiti_dota, "Ring of Protection", "Кольцо защиты", "175")
            .addProperty("Броня: +2(10%)"));

        items.add(new Item(ItemContract.STOUT_SHIELD_ID, R.drawable.stout_shield_krepkaya_bronya_dota, "Stout Shield", "Крепкая броня", "250")
            .addProperty("Блокирует 20 урона наносимый герою ближнего боя")
            .addProperty("Блокирует 20 урона наносимый герою ближнего боя")
            .addProperty("Шанс блока: 60%"));

        items.add(new Item(ItemContract.JAVELIN_ID, R.drawable.javelin_drotik_dota, "Javelin", "Дротик", "1500")
            .addProperty("Урон: +21")
            .addProperty("20% шанс нанести ещё 40 урона"));

        items.add(new Item(ItemContract.MITHRIL_HAMMER_ID, R.drawable.mithril_hammer_mitriloviy_molot_dota, "Mithrill Hammer", "Митриловый молоток", "1600")
            .addProperty("Урон: +24"));

        items.add(new Item(ItemContract.CHAINMAIL_ID, R.drawable.chainmail_kolchuga_dota, "Chainmail", "Кольчуга", "550")
            .addProperty("Броня: +5(23%)"));

        items.add(new Item(ItemContract.HELM_OF_IRON_WILL_ID, R.drawable.helm_of_iron_will_shlem_zheleznoy_voli_dota, "Helm of Iron Will", "Шлем железной воли", "950")
            .addProperty("Реген. HP: +3")
            .addProperty("Броня: +5(23%)"));

        items.add(new Item(ItemContract.PLATE_MAIL_ID, R.drawable.plate_mail_dospehi_dota, "Plate Mail", "Доспехи", "1400")
            .addProperty("Броня: +10(37%)"));

        items.add(new Item(ItemContract.QUELLING_BLADE_ID, R.drawable.quelling_blade_podavlyayushchee_lezvie_dota, "Quelling Blade", "Подавляющее Лезвие", "225")
            .addProperty("Позволяет рубить деревья")
            .addProperty("Урон по крипам для героев ближнего боя: +32%")
            .addProperty("Урон по крипам для героев дальнего боя: +12%"));

        for(Item i : items){
            i.setShop(shop);
            shop.addItem(i);
        }

        ShopLab.addShop(shop);

        return items;
    }

    private static ArrayList<Item> createItemsFromGatewayRelics(){

        Shop shop = new Shop("Gateway Relics", R.drawable.gateway_rad, R.drawable.gateway_dier);
        ArrayList<Item> items = new ArrayList<Item>();

        items.add(new Item(ItemContract.BOOTS_OF_TRAVEL_ID, R.drawable.boots_of_travel_sapogi_puteshestviya_dota,
                "Boots of Travel", "Сапоги путешествия", "2000(2450)", true)
            .addProperty("Скорость движения: +100")
            .addProperty("Активная: перемещает к друж. юниту(не герою)")
            .addProperty("КД 60сек, 75 МР, время заклинания: 3 сек")
            .addNeedForRecipeItem(ItemContract.BOOTS_OF_SPEED_ID));

        items.add(new Item(ItemContract.PHASE_BOOTS_ID, R.drawable.phase_boots_botinki_fazi_dota, "Phase Boots",
                "Ботинки Фазы", "0(1350)")
            .addProperty("Урон: +24")
            .addProperty("Скорость движения: +50")
            .addProperty("Активная: передвежение сквозь юнитов, +16% скорость")
            .addProperty("длится 4 секунды, КД: 8 сек")
            .addNeedForRecipeItem(ItemContract.BOOTS_OF_SPEED_ID)
            .addNeedForRecipeItem(ItemContract.BLADES_OF_ATTACK_ID)
            .addNeedForRecipeItem(ItemContract.BLADES_OF_ATTACK_ID));

        items.add(new Item(ItemContract.POWER_THREADS_ID, R.drawable.power_threads_strength_moshch_shagov_kostil_dota,
                "Power Threads (Strength)", "Костыль", "0(1400)")
            .addProperty("Сила: +8")
            .addProperty("Скорость атаки: +30")
            .addProperty("Скорость движения: +50")
            .addNeedForRecipeItem(ItemContract.BOOTS_OF_SPEED_ID)
            .addNeedForRecipeItem(ItemContract.BELT_OF_GIANT_STRENGTH_ID)
            .addNeedForRecipeItem(ItemContract.GLOVES_OF_HASTE_ID));

        items.add(new Item(ItemContract.SOUL_RING_ID, R.drawable.soul_ring_koltso_dushi_dota,
                "Soul Ring", "Кольцо Души", "125(800)", true)
            .addProperty("Реген. HP: +3")
            .addProperty("Реген. MP: +50%")
            .addProperty("Активная: 150MP в обмен на 150HP? КД 30 сек")
            .addProperty("Мана исчезнет, если не использовать за 10 сек")
            .addNeedForRecipeItem(ItemContract.RING_OF_REGENERATION_ID)
            .addNeedForRecipeItem(ItemContract.SOBI_MASK_ID));

        items.add(new Item(ItemContract.HAND_OF_MIDAS_ID, R.drawable.hand_of_midas_ruka_midasa_dota,
                "Hand of Midas", "Рука Мидаса", "1550(2050)", true)
            .addProperty("Скорость атаки: +30")
            .addProperty("Актив.: превращает юнита в 190 золота и 1.5x опыта")
            .addProperty("КД: 100сек")
            .addNeedForRecipeItem(ItemContract.GLOVES_OF_HASTE_ID));

        items.add(new Item(ItemContract.OBLIVION_STAFF_ID, R.drawable.oblivion_staff_posoh_nebrezhnosti_dota,
                "Oblivion Staff", "Посох небрежности", "0(1675)")
            .addProperty("Скорость атаки: +10")
            .addProperty("Интеллект: +6")
            .addProperty("Урон: +15")
            .addProperty("Реген. MP: +75%")
            .addNeedForRecipeItem(ItemContract.QUARTERSTAFF_ID)
            .addNeedForRecipeItem(ItemContract.SOBI_MASK_ID)
            .addNeedForRecipeItem(ItemContract.ROBE_OF_THE_MAGI_ID));

        items.add(new Item(ItemContract.PERSEVERANCE_ID, R.drawable.perseverance_sokrovishche_dota,
                "Perseverance", "Сокровище", "0(1750)")
            .addProperty("Реген. HP: +5")
            .addProperty("Урон: +10")
            .addProperty("Реген. MP: +125%")
            .addNeedForRecipeItem(ItemContract.VOID_STONE_ID)
            .addNeedForRecipeItem(ItemContract.RING_OF_HEALTH_ID));

        items.add(new Item(ItemContract.POOR_MANS_SHIELD_ID, R.drawable.poor_man_s_shield_shchit_bednyaka_dota,
                "Poor Mans Shield", "Щит бедняка", "0(550)")
            .addProperty("Ловкость: +6")
            .addProperty("100% шанс блокировать урон от героя")
            .addProperty("60% шанс блокировать урон не от героя")
            .addProperty("20 урона для ближнего боя, 10 для дальнего")
            .addNeedForRecipeItem(ItemContract.STOUT_SHIELD_ID)
            .addNeedForRecipeItem(ItemContract.SLIPPERS_OF_AGILITY_ID)
            .addNeedForRecipeItem(ItemContract.SLIPPERS_OF_AGILITY_ID));

        items.add(new Item(ItemContract.BRACER_ID, R.drawable.bracer_narukavnik_dota,
                "Bracer", "Нарукавник", "190(505)", true)
            .addProperty("Сила: +6")
            .addProperty("Ловкость: +3")
            .addProperty("Интеллект: +3")
            .addNeedForRecipeItem(ItemContract.CIRCLET_OF_NOBILITY_ID)
            .addNeedForRecipeItem(ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID));

        items.add(new Item(ItemContract.WRAITH_BAND_ID, R.drawable.wraith_band_maska_duha_dota,
                "Wraith Band", "Маска духа", "150(505)", true)
                .addProperty("Сила: +3")
                .addProperty("Ловкость: +6")
                .addProperty("Интеллект: +3")
                .addNeedForRecipeItem(ItemContract.CIRCLET_OF_NOBILITY_ID)
                .addNeedForRecipeItem(ItemContract.SLIPPERS_OF_AGILITY_ID));

        items.add(new Item(ItemContract.NULL_TALISMAN_ID, R.drawable.null_talisman_null_talisman_dota,
                "Null Talisman", "Нулл талисман", "135(450)", true)
                .addProperty("Сила: +3")
                .addProperty("Ловкость: +3")
                .addProperty("Интеллект: +6")
                .addNeedForRecipeItem(ItemContract.CIRCLET_OF_NOBILITY_ID)
                .addNeedForRecipeItem(ItemContract.MANTLE_OF_INTELLIGENCE_ID));

        items.add(new Item(ItemContract.MAGIC_WAND_ID, R.drawable.magic_wand_volshebnaya_palochka_dota,
                "Magic Wand", "Волшебная палочка", "150(500)", true)
                .addProperty("Сила: +3")
                .addProperty("Ловкость: +3")
                .addProperty("Интеллект: +3")
                .addProperty("Активная: 15 MP и HP за заряд")
                .addProperty("Заряды появляются при касте спелл возле вас")
                .addProperty("Максимум 17 зарядов")
                .addNeedForRecipeItem(ItemContract.MAGIC_STICK_ID)
                .addNeedForRecipeItem(ItemContract.IRONWOOD_BRANCH_ID)
                .addNeedForRecipeItem(ItemContract.IRONWOOD_BRANCH_ID)
                .addNeedForRecipeItem(ItemContract.IRONWOOD_BRANCH_ID));

        for(Item i : items){
            i.setShop(shop);
            shop.addItem(i);
        }

        ShopLab.addShop(shop);

        return items;
    }

    private static ArrayList<Item> createItemsFromSupportiveVestments(){

        Shop shop = new Shop("Supportive Vestments", R.drawable.support_rad, R.drawable.support_dier);

        ArrayList<Item> items = new ArrayList<>();

        Item headdressOfRejuvenation = new Item(ItemContract.HEADDRESS_OF_REJUVENATION_ID, R.drawable.headdress_of_rejuvenation_molodilnoe_odeyanie_dota,
                "Headdress of Rejuvenation", "Молодильное одеяние", "200(600)", true)
                .addProperty("Сила: +2")
                .addProperty("Ловкость: +2")
                .addProperty("Интеллект: +2")
                .addProperty("Аура здоровья: +3HP в радиусе 750")
                .addNeedForRecipeItem(ItemContract.IRONWOOD_BRANCH_ID)
                .addNeedForRecipeItem(ItemContract.RING_OF_REGENERATION_ID);
        items.add(headdressOfRejuvenation);

        Item netherizmBuckler = new Item(ItemContract.NETHERIZM_BUCKLER_ID, R.drawable.netherezim_buckler_shchit_nezerezima_dota,
                "Netherizim Buckler", "Щит Незеризма", "200(800)", true)
                .addProperty("Сила: +2")
                .addProperty("Ловкость: +2")
                .addProperty("Интеллект: +2")
                .addProperty("Активная: +2 армора на 25 сек в радиусе 750, стоит 10MP, КД: 25сек")
                .addNeedForRecipeItem(ItemContract.IRONWOOD_BRANCH_ID)
                .addNeedForRecipeItem(ItemContract.CHAINMAIL_ID);
        items.add(netherizmBuckler);

        items.add(new Item(ItemContract.MEKANSM_ID, R.drawable.mekansm_mekanzm_dota,
                "Mekansm", "Меканзм", "900(2300)", true)
                .addProperty("Сила: +5")
                .addProperty("Ловкость: +5")
                .addProperty("Интеллект: +5")
                .addProperty("Реген. HP у друж. юнитов в радиусе 900: +4")
                .addProperty("Активная: +2 армора на 25 сек друж. юнитам и разово восстанавливает 250HP, стоит 150MP")
                .addNeedForRecipeItem(headdressOfRejuvenation)
                .addNeedForRecipeItem(netherizmBuckler));

        Item ringOfBasilius = new Item(ItemContract.RING_OF_BASILIUS_ID, R.drawable.ring_of_basilius_koltso_baziliusa_dota,
                "Ring of Basilius", "Кольцо Базилиуса", "0(525)")
                .addProperty("Аура магии: +0.65МР в радиусе 900")
                .addProperty("Аура армора: +2 в радиусе 900")
                .addProperty("Урон: +6")
                .addNeedForRecipeItem(ItemContract.SOBI_MASK_ID)
                .addNeedForRecipeItem(ItemContract.RING_OF_PROTECTION_ID);

        items.add(ringOfBasilius);

        items.add(new Item(ItemContract.VLADMIRS_OFFERING_ID, R.drawable.vladimir_s_offering_podarok_vlada_dota,
                "Vladmirs Offering", "Подарок Влада", "300(2075)", true)
            .addProperty("Реген. HP(Аура): +2")
            .addProperty("Броня(Аура): +5(23%)")
            .addProperty("Реген. MP(Аура): +0.8")
            .addProperty("Аура вампиризма: +16%")
            .addProperty("Аура атаки: +15%")
            .addNeedForRecipeItem(ItemContract.MASK_OF_DEATH_ID)
            .addNeedForRecipeItem(ItemContract.RING_OF_REGENERATION_ID)
            .addNeedForRecipeItem(ringOfBasilius));

        items.add(new Item(ItemContract.ARCANE_BOOTS_ID, R.drawable.arcane_boots_temnie_sapogi_dota,
                "Arcane Boots", "Темные сапоги", "0(1450)")
            .addProperty("MP: +250")
            .addProperty("Скорость движения: +55")
            .addProperty("Активная: +135MP себе и союзникам в радиусе 600, стоит 25 MP, КД: 55сек")
            .addNeedForRecipeItem(ItemContract.ENERGY_BOOSTER_ID)
            .addNeedForRecipeItem(ItemContract.BOOTS_OF_SPEED_ID));

        items.add(new Item(ItemContract.RING_OF_AQUILA_ID, R.drawable.ring_of_aquila_koltso_orla_dota,
                "Ring of Aquila", "Кольцо Орла", "0(990)")
                .addProperty("Сила: +3")
                .addProperty("Ловкость: +6")
                .addProperty("Интеллект: +3")
                .addProperty("+2 армора на расстоянии 900")
                .addProperty("+0.65 MP на расстоянии 900")
                .addNeedForRecipeItem(ItemContract.WRAITH_BAND_ID)
                .addNeedForRecipeItem(ringOfBasilius));


        items.add(new Item(ItemContract.KHADGARS_PIPE_OF_INSIGHT_ID, R.drawable.khadgar_s_pipe_of_insight_trubka_prosvetleniya_dota,
                "Khadgars Pipe of Insight", "Трубка просветления", "800(3525)", true)
            .addProperty("Реген. HP: +11")
            .addProperty("Сопротивление магии: +30")
            .addProperty("Активная: щит от маны (блок 400 урона), в радиусе 900, стоит 100MP")
            .addNeedForRecipeItem(ItemContract.HOOD_OF_DEFIANCE_ID)
                .addNeedForRecipeItem(headdressOfRejuvenation));

        items.add(new Item(ItemContract.URN_OF_SHADOWS_ID, R.drawable.urn_of_shadows_urna_teney_dota,
                "Urn of Shadows", "Урна Теней", "250(875)", true)
            .addProperty("Сила: +6")
            .addProperty("Реген. МРЖ +50%")
            .addProperty("Активная: восстановление союзнику 400HP за 8сек, КД 7сек")
            .addProperty("За смерть вражеского героя добавляется заряд")
            .addNeedForRecipeItem(ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID)
            .addNeedForRecipeItem(ItemContract.GAUNTLETS_OF_OGRE_STRENGTH_ID)
            .addNeedForRecipeItem(ItemContract.SOBI_MASK_ID));


        items.add(new Item(ItemContract.MEDALLION_OF_COURAGE_ID, R.drawable.medallion_of_courage_medalen_hrabrosti_dota,
                "Medallion of Courage", "Медальон Храбрости", "325(1200)", true)
            .addProperty("Броня: +6(26%)")
            .addProperty("Реген. МР: +50%")
            .addProperty("Активная: Увеличение(уменьшение) брони себе (себе и врагу) на 7 на 7сек, КД: 7сек")
            .addNeedForRecipeItem(ItemContract.CHAINMAIL_ID)
            .addNeedForRecipeItem(ItemContract.SOBI_MASK_ID));

        items.add(new Item(ItemContract.ANCIENT_JANGGO_OF_ENDURANCE_ID, R.drawable.ancient_janggo_of_endurance_drevniy_buben_vinoslivosti_dota,
                "Ancient Janggo of Endurance", "Древний бубен Выносливости", "800(1755)", true)
            .addProperty("Сила: +9")
            .addProperty("Ловкость: +9")
            .addProperty("Интелеект: +9")
            .addProperty("Скорость атаки: +5% в радиусе 900")
            .addProperty("Скорость перемещения: +5% в радиусе 900")
            .addProperty("Активная: +10% к скорости атаки и передвижения на 6 сек в радиусе 900, КД: 30сек")
            .addProperty("6 зарядов, перезаряжается на 6 зарядов при покупке рецепта(повторной)")
            .addNeedForRecipeItem(ItemContract.BRACER_ID)
            .addNeedForRecipeItem(ItemContract.ROBE_OF_THE_MAGI_ID));

        items.add(new Item(ItemContract.TRANQUIL_BOOTS_ID, R.drawable.tranquil_boots_tihie_sapogi_dota,
                "Tranquil Boots", "Тихие сапоги", "0(1000)")
            .addProperty("Реген. HP: +10")
            .addProperty("Скорость движения: +85")
            .addProperty("Броня: +4(19%)")
            .addProperty("При атаке реген прекращается, скорость падает до 60")
            .addNeedForRecipeItem(ItemContract.BOOTS_OF_SPEED_ID)
            .addNeedForRecipeItem(ItemContract.RING_OF_PROTECTION_ID)
            .addNeedForRecipeItem(ItemContract.RING_OF_REGENERATION_ID));

        for(Item i : items){
            i.setShop(shop);
            shop.addItem(i);
        }

        ShopLab.addShop(shop);

        return items;
    }

    private static ArrayList<Item> createItemsFromProtactorate(){

        Shop shop = new Shop("Protactorate", R.drawable.protactorate_rad, R.drawable.protactorate_dier);

        ArrayList<Item> items = new ArrayList<>();

        items.add(new Item(ItemContract.ASSAULT_CUIRAS_ID, R.drawable.assault_cuirass_kirasa_napadeniya_dota,
                "Assault Cuirass", "Кираса Нападения", "1300(5250)", true)
            .addProperty("Броня: +15(47%)")
            .addProperty("Скорость атаки: +55")
            .addProperty("Понижение брони врагам: -5%")
            .addNeedForRecipeItem(ItemContract.PLATE_MAIL_ID)
            .addNeedForRecipeItem(ItemContract.CHAINMAIL_ID)
            .addNeedForRecipeItem(ItemContract.HYPERSTONE_ID));

        items.add(new Item(ItemContract.HEART_OF_TARRASQUE_ID, R.drawable.heart_of_tarrasque_tarasik_dota,
                "Heart of Tarrasque", "Тараска", "1200(2500)", true)
            .addProperty("Сила: +40")
            .addProperty("Реген. НР: +2%")
            .addProperty("Не работает 6 сек после урона у ближников и 4 сек у дальников")
            .addNeedForRecipeItem(ItemContract.MESSERSHMIDTS_REAVER_ID)
            .addNeedForRecipeItem(ItemContract.VITALITY_BOOSTER_ID));

        items.add(new Item(ItemContract.BLACK_KING_BAR_ID, R.drawable.black_king_bar_cherniy_posoh_bkb_dota,
                "Black King Bar", "Чёрный посох Короля", "1375(3975)", true)
            .addProperty("Сила: +10")
            .addProperty("Урон: +24")
            .addProperty("Активная: Невосприимчивость к магии и стану на 10/9/.../4 секунд (по очереди применения)")
            .addProperty("КД: 80/75/.../50 секунд, затем останавливается на 4 сек и 50сек КД")
            .addNeedForRecipeItem(ItemContract.MITHRIL_HAMMER_ID)
            .addNeedForRecipeItem(ItemContract.OGRE_AXE_ID));

        items.add(new Item(ItemContract.SHIVAS_GUARD_ID, R.drawable.shiva_s_guard_zashchita_shivi_dota,
                "Shiva's Guard", "Защита Шивы", "600(4700)", true)
            .addProperty("Броня: +15(47%)")
            .addProperty("Интеллект: +30")
            .addProperty("Аура замедления скор. атаки противника: -40%, рад 900")
            .addProperty("Активная: 200 урона в рад 900, -40% скорость всех врагов, КД: 30сек, стоит 100МР")
            .addNeedForRecipeItem(ItemContract.PLATE_MAIL_ID)
            .addNeedForRecipeItem(ItemContract.MYSTIC_STAFF_ID));

        Item soulBooster = new Item(ItemContract.SOUL_BOOSTER_ID, R.drawable.soul_buster_buster_serdtsa_dota,
                "Soul Buster", "Бустер сердца", "0(3300)")
                .addProperty("НР: +450")
                .addProperty("Реген. НР: +4")
                .addProperty("МР: +400")
                .addProperty("Реген. МР: +100%")
                .addNeedForRecipeItem(ItemContract.ENERGY_BOOSTER_ID)
                .addNeedForRecipeItem(ItemContract.POINT_BOOSTER_ID)
                .addNeedForRecipeItem(ItemContract.VITALITY_BOOSTER_ID);

        items.add(soulBooster);

        items.add(new Item(ItemContract.BLOODSTONE_ID, R.drawable.bloodstone_kamen_krovi_dota,
                "Bloodstone", "Камень крови", "0(5050)")
                .addProperty("НР: +500")
                .addProperty("Реген. НР: +9")
                .addProperty("МР: +400")
                .addProperty("Реген. МР: +200%")
                .addProperty("Урон: +10")
                .addProperty("в начале предмет обладает 8 зарядами. Когда герой-носитель умирает окружающим союзникам" +
                        " восстанавливается 400 жизни +30 за каждый заряд на расстоянии 1675")
                .addProperty("Опыт и обзор на месте своей смерти до воскрешения: дает обзор в радиусе 1800 и дает опыт в радиусе 1000")
                .addProperty("Каждый заряд уменьшает на 25 монет потерю золота при смерти")
                .addProperty("Каждый заряд уменьшает на 4 секунды время воскрешения при смерти")
                .addProperty("Заряды пополняются при убийстве/смерти героя на расстоянии 1600")
                .addProperty("При смерти теряется половина зарядов. Можно разложить в круге силы.")
                .addNeedForRecipeItem(soulBooster)
                .addNeedForRecipeItem(ItemContract.PERSEVERANCE_ID));

        items.add(new Item(ItemContract.LINKENS_SPHERE_ID, R.drawable.linken_s_sphere_linkin_dota,
                "Linken's Sphere", "Линка", "1325(5175)", true)
            .addProperty("Сила: +15")
            .addProperty("Ловкость: +15")
            .addProperty("Интеллект: +15")
            .addProperty("Урон: +10")
            .addProperty("Реген. МР: +150%")
            .addProperty("Защит от заклинания, КД 17 сек")
            .addNeedForRecipeItem(ItemContract.ULTIMATE_ORB_ID)
            .addNeedForRecipeItem(ItemContract.PERSEVERANCE_ID));

        items.add(new Item(ItemContract.VANGUARD_ID, R.drawable.vanguard_shchit_avangarda_dota,
                "Vanguard", "Щит авангарда", "0(2225)")
            .addProperty("HP: +250")
            .addProperty("Реген. НР: +6")
            .addNeedForRecipeItem(ItemContract.RING_OF_HEALTH_ID)
            .addNeedForRecipeItem(ItemContract.STOUT_SHIELD_ID)
            .addNeedForRecipeItem(ItemContract.VITALITY_BOOSTER_ID));

        items.add(new Item(ItemContract.AEGIS_OF_IMMORTAL_ID, R.drawable.aegis_of_the_immortal_shchit_bessmertiya_aegis_dota, "Aegis of Immortal", "Аегис", "0")
            .addProperty("Даёт вторую жизнь")
            .addProperty("Выпадает из Рошана")
            .addProperty("Зачем ты читаешь это о_О?"));

        items.add(new Item(ItemContract.BLADE_MAIL_ID, R.drawable.blade_mail_ostraya_kolchuga_dota,
                "Blade Mail", "Обратка", "0(2200)")
            .addProperty("Броня: +6(26%)")
            .addProperty("Интеллект: +10")
            .addProperty("Урон: +22")
            .addProperty("Активная: возврат урона врагу (100%), длится 4.5сек, КД 17сек, стоит 25МР")
            .addNeedForRecipeItem(ItemContract.CHAINMAIL_ID)
            .addNeedForRecipeItem(ItemContract.BROADSWORD_ID)
            .addNeedForRecipeItem(ItemContract.ROBE_OF_THE_MAGI_ID));

        items.add(new Item(ItemContract.HOOD_OF_DEFIANCE_ID, R.drawable.hood_of_defiance_plashch_prenebrezheniya_dota,
                "Hood of Deviance", "Плащ Пренебрежения", "0(2125)")
            .addProperty("Реген. НР: +8")
            .addProperty("Сопротивление магии: +30")
            .addNeedForRecipeItem(ItemContract.PLANESWALKERS_CLOAK_ID)
            .addNeedForRecipeItem(ItemContract.RING_OF_REGENERATION_ID)
            .addNeedForRecipeItem(ItemContract.RING_OF_REGENERATION_ID)
            .addNeedForRecipeItem(ItemContract.RING_OF_HEALTH_ID));

        items.add(new Item(ItemContract.MANTA_STYLE_ID, R.drawable.manta_style_manta_dota,
                "Manta Style", "Манта", "900(5050)", true)
            .addProperty("Сила: +10")
            .addProperty("Ловкость: +26")
            .addProperty("Интеллект: +10")
            .addProperty("Скорость передвижения: +10%")
            .addProperty("Активная: Создание 2 иллюзий, длятся 20сек, стоят 165МР, КД: 50сек, наносят 33(28)% урона и получают 350(400)% для ближников(дальников)")
            .addNeedForRecipeItem(ItemContract.ULTIMATE_ORB_ID)
            .addNeedForRecipeItem(ItemContract.YASHA_ID));

        for(Item i : items){
            i.setShop(shop);
            shop.addItem(i);
        }

        ShopLab.addShop(shop);

        return items;
    }

    private static ArrayList<Item> createItemsFromEnchantedArtifacts(){

        Shop shop = new Shop("Enchanted Artifacts", R.drawable.enchanted_rad, R.drawable.enchanted_dier);

        ArrayList<Item> items = new ArrayList<>();

        Item yasha = new Item(ItemContract.YASHA_ID, R.drawable.yasha_yasha_dota,
                "Yasha", "Яша", "600(2050)", true)
            .addProperty("Ловкость: +16")
            .addProperty("Скорость атаки: +31")
            .addProperty("Скорость передвижения: +10%")
            .addProperty("Броня: +31")
            .addNeedForRecipeItem(ItemContract.BLADE_OF_ALACRITY_ID)
            .addNeedForRecipeItem(ItemContract.BOOTS_OF_ELVENSKIN_ID);
        items.add(yasha);

        Item sange = new Item(ItemContract.SANGE_ID, R.drawable.sange_sendzh_dota,
                "Sange", "Саша", "600(2500)", true)
            .addProperty("Сила: +16")
            .addProperty("Урон: +10")
            .addProperty("15% шанс нанести удар, отнимающий у врага 20% скорости атаки и движения на 4 секунды")
            .addProperty("Орб-эффект!")
            .addNeedForRecipeItem(ItemContract.OGRE_AXE_ID)
            .addNeedForRecipeItem(ItemContract.BELT_OF_GIANT_STRENGTH_ID);
        items.add(sange);

        items.add(new Item(ItemContract.SANGE_AND_YASHA_ID, R.drawable.sange_yasha_sendzh_i_yasha_dota,
                "Sange & Yasha", "Саша Яша", "0(4100)")
                .addProperty("Сила: +16")
                .addProperty("Ловкость: +16")
                .addProperty("Урон: +16")
                .addProperty("Скорость передвижения: +16%")
                .addProperty("16% шанс нанести удар, отнимающий у врага 30% скорости атаки и движения на 4 секунды")
                .addProperty("Орб-эффект!")
                .addNeedForRecipeItem(yasha)
                .addNeedForRecipeItem(sange));

        Item helmOfDominator = new Item(ItemContract.HELM_OF_THE_DOMINATOR_ID, R.drawable.helm_of_the_dominator_shlem_dominirovaniya_dota,
                "Helm of the Dominator", "Шлем доминирования", "0(1850)")
            .addProperty("Вампиризм: +20")
            .addProperty("Броня: +5(23%)")
            .addProperty("Урон: +20")
            .addProperty("Активная: подчиняет нейтрального крипа на 10 мин, увеличивая ему хп на 500, стоит 75МР")
            .addProperty("Орб-эффект!")
            .addNeedForRecipeItem(ItemContract.HELM_OF_IRON_WILL_ID)
            .addNeedForRecipeItem(ItemContract.MASK_OF_DEATH_ID);
        items.add(helmOfDominator);

        items.add(new Item(ItemContract.SATANIC_ID, R.drawable.satanic_satanik_dota,
                "Satanic", "Сатаник", "1100(6150)", true)
            .addProperty("Сила: +25")
            .addProperty("Урон: +20")
            .addProperty("Броня: +5(23%)")
            .addProperty("Вампиризм: +25%")
            .addProperty("Активная: 200% вампиризма, длится 3.5 секунд. КД: 35 секунд")
            .addNeedForRecipeItem(ItemContract.MESSERSHMIDTS_REAVER_ID)
            .addNeedForRecipeItem(helmOfDominator));

        Item maelstorm = new Item(ItemContract.MAELSTORM_ID, R.drawable.maelstrom_vihr_dota,
                "Maelstrom", "Вихрь", "600(2700)", true)
            .addProperty("Скорость атаки: +25")
            .addProperty("Урон: +24")
            .addProperty("25% шанс вызвать молнию (120 урона по 4 целям")
            .addProperty("Орб-эффект(сочетается с другими, перекрывает только при срабатывании молнии")
            .addNeedForRecipeItem(ItemContract.GLOVES_OF_HASTE_ID)
            .addNeedForRecipeItem(ItemContract.MITHRIL_HAMMER_ID);
        items.add(maelstorm);

        items.add(new Item(ItemContract.MJOLNIR_ID, R.drawable.mjollnir_melnir_dota,
                "Mjollnir", "Мьёльнир", "600(5300)", true)
            .addProperty("Скорость атаки: +80")
            .addProperty("Урон: +24")
            .addProperty("25% шанс вызвать молнию, (200 урона по 8 целям)")
            .addProperty("Орб эффект!, но перекрывает другие орбы только во время молнии")
            .addProperty("Активная: 20% шанса возвратить 200 урона атакуюшему и рядом стоящим врагам(до 4 целей), длится 20сек, КД 35, тратит 50МР")
            .addNeedForRecipeItem(ItemContract.HYPERSTONE_ID)
            .addNeedForRecipeItem(maelstorm));

        items.add(new Item(ItemContract.EYE_OF_SCADI_ID, R.drawable.eye_of_skadi_glaz_skadi_dota,
                "Eye of Skadi", "Глаз Скади", "0(5675)")
            .addProperty("Сила: +25")
            .addProperty("Ловкость: +25")
            .addProperty("Интеллект: +25")
            .addProperty("Возможность при каждой атаке заморозить цель.(-35% скорость атаки и движения), длится 3 секунды для дальников, 5 для ближников")
            .addNeedForRecipeItem(ItemContract.ULTIMATE_ORB_ID)
            .addNeedForRecipeItem(ItemContract.POINT_BOOSTER_ID)
            .addNeedForRecipeItem(ItemContract.ORB_OF_VENOM_ID));

        items.add(new Item(ItemContract.STYGIAN_DESOLATOR_ID, R.drawable.stygian_desolator_stigian_desolyator_dota,
                "Stygian Desolator", "Дезолятор", "900(4100)", true)
            .addProperty("Урон: +60")
            .addProperty("Снижение защиты атакуемого юнита на 7 на 15 сек. Работает на строениях")
            .addProperty("Орб-эффект")
            .addNeedForRecipeItem(ItemContract.MITHRIL_HAMMER_ID)
            .addNeedForRecipeItem(ItemContract.MITHRIL_HAMMER_ID));

        items.add(new Item(ItemContract.MASK_OF_MADNESS_ID, R.drawable.mask_of_madness_maska_sumasshestviya_dota,
                "Mask of Madness", "Маска Безумия", "900(1800)", true)
            .addProperty("Вампиризм: +17%")
            .addProperty("Активная: Увеличивает на 100% скорость атаки, на 30% скорость движения и получаемый урон героем, 12сек, КД 25сек, стоит 25МР")
            .addNeedForRecipeItem(ItemContract.MASK_OF_DEATH_ID));

        items.add(new Item(ItemContract.DIFFUSAL_BLADE_ID, R.drawable.diffusal_blade_1_klinok_diffuzii_dota,
                "Diffusal Blade", "Дифузы", "850(3300)", true)
            .addProperty("Ловкость: +22")
            .addProperty("Интеллект: +6")
            .addProperty("Сжигает 20 маны противника за удар")
            .addProperty("Активная: замеждение юнита и снятие всех маг. эффектов на расстоянии 600, убивает призванных юнитов, 8 зарядов, КД: 8сек")
            .addProperty("Улучшается 1 раз покупкой второго рецепта")
            .addNeedForRecipeItem(ItemContract.BLADE_OF_ALACRITY_ID)
            .addNeedForRecipeItem(ItemContract.BLADE_OF_ALACRITY_ID)
            .addNeedForRecipeItem(ItemContract.ROBE_OF_THE_MAGI_ID));

        items.add(new Item(ItemContract.HEAVENS_HALLBERD_ID, R.drawable.heavens_halberd_nebesa_alebarda_dota,
                "Heaven's Halberd", "Алебарда", "0(3850)")
            .addProperty("Сила: +20")
            .addProperty("Урон: +25")
            .addProperty("25% шанс уклониться от физ. атаки противника")
            .addProperty("шанс снизить скор. атаки противника на 15% и скор. перемещения на 20% в течение 4 сек")
            .addProperty("Активная: обезоруживает противника ближника на 3 сек и дальника на 4.5 сек, стоит 100МР, КД: 30 сек")
            .addNeedForRecipeItem(sange)
            .addNeedForRecipeItem(ItemContract.TALISMAN_OF_EVASION_ID));

        for(Item i : items){
            i.setShop(shop);
            shop.addItem(i);
        }

        ShopLab.addShop(shop);

        return items;
    }

    private static ArrayList<Item> createItemsFromArcaneSanctrum(){

        Shop shop = new Shop("Arcane Sanctrum", R.drawable.arcane_rad, R.drawable.arcane_dier);

        ArrayList<Item> items = new ArrayList<>();

        items.add(new Item(ItemContract.GUINSOOS_SCYTHE_OF_VYSE_ID, R.drawable.guinsoo_s_scythe_of_vyse_guinso_dota,
                "Guinsoo's Scythe of Vyse (Hex)", "Гуинсо (Хекс)", "0(5675)")
            .addProperty("Сила: +10")
            .addProperty("Ловкость: +10")
            .addProperty("Интеллект: +35")
            .addProperty("Реген. МР: +150%")
            .addProperty("Активная: превращает врага в животное на 3.5 сек и уменьшает скор. движения до 100, КД: 35сек, стоит 100МР")
            .addNeedForRecipeItem(ItemContract.MYSTIC_STAFF_ID)
            .addNeedForRecipeItem(ItemContract.ULTIMATE_ORB_ID)
            .addNeedForRecipeItem(ItemContract.VOID_STONE_ID));

        items.add(new Item(ItemContract.ORCHID_MALEVOLENCE_ID, R.drawable.orchid_malevolence_orhideya_zloradstva_dota,
                "Orchid Malevolence (Silence)", "Орхидея злорадства (Сало)", "775(4125)", true)
            .addProperty("Скорость атаки: +30")
            .addProperty("Интеллект: +25")
            .addProperty("Урон: +30")
            .addProperty("Реген. МР: +150%")
            .addProperty("Активная: запрещает врогу колдовать на 5 сек, стоит 100МР, КД: 18 сек")
            .addNeedForRecipeItem(ItemContract.OBLIVION_STAFF_ID)
            .addNeedForRecipeItem(ItemContract.OBLIVION_STAFF_ID));

        items.add(new Item(ItemContract.EULS_SCEPTER_OF_DIVINITY_ID, R.drawable.eul_s_scepter_of_divinity_bozhestvenniy_skipetr_eula_dota,
                "Eul's Scepter of Divinity", "Еул", "650(2850)", true)
            .addProperty("Интеллект: +10")
            .addProperty("Скорость движения: +40")
            .addProperty("Реген. МР: +150%")
            .addProperty("Активная: поднимает юнит в воздух на 2.5 сек: его нельзя атаковать, КД: 25сек, стоит 75МР")
            .addNeedForRecipeItem(ItemContract.STAFF_OF_WIZARDY_ID)
            .addNeedForRecipeItem(ItemContract.SOBI_MASK_ID)
            .addNeedForRecipeItem(ItemContract.VOID_STONE_ID));

        items.add(new Item(ItemContract.FORCE_STAFF_ID, R.drawable.force_staff_posoh_prinuzhdeniya_dota,
                "Force Staff", "Форс стаф", "900(2250)", true)
                .addProperty("Реген. НР: +3")
                .addProperty("Интеллект: +10")
                .addProperty("Активная: толкает юнита на 600 юнитов в направлении лица героя в течение 0.3 сек, стоит 25МР, КД: 20сек, при двойном нажатии кастуется на себя")
                .addNeedForRecipeItem(ItemContract.STAFF_OF_WIZARDY_ID)
                .addNeedForRecipeItem(ItemContract.RING_OF_REGENERATION_ID));

        items.add(new Item(ItemContract.DAGON_ID, R.drawable.dagon_1_dagon_dota,
                "Dagon", "Дагон", "1250(2700)", true)
            .addProperty("Сила: +3")
            .addProperty("Ловкость: +3")
            .addProperty("Интеллект: +16")
            .addProperty("Урон: +9")
            .addProperty("Активная: 600 урона на расстоянии 400, КД:35 сек, стоит 180МР")
            .addProperty("Можно улучишить 4 раза рецептом, увеличивая на 50 урон, на 2 интеллект, расстояние на 100, уменьшая стоимость на 20МР и КД на 5 сек")
            .addNeedForRecipeItem(ItemContract.STAFF_OF_WIZARDY_ID)
            .addNeedForRecipeItem(ItemContract.NULL_TALISMAN_ID));

        items.add(new Item(ItemContract.NECROMICON_ID, R.drawable.necromicon_1_kniga_nekromanta_dota,
                "Necromicon", "Книга некроманта", "1250(2700)", true)
            .addProperty("Сила: +8")
            .addProperty("Интеллект: +15")
            .addProperty("Активная: вызывает воинов ближнего и дальнего боя на 40 сек, КД 80 сек, стоит 50МР")
            .addProperty("Warrior: Атака:21, Защита:3, 400НР, скор. передвижения: 330, обзор 1300, наносит 200 урона тому, кто его убьет, сжигает 25 МР за удар")
            .addProperty("Archer: Атака: 21, Защита:6, 400НР, скор. передвижения: 330, обзор 1300, сжигает 125 МР указанному юниту" +
                    " (активная), увелич. скор. передвижения и атаки союзников на 3% на расстоянии 900")
            .addProperty("Улучшается 2 раза покупкой рецепта")
            .addNeedForRecipeItem(ItemContract.STAFF_OF_WIZARDY_ID)
            .addNeedForRecipeItem(ItemContract.BELT_OF_GIANT_STRENGTH_ID));

        items.add(new Item(ItemContract.AGHANIMS_SCEPTER_ID, R.drawable.aghanim_s_scepter_skipetr_aganima_dota,
                "Aghanim's Scepter", "Аганим", "0(4200)")
            .addProperty("Сила: +10")
            .addProperty("Ловкость: +10")
            .addProperty("Интеллект: +10")
            .addProperty("Улучшает ульт следующим героям:")
            .addProperty("Сила: Abaddon, Balanaur, Barathrum, Crixalis, Dirge (Undying), Huskar, Lucifer, Mangix, Mogul Kahn(Axe), Pudge, Purist Thunderwrath(Omniknight)," +
                    "Raigor Stonehoof(Earthshaker), Rattletrap (Clockwerk), Rexxar, Sven, Tiny")
            .addProperty("Ловкость: Gyrocopter, Void, Venomancer, Moon Rider, Geomancer, Razor, Vengeful Spirit, Viper, Yurnero")
            .addProperty("Интеллект: Ogre Magi, Enchantress, Queen of Pain, Windrunner, Ancient Apparition, Atropos, Boush, Chen, Enigma, Dazzle, Warlock, Thrall, Furion" +
                    "Rubick, Obsidian Destroyer, Dark Seer, Jakiro, Invoker, Lich, Leshrac, Lina, Lion, Nortrom, Puck, Pugna, Rhasta, Necrolyte, Crystal Maiden, Dragonus," +
                    "Goblin Techies, Visage, Witch Doctor, Zeus")
            .addNeedForRecipeItem(ItemContract.POINT_BOOSTER_ID)
            .addNeedForRecipeItem(ItemContract.STAFF_OF_WIZARDY_ID)
            .addNeedForRecipeItem(ItemContract.OGRE_AXE_ID)
            .addNeedForRecipeItem(ItemContract.BLADE_OF_ALACRITY_ID));

        items.add(new Item(ItemContract.REFRESHER_ORB_ID, R.drawable.refresher_orb_shar_osvezheniya_dota,
                "Refresher Orb", "Рефрешер", "1875(5300)", true)
            .addProperty("Реген. НР: +5")
            .addProperty("Скорость атаки: +10")
            .addProperty("Интеллект: +6")
            .addProperty("Урон: +40")
            .addProperty("Реген. МР: +200%")
            .addProperty("Активная: Сброс КД у всех способностей героя, КД: 195 сек, стоит 375 МР")
            .addNeedForRecipeItem(ItemContract.OBLIVION_STAFF_ID)
            .addNeedForRecipeItem(ItemContract.PERSEVERANCE_ID));

        items.add(new Item(ItemContract.VEIL_OF_DISCORD_ID, R.drawable.veil_of_discord_zavesa_raznoglasiya_dota,
                "Veil of Discord", "Завеса Разногласия", "1250(2650)", true)
            .addProperty("Сила: +3")
            .addProperty("Ловкость: +3")
            .addProperty("Интеллект: +3")
            .addProperty("Урон: +3")
            .addProperty("Активная: снижает защиту к магии на 25% на выбранной области, длится 25 сек, стоит 75МР, КД: 30сек")
            .addNeedForRecipeItem(ItemContract.NULL_TALISMAN_ID)
            .addNeedForRecipeItem(ItemContract.HELM_OF_IRON_WILL_ID));

        items.add(new Item(ItemContract.ROD_OF_ATOS_ID, R.drawable.rod_of_atos_zhezl_atosa_dota,
                "Rod of Atos", "Жезл Атоса", "0(3100)")
            .addProperty("НР: +325")
            .addProperty("Интеллект: +25")
            .addProperty("Активная:  снижает скорость движения врага на 60% в течение 4сек, стоит 50МР, КД: 12сек, дальность: 1200")
            .addNeedForRecipeItem(ItemContract.STAFF_OF_WIZARDY_ID)
            .addNeedForRecipeItem(ItemContract.STAFF_OF_WIZARDY_ID)
            .addNeedForRecipeItem(ItemContract.VITALITY_BOOSTER_ID));

        for(Item i : items){
            i.setShop(shop);
            shop.addItem(i);
        }

        ShopLab.addShop(shop);

        return items;
    }

    private static ArrayList<Item> createItemsFromAncientWeaponary(){
        ArrayList<Item> items = new ArrayList<>();

        items.add(new Item(ItemContract.DIVINE_RAPIRE_ID, R.drawable.divine_rapire_rapira_dota,
                "Divine Rapire", "Рапира", "0(6200)")
            .addProperty("Урон: +300")
            .addProperty("Выпадает при смерти, улучшаясь у врага")
            .addNeedForRecipeItem(ItemContract.SACRED_RELIC_ID)
            .addNeedForRecipeItem(ItemContract.DEMON_EDGE_ID));

        items.add(new Item(ItemContract.MONKEY_KING_BAR_ID, R.drawable.monkey_king_bar_posoh_korolya_obezyan_dota,
                "Monkey King Bar (MKB)", "Посох короля обезьян (МКБ)", "0(5400)")
                .addProperty("Скорость атаки: +15")
                .addProperty("Урон: +88")
                .addProperty("Микростан и 100 урона с шансом 35% или True Strike(без промахов)")
                .addNeedForRecipeItem(ItemContract.JAVELIN_ID)
                .addNeedForRecipeItem(ItemContract.JAVELIN_ID)
                .addNeedForRecipeItem(ItemContract.DEMON_EDGE_ID));

        items.add(new Item(ItemContract.RADIANCE_ID, R.drawable.radiance_radians_dota,
                "Radiance", "Радианс", "1350(5150)", true)
            .addProperty("Урон: +60")
            .addProperty("Все враги на расстоянии 700 все враги получают 50 урона в секунду")
            .addProperty("Активная: включается/выключается")
            .addNeedForRecipeItem(ItemContract.SACRED_RELIC_ID));

        items.add(new Item(ItemContract.BUTTERFLY_ID, R.drawable.the_butterfly_babochka_dota,
                "Butterfly", "Бабочка", "0(6000)")
                .addProperty("Ловкость: +30")
                .addProperty("Урон: +30")
                .addProperty("Скорость атаки: +60 (учит. ловкость)")
                .addProperty("Уклонение: 35%")
                .addNeedForRecipeItem(ItemContract.EAGLEHORN_ID)
                .addNeedForRecipeItem(ItemContract.TALISMAN_OF_EVASION_ID)
                .addNeedForRecipeItem(ItemContract.QUARTERSTAFF_ID));

        Item crystalus = new Item(ItemContract.CRYSTALYS_ID, R.drawable.crystalys_kristalis_dota,
                "Crystalys", "Кристалис", "500(2150)", true)
            .addProperty("Урон: +30")
            .addProperty("Крит. удар: 1.8х, 20% шанс")
            .addNeedForRecipeItem(ItemContract.BROADSWORD_ID)
            .addNeedForRecipeItem(ItemContract.BLADES_OF_ATTACK_ID);

        items.add(crystalus);

        items.add(new Item(ItemContract.BURIZE_ID, R.drawable.burize_do_kyanon_buriza_dota,
                "Burize-do Kyanon", "Буриза", "1000(5550)", true)
            .addProperty("Урон: +81")
            .addProperty("Крит. удар: 2.5х, 25% шанс")
            .addNeedForRecipeItem(crystalus)
            .addNeedForRecipeItem(ItemContract.DEMON_EDGE_ID));

        Item basher = new Item(ItemContract.BASHER_ID, R.drawable.cranium_basher_basher_dota,
                "Cranium Basher", "Башер", "1000(2950)", true)
            .addProperty("Сила: +6")
            .addProperty("Урон: +40")
            .addProperty("Bash: 25(10)% оглушения на 1.4 сек для ближников(дальников), КД: 2сек")
            .addNeedForRecipeItem(ItemContract.JAVELIN_ID)
            .addNeedForRecipeItem(ItemContract.BELT_OF_GIANT_STRENGTH_ID);

        items.add(basher);

        items.add(new Item(ItemContract.BATTLE_FURY_ID, R.drawable.battle_fury_yarost_bitvi_dota,
                "Battle Fury", "Батл Фури", "0(4350)")
            .addProperty("Реген. НР: +6")
            .addProperty("Урон: +65")
            .addProperty("Ближникам дает возможность нанеси 35% урона, нанесенного врагу, юнитам в радиусе 225 единиц")
            .addProperty("Реген. МР: +150%")
            .addNeedForRecipeItem(ItemContract.CLAYMORE_ID)
            .addNeedForRecipeItem(ItemContract.BROADSWORD_ID)
            .addNeedForRecipeItem(ItemContract.PERSEVERANCE_ID));

        items.add(new Item(ItemContract.ABYSSAL_BLADE_ID, R.drawable.abyssal_blade_abisalniy_klinok_dota,
                "Abyssal Blade", "Абисальный клинок", "0(6750)")
            .addProperty("Сила: +10")
            .addProperty("Урон: +100")
            .addProperty("Стан на 1.4 сек с шансом 25(10)% для ближников (дальников), КД: 2сек")
            .addProperty("Не работает с Cranium Bashers, Abyssal Blades, Troll Warlord's Bash, " +
                    "Slithereen Guard's Bash, Faceless Void's Time Lock or Spiritbreaker's Greater Bash")
            .addProperty("Активная: Станит врага на 2 секунды, " +
                    "можно юзать на магически имунных юнитов, КД: 60сек, стоит 150 МР , дальность: 140")
            .addNeedForRecipeItem(ItemContract.SACRED_RELIC_ID)
            .addNeedForRecipeItem(basher));

        items.add(new Item(ItemContract.ARMLET_OF_MORDIGGIAN_ID, R.drawable.armlet_of_mordiggian_braslet_mordiggana_dota,
                "Armlet of Mordiggian", "Браслет Мордиггана", "500(2400)", true)
            .addProperty("Реген.НР: +7")
            .addProperty("Урон: +9")
            .addProperty("Броня: +5(23%)")
            .addProperty("Скорость атаки: +15")
            .addProperty("Активная: повышает атаку на 31, силу на 25, но теряет 40НР в сек, пока включено." +
                    " При деактивации отнимает 471 НР. КД: 5сек, но от него невозможно умереть")
            .addNeedForRecipeItem(ItemContract.HELM_OF_IRON_WILL_ID)
            .addNeedForRecipeItem(ItemContract.GLOVES_OF_HASTE_ID)
            .addNeedForRecipeItem(ItemContract.BLADES_OF_ATTACK_ID));

        items.add(new Item(ItemContract.LOTHARS_EDGE_ID, R.drawable.lothar_s_edge_lezvie_lotara_dota,
                "Lothar's Edge", "Лезвие Лотара", "0(2800)")
            .addProperty("Скорость атаки: +10")
            .addProperty("Урон: +38")
            .addProperty("Активная (Wind Walk): скор. движения: +20%, невидимость на 12 сек, при ударе из инвиза: +150 урона, КД: 28сек, стоит 75МР")
            .addNeedForRecipeItem(ItemContract.CLAYMORE_ID)
            .addNeedForRecipeItem(ItemContract.SHADOW_AMULET_ID));

        items.add(new Item(ItemContract.EHTHEREAL_BLADE_ID, R.drawable.ethereal_blade_efirnoe_lezvie_dota,
                "Ethereal Blade", "Эфирное Лезвие", "0(4900)")
            .addProperty("Сила: +10")
            .addProperty("Ловкость: +40")
            .addProperty("Интеллект: +10")
            .addProperty("Активная: переносит обладателя и указаную цель в астрал на 3 сек, можно послать в астрал себя на 4 секунды." +
                    " дарует физический иммунитет, но отбирает возможность атаковать и увеличивает получаемый магический урон на 40% " +
                    " вражеская цель также получает 75 +2x от главного атрибута обладателя в виде урона и замедляется на 80% " +
                    " стоит 150 МР, КД: 30сек, радиус 800.")
            .addNeedForRecipeItem(ItemContract.EAGLEHORN_ID)
            .addNeedForRecipeItem(ItemContract.GHOST_SCEPTER_ID));

        Shop shop = new Shop("Ancient Weaponry", R.drawable.ancient_weaponry_rad, R.drawable.ancient_weaponary_diers);
        for(Item i : items){
            i.setShop(shop);
            shop.addItem(i);
        }

        ShopLab.addShop(shop);

        return items;
    }

}