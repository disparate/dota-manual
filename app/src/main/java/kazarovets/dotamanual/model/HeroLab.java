package kazarovets.dotamanual.model;

import java.util.ArrayList;

import kazarovets.dotamanual.R;
import kazarovets.dotamanual.contracts.HeroContract;


public class HeroLab {
    private static ArrayList<Hero> heroes = null;
    public static ArrayList<Hero> getHeroes(){
        if (heroes == null){
            heroes = createHeroes();
        }
        return heroes;
    }

    public static Hero getHeroById(int id){
        Hero result = null;

        if(getHeroes().get(id).getId() == id){
            return getHeroes().get(id);
        } else {
            for (Hero h : getHeroes()) {
                if (h.getId() == id) {
                    result = h;
                    break;
                }
            }
        }
        return result;
    }

    public static ArrayList<Hero> getHeroesByCategory(int category){
        ArrayList result = new ArrayList();
        int beginId = 0;
        int endId = 0;
        switch (category){
            case HeroContract.SENTINEL_STRENGTH:{
                beginId = HeroContract.FIRST_SENTINEL_STRENGTH;
                endId = HeroContract.LAST_SENTINEL_STRENGTH;
                break;
            }

            case HeroContract.SENTINEL_AGILITY:{
                beginId = HeroContract.FIRST_SENTINEL_AGILITY;
                endId = HeroContract.LAST_SENTINEL_AGILITY;
                break;
            }

            case HeroContract.SENTINEL_INTELLIGENCE:{
                beginId = HeroContract.FIRST_SENTINEL_INTELLIGENCE;
                endId = HeroContract.LAST_SENTINEL_INTELLIGENCE;
                break;
            }

            case HeroContract.SCOURGE_STRENGTH:{
                beginId = HeroContract.FIRST_SCOURGE_STRENGTH;
                endId = HeroContract.LAST_SCOURGE_STRENGTH;
                break;
            }

            case HeroContract.SCOURGE_AGILITY:{
                beginId = HeroContract.FIRST_SCOURGE_AGILITY;
                endId = HeroContract.LAST_SCOURGE_AGILITY;
                break;
            }

            case HeroContract.SCOURGE_INTELLIGENCE:{
                beginId = HeroContract.FIRST_SCOURGE_INTELLIGENCE;
                endId = HeroContract.LAST_SCOURGE_INTELLIGENCE;
                break;
            }
        }

        for(int id = beginId; id <= endId; id++){
            result.add(getHeroById(id));
        }
        return result;
    }

    private static ArrayList<Hero> createHeroes(){
        ArrayList<Hero> heroes = new ArrayList<Hero>();

        heroes.add(new Hero(HeroContract.ALCHEMIC_ID, "Alchemist", "Razzil Darbrew", R.drawable.hero_alxim,
                new int[]{R.drawable.hero_alxim_1,
                        R.drawable.hero_alxim_2,
                        R.drawable.hero_alxim_3,
                        R.drawable.hero_alxim_4}, R.drawable.gif_alchemist));

        heroes.add(new Hero(HeroContract.BRISTLEBACK_ID, "Bristleback", "Rigwarl", R.drawable.hero_bristle,
                new int[]{R.drawable.hero_bristleback_1,
                    R.drawable.hero_bristleback_2,
                    R.drawable.hero_bristleback_3,
                    R.drawable.hero_bristleback_4}, R.drawable.gif_bristle));

        heroes.add(new Hero(HeroContract.BREWMASTER_ID, "Brewmaster", "Mangix", R.drawable.hero_pivopanda,
                new int[]{R.drawable.hero_brew_1,
                    R.drawable.hero_brew_2,
                    R.drawable.hero_brew_3,
                    R.drawable.hero_brew_4}, R.drawable.gif_mangix));

        heroes.add(new Hero(HeroContract.WISP_ID, "Wisp", "IO Guardian", R.drawable.hero_io,
                new int[]{R.drawable.hero_wisp_1,
                    R.drawable.hero_wisp_2,
                    R.drawable.hero_wisp_3,
                    R.drawable.hero_wisp_4}, R.drawable.gif_whisp));

        heroes.add(new Hero(HeroContract.TREANT_PROTECTOR_ID, "Treant Protector", "Rooftrellen", R.drawable.hero_derevo,
                new int[]{R.drawable.hero_treant_1,
                    R.drawable.hero_treant_2,
                    R.drawable.hero_treant_3,
                    R.drawable.hero_treant_4}, R.drawable.gif_treant));

        heroes.add(new Hero(HeroContract.DRAGON_KNIGHT_ID, "Dragon Knight", "Knight Davion", R.drawable.hero_dk,
                new int[]{R.drawable.hero_dragon_knight_1,
                    R.drawable.hero_dragon_knight_2,
                    R.drawable.hero_dragon_knight_3,
                    R.drawable.hero_dragon_knight_4}, R.drawable.gif_dragon_knight));

        heroes.add(new Hero(HeroContract.CAOLIN_ID, "Earth Spirit", "Kaolin", R.drawable.hero_kaolin,
                new int[]{R.drawable.hero_caolin_1,
                    R.drawable.hero_caolin_2,
                    R.drawable.hero_caolin_3,
                    R.drawable.hero_caolin_4}, R.drawable.gif_earth_spirit));

        heroes.add(new Hero(HeroContract.CENTAUR_ID, "Centaur Warchief", "Bradwarden", R.drawable.hero_kentavr,
                new int[]{R.drawable.hero_centaur_1,
                    R.drawable.hero_centaur_2,
                    R.drawable.hero_centaur_3,
                    R.drawable.hero_centaur_4}, R.drawable.gif_centaur));

        heroes.add(new Hero(HeroContract.CLOCKWERK_ID, "Clockwerk Goblin", "Rattletrap", R.drawable.hero_klok,
                new int[]{R.drawable.hero_clockwerk_1,
                    R.drawable.hero_clockwerk_2,
                    R.drawable.hero_clockwerk_3,
                    R.drawable.hero_clockwerk_4}, R.drawable.gif_clockwerk));

        heroes.add(new Hero(HeroContract.CUNKA_ID, "Admiral", "Kunkka", R.drawable.hero_kunka,
                new int[]{R.drawable.hero_cunka_1,
                    R.drawable.hero_cunka_2,
                    R.drawable.hero_cunka_3,
                    R.drawable.hero_cunka_4}, R.drawable.gif_kunkka));

        heroes.add(new Hero(HeroContract.LEGION_COMMANDER_ID, "Legion Commander", "Tresdin", R.drawable.hero_legioner,
                new int[]{R.drawable.hero_legion_1,
                    R.drawable.hero_legion_2,
                    R.drawable.hero_legion_3,
                    R.drawable.hero_legion_4}, R.drawable.gif_legion));

        heroes.add(new Hero(HeroContract.OMNIKNIGHT_ID, "Omniknight", "Purist Thunderwrath", R.drawable.hero_omni,
                new int[]{R.drawable.hero_omnik_1,
                    R.drawable.hero_omnik_2,
                    R.drawable.hero_omnik_3,
                    R.drawable.hero_omnik_4}, R.drawable.gif_omnik));

        heroes.add(new Hero(HeroContract.REZAK_ID, "Goblin Shredder", "Rizzrak", R.drawable.hero_rezak,
                new int[]{R.drawable.hero_rezak_1,
                    R.drawable.hero_rezak_2,
                    R.drawable.hero_rezak_3,
                    R.drawable.hero_rezak_4}, R.drawable.gif_rizzrak));

        heroes.add(new Hero(HeroContract.REXAR_ID, "Rexar", "The Beastmaster", R.drawable.hero_rexar,
                new int[]{R.drawable.hero_rexar_1,
                    R.drawable.hero_rexar_2,
                    R.drawable.hero_rexar_3,
                    R.drawable.hero_rexar_4}, R.drawable.gif_beastmaster));

        heroes.add(new Hero(HeroContract.SVEN_ID, "Sven", "Rogue Knight", R.drawable.hero_sven,
                new int[]{R.drawable.hero_sven_1,
                    R.drawable.hero_sven_2,
                    R.drawable.hero_sven_3,
                    R.drawable.hero_sven_4}, R.drawable.gif_sven));

        heroes.add(new Hero(HeroContract.TAUREN_ID, "Tauren", "Chieftain", R.drawable.hero_chiften,
                new int[]{R.drawable.hero_tauren_1,
                    R.drawable.hero_tauren_2,
                    R.drawable.hero_tauren_3,
                    R.drawable.hero_tauren_4}, R.drawable.gif_tauren));

        heroes.add(new Hero(HeroContract.TINI_ID, "Tiny", "Stone giant", R.drawable.hero_tiny,
                new int[]{R.drawable.hero_tiny_1,
                    R.drawable.hero_tiny_2,
                    R.drawable.hero_tiny_3,
                    R.drawable.hero_tiny_4}, R.drawable.gif_tiny));

        heroes.add(new Hero(HeroContract.TUSKAR_ID, "Tuskar", "Ymir", R.drawable.hero_tuskar,
                new int[]{R.drawable.hero_tuskar_1,
                    R.drawable.hero_tuskar_2,
                    R.drawable.hero_tuskar_3,
                    R.drawable.hero_tuskar_4}, R.drawable.gif_tuskar));

        heroes.add(new Hero(HeroContract.PHOENIX_ID, "Phoenix", "Icarus", R.drawable.hero_fenix,
                new int[]{R.drawable.hero_phoenix_1,
                    R.drawable.hero_phoenix_2,
                    R.drawable.hero_phoenix_3,
                    R.drawable.hero_phoenix_4}, R.drawable.gif_phoenix));

        heroes.add(new Hero(HeroContract.HUSCKAR_ID, "Huskar", "Sacred Warrior", R.drawable.hero_huskar,
                new int[]{R.drawable.hero_huskar_1,
                    R.drawable.hero_huskar_2,
                    R.drawable.hero_huskar_3,
                    R.drawable.hero_huskar_4}, R.drawable.gif_huskar));

        heroes.add(new Hero(HeroContract.SHEIKER_ID, "Earthshaker", "Raigor Stonehoof", R.drawable.hero_es,
                new int[]{R.drawable.hero_shaker_1,
                    R.drawable.hero_shaker_2,
                    R.drawable.hero_shaker_3,
                    R.drawable.hero_shaker_4}, R.drawable.gif_earthshaker));

        heroes.add(new Hero(HeroContract.BOUNTY_HUNTER_ID, "Gondar", "Bounty Hunter", R.drawable.hero_bh,
                new int[]{R.drawable.hero_gondar_1,
                    R.drawable.hero_gondar_2,
                    R.drawable.hero_gondar_3,
                    R.drawable.hero_gondar_4}, R.drawable.gif_bounty_hunter));

        heroes.add(new Hero(HeroContract.VENGEFUL_SPIRIT_ID, "Vengeful Spirit", "Shendelzare", R.drawable.hero_venga,
                new int[]{R.drawable.hero_venga_1,
                    R.drawable.hero_venga_2,
                    R.drawable.hero_venga_3,
                    R.drawable.hero_venga_4}, R.drawable.gif_venga));

        heroes.add(new Hero(HeroContract.GHYROCOPTER_ID, "Gyrocopter", "Aurel Vlauci", R.drawable.hero_vert,
                new int[]{R.drawable.hero_gyro_1,
                    R.drawable.hero_gyro_2,
                    R.drawable.hero_gyro_3,
                    R.drawable.hero_gyro_4}, R.drawable.gif_gyrocopter));

        heroes.add(new Hero(HeroContract.JAGGERNAUT_ID, "Jaggernaut", "Yurnero", R.drawable.hero_juger,
                new int[]{R.drawable.hero_juger_1,
                    R.drawable.hero_juger_2,
                    R.drawable.hero_juger_3,
                    R.drawable.hero_juger_4}, R.drawable.gif_jugernaut));

        heroes.add(new Hero(HeroContract.EMBER_SPIRIT_ID, "Ember Spirit", "Xin", R.drawable.hero_xin,
                new int[]{R.drawable.hero_ember_1,
                    R.drawable.hero_ember_2,
                    R.drawable.hero_ember_3,
                    R.drawable.hero_ember_4}, R.drawable.gif_xin));

        heroes.add(new Hero(HeroContract.LANAYA_ID, "Templar Assasin", "Lanaya", R.drawable.hero_lanaya,
                new int[]{R.drawable.hero_lanaya_1,
                    R.drawable.hero_lanaya_2,
                    R.drawable.hero_lanaya_3,
                    R.drawable.hero_lanaya_4}, R.drawable.gif_lanaya));

        heroes.add(new Hero(HeroContract.SYLLABEAR_ID, "Lone Druid", "Syllabear", R.drawable.hero_sulabir,
                new int[]{R.drawable.hero_druid_1,
                    R.drawable.hero_druid_2,
                    R.drawable.hero_druid_3,
                    R.drawable.hero_druid_4}, R.drawable.gif_druid));

        heroes.add(new Hero(HeroContract.LUNA_ID, "Luna Moonfag", "Moon Rider", R.drawable.hero_luna,
                new int[]{R.drawable.hero_luna_1,
                    R.drawable.hero_luna_2,
                    R.drawable.hero_luna_3,
                    R.drawable.hero_luna_4}, R.drawable.gif_moon_rider));

        heroes.add(new Hero(HeroContract.MAGINA_ID, "Antimag", "Magina", R.drawable.hero_magina,
                new int[]{R.drawable.hero_magina_1,
                    R.drawable.hero_magina_2,
                    R.drawable.hero_magina_3,
                    R.drawable.hero_magina_4}, R.drawable.gif_antimag));

        heroes.add(new Hero(HeroContract.MIRANA_ID, "Mirana", "Priestless of the Moon", R.drawable.hero_mirana,
                new int[]{R.drawable.hero_mirana_1,
                    R.drawable.hero_mirana_2,
                    R.drawable.hero_mirana_3,
                    R.drawable.hero_mirana_4}, R.drawable.gif_mirana));

        heroes.add(new Hero(HeroContract.MORPHLING_ID, "Morphling", "The Morphling", R.drawable.hero_morf,
                new int[]{R.drawable.skill_morph_1,
                    R.drawable.skill_morph_2,
                    R.drawable.skill_morph_3,
                    R.drawable.skill_morph_4}, R.drawable.gif_morphling));

        heroes.add(new Hero(HeroContract.NAGA_ID, "Naga Siren", "Slithice", R.drawable.hero_naga,
                new int[]{R.drawable.skill_naga_1,
                    R.drawable.skill_naga_2,
                    R.drawable.skill_naga_3,
                    R.drawable.skill_naga_4}, R.drawable.gif_naga));

        heroes.add(new Hero(HeroContract.RIKIMARU_ID, "Rikimaru", "Stealth Assasin", R.drawable.hero_riki,
                new int[]{R.drawable.skill_riki_1,
                    R.drawable.skill_riki_2,
                    R.drawable.skill_riki_3,
                    R.drawable.skill_riki_4}, R.drawable.gif_riki));

        heroes.add(new Hero(HeroContract.SNIPER_ID, "Dwarven Sniper", "Kardel Sharpeye", R.drawable.hero_sniper,
                new int[]{R.drawable.skill_sniper_1,
                    R.drawable.skill_sniper_2,
                    R.drawable.skill_sniper_3,
                    R.drawable.skill_sniper_4}, R.drawable.gif_sniper));

        heroes.add(new Hero(HeroContract.TRAKSES_ID, "Traxex", "Draw Ranger", R.drawable.hero_traxa,
                new int[]{R.drawable.skill_traxa_1,
                    R.drawable.skill_traxa_2,
                    R.drawable.skill_traxa_3,
                    R.drawable.skill_traxa_4}, R.drawable.gif_traxa));

        heroes.add(new Hero(HeroContract.TROLL_ID, "Troll Warlock", "Jah'rakal", R.drawable.hero_trol,
                new int[]{R.drawable.skill_troll_1,
                    R.drawable.skill_troll_2,
                    R.drawable.skill_troll_3,
                    R.drawable.skill_troll_4}, R.drawable.gif_troll));

        heroes.add(new Hero(HeroContract.URSA_ID, "Ursa Warrior", "Ulfsaar", R.drawable.hero_ursa,
                new int[]{R.drawable.skill_ursa_1,
                    R.drawable.skill_ursa_2,
                    R.drawable.skill_ursa_3,
                    R.drawable.skill_ursa_4}, R.drawable.gif_ursa));

        heroes.add(new Hero(HeroContract.PHANTOM_LANCER_ID, "Phantom Lancer", "Azwraith", R.drawable.hero_pl,
                new int[]{R.drawable.skill_phantom_1,
                    R.drawable.skill_phantom_2,
                    R.drawable.skill_phantom_3,
                    R.drawable.skill_phantom_4}, R.drawable.gif_phantom_lancer));

        heroes.add(new Hero(HeroContract.WINDRUNNER_ID, "Windrunner", "Alleria", R.drawable.hero_wr,
                new int[]{R.drawable.skill_windrunner_1,
                    R.drawable.skill_windrunner_2,
                    R.drawable.skill_windrunner_3,
                    R.drawable.skill_windrunner_4}, R.drawable.gif_windrunner));

        heroes.add(new Hero(HeroContract.JAKIRO_ID, "Jakiro", "Twin Head Dragon", R.drawable.hero_jakiro,
                new int[]{R.drawable.hero_jakiro_1,
                    R.drawable.hero_jakiro_2,
                    R.drawable.hero_jakiro_3,
                    R.drawable.hero_jakiro_4}, R.drawable.gif_twin_head));

        heroes.add(new Hero(HeroContract.DRAGONUS_ID, "Dragonus", "Skywrath Mage", R.drawable.hero_dragonus,
                new int[]{R.drawable.skill_dragonus_1,
                    R.drawable.skill_dragonus_2,
                    R.drawable.skill_dragonus_3,
                    R.drawable.skill_dragonus_4}, R.drawable.gif_dragonus));

        heroes.add(new Hero(HeroContract.ZEUS_ID, "Zeus", "Lord of Olympus", R.drawable.hero_zeus,
                new int[]{R.drawable.skill_zeus_1,
                    R.drawable.skill_zeus_2,
                    R.drawable.skill_zeus_3,
                    R.drawable.skill_zeus_4}, R.drawable.gif_zeus));

        heroes.add(new Hero(HeroContract.LINA_ID, "Lina Inverse", "Slayer", R.drawable.hero_lina,
                new int[]{R.drawable.hero_lina_1,
                    R.drawable.hero_lina_2,
                    R.drawable.hero_lina_3,
                    R.drawable.hero_lina_4}, R.drawable.gif_lina));

        heroes.add(new Hero(HeroContract.MINER_ID, "Miner", "Goblin Techies", R.drawable.hero_miner,
                new int[]{R.drawable.skill_miner_1,
                    R.drawable.skill_miner_2,
                    R.drawable.skill_miner_3,
                    R.drawable.skill_miner_4}, R.drawable.gif_techies));

        heroes.add(new Hero(HeroContract.ORACKLE_ID, "Orackle", "Nerif", R.drawable.hero_nerif_oracle,
                new int[]{R.drawable.skill_orackle_1,
                    R.drawable.skill_orackle_2,
                    R.drawable.skill_orackle_3,
                    R.drawable.skill_orackle_4}, R.drawable.gif_nerif));

        heroes.add(new Hero(HeroContract.OGRE_ID, "Ogre Magi", "Aggron Stonebreaker", R.drawable.hero_ogr,
                new int[]{R.drawable.skill_ogr_1,
                    R.drawable.skill_ogr_2,
                    R.drawable.skill_ogr_3,
                    R.drawable.skill_ogr_4}, R.drawable.gif_ogre_mag));

        heroes.add(new Hero(HeroContract.PUCK_ID, "Puck", "Faerie Dragon", R.drawable.hero_puk,
                new int[]{R.drawable.skill_puck_1,
                    R.drawable.skill_puck_2,
                    R.drawable.skill_puck_3,
                    R.drawable.skill_puck_4}, R.drawable.gif_puck));

        heroes.add(new Hero(HeroContract.RHASTA_ID, "Rasta", "Shadow Shaman", R.drawable.hero_rasta,
                new int[]{R.drawable.skill_rasta_1,
                    R.drawable.skill_rasta_2,
                    R.drawable.skill_rasta_3,
                    R.drawable.skill_rasta_4}, R.drawable.gif_rhasta));

        heroes.add(new Hero(HeroContract.RUBICK_ID, "Rubick", "Grand Magus", R.drawable.hero_rubick,
                new int[]{R.drawable.skill_rubick_1,
                    R.drawable.skill_rubick_2,
                    R.drawable.skill_rubick_3,
                    R.drawable.skill_rubick_4}, R.drawable.gif_rubick));

        heroes.add(new Hero(HeroContract.RULAY_ID, "Crystal Maiden", "Rulai", R.drawable.hero_cm,
                new int[]{R.drawable.skill_kristal_1,
                    R.drawable.skill_kristal_2,
                    R.drawable.skill_kristal_3,
                    R.drawable.skill_kristal_4}, R.drawable.gif_crystal_maiden));

        heroes.add(new Hero(HeroContract.SILENCER_ID, "Silencer", "Nortom", R.drawable.hero_salo,
                new int[]{R.drawable.hero_silencer_1,
                    R.drawable.hero_silencer_2,
                    R.drawable.hero_silencer_3,
                    R.drawable.hero_silencer_4}, R.drawable.gif_silencer));

        heroes.add(new Hero(HeroContract.TINKER_ID, "Tinker", "Boush", R.drawable.hero_tinker,
                new int[]{R.drawable.skill_tinker_1,
                    R.drawable.skill_tinker_2,
                    R.drawable.skill_tinker_3,
                    R.drawable.skill_tinker_4}, R.drawable.gif_tinker));

        heroes.add(new Hero(HeroContract.TRALL_ID, "Thrall", "Disruptor", R.drawable.hero_thrall,
                new int[]{R.drawable.skill_trall_1,
                    R.drawable.skill_trall_2,
                    R.drawable.skill_trall_3,
                    R.drawable.skill_trall_4}, R.drawable.gif_thrall));

        heroes.add(new Hero(HeroContract.PHURION_ID, "Furion", "The Prophet", R.drawable.hero_fura,
                new int[]{R.drawable.skill_fura_1,
                    R.drawable.skill_fura_2,
                    R.drawable.skill_fura_3,
                    R.drawable.skill_fura_4}, R.drawable.gif_furion));

        heroes.add(new Hero(HeroContract.CHEN_ID, "Chen", "Holy Knight", R.drawable.hero_chen,
                new int[]{R.drawable.skill_chen_1,
                    R.drawable.skill_chen_2,
                    R.drawable.skill_chen_3,
                    R.drawable.skill_chen_4}, R.drawable.gif_chen));

        heroes.add(new Hero(HeroContract.SHTORM_SPIRIT_ID, "Storm Spirit", "Raijin", R.drawable.hero_storm,
                new int[]{R.drawable.skill_storm_1,
                    R.drawable.skill_storm_2,
                    R.drawable.skill_storm_3,
                    R.drawable.skill_storm_4}, R.drawable.gif_storm_spirit));

        heroes.add(new Hero(HeroContract.EZALOR_ID, "Ezalor", "Keeper of the Light", R.drawable.hero_ezalor,
                new int[]{R.drawable.skill_ezalor_1,
                    R.drawable.skill_ezalor_2,
                    R.drawable.skill_ezalor_3,
                    R.drawable.skill_ezalor_4}, R.drawable.gif_keeper_of_the_light));

        heroes.add(new Hero(HeroContract.ENCHANTRES_ID, "Enchantress", "Aiushtha", R.drawable.hero_ench,
                new int[]{R.drawable.skill_enchantress_1,
                    R.drawable.skill_enchantress_2,
                    R.drawable.skill_enchantress_3,
                    R.drawable.skill_enchantress_4}, R.drawable.gif_enchantress));

        heroes.add(new Hero(HeroContract.ABBADON_ID, "Abaddon", "Lord of Avernus", R.drawable.hero_aba,
                new int[]{R.drawable.skill_abadon_1,
                    R.drawable.skill_abadon_2,
                    R.drawable.skill_abadon_3,
                    R.drawable.skill_abadon_4}, R.drawable.gif_abadon));

        heroes.add(new Hero(HeroContract.AXE_ID, "Axe", "Mogul Khan", R.drawable.hero_axe,
                new int[]{R.drawable.skill_axe_1,
                    R.drawable.skill_axe_2,
                    R.drawable.skill_axe_3,
                    R.drawable.skill_axe_4}, R.drawable.gif_axe));

        heroes.add(new Hero(HeroContract.BALANAR_ID, "Balanar", "Night Stalker", R.drawable.hero_balanar,
                new int[]{R.drawable.skill_balanar_1,
                    R.drawable.skill_balanar_2,
                    R.drawable.skill_balanar_3,
                    R.drawable.skill_balanar_4}, R.drawable.gif_balanar));

        heroes.add(new Hero(HeroContract.BARATHRUM_ID, "Barathrum", "The Spiritbreaker", R.drawable.hero_bara,
                new int[]{R.drawable.skill_bara_1,
                    R.drawable.skill_bara_2,
                    R.drawable.skill_bara_3,
                    R.drawable.skill_bara_4}, R.drawable.gif_barathrum));

        heroes.add(new Hero(HeroContract.WOLF_ID, "Lycantrope", "Banehallow", R.drawable.hero_volk,
                new int[]{R.drawable.skill_lycantrope_1,
                    R.drawable.skill_lycantrope_2,
                    R.drawable.skill_lycantrope_3,
                    R.drawable.skill_lycantrope_4}, R.drawable.gif_lycantrope));

        heroes.add(new Hero(HeroContract.DOOM_ID, "Doom Bringer", "Lucifer", R.drawable.hero_doom,
                new int[]{R.drawable.skill_doom_1,
                    R.drawable.skill_doom_2,
                    R.drawable.skill_doom_3,
                    R.drawable.skill_doom_4}, R.drawable.gif_doom));

        heroes.add(new Hero(HeroContract.ZOMBI_ID, "Undying", "Dirge", R.drawable.hero_undaing,
                new int[]{R.drawable.skill_undying_1,
                    R.drawable.skill_undying_2,
                    R.drawable.skill_undying_3,
                    R.drawable.skill_undying_4}, R.drawable.gif_zombi));

        heroes.add(new Hero(HeroContract.LEORIK_ID, "Leorik", "Skeleton King", R.drawable.hero_leorik,
                new int[]{R.drawable.skill_leorick_1,
                    R.drawable.skill_leorick_2,
                    R.drawable.skill_leorick_3,
                    R.drawable.skill_leorick_4}, R.drawable.gif_skeleton_king));

        heroes.add(new Hero(HeroContract.MAGNUS_ID, "Magnus", "Magnataur", R.drawable.hero_magnus,
                new int[]{R.drawable.skill_magnus_1,
                    R.drawable.skill_magnus_2,
                    R.drawable.skill_magnus_3,
                    R.drawable.skill_magnus_4}, R.drawable.gif_magnus));

        heroes.add(new Hero(HeroContract.NIXE_ID, "Naix", "Lifestealer", R.drawable.hero_naix,
                new int[]{R.drawable.hero_naix_1,
                    R.drawable.hero_naix_2,
                    R.drawable.hero_naix_3,
                    R.drawable.hero_naix_4}, R.drawable.gif_naix));

        heroes.add(new Hero(HeroContract.PIT_LORD_ID, "Pit Lord", "Azgalor", R.drawable.hero_pitlord,
                new int[]{R.drawable.skill_pitlord_1,
                    R.drawable.skill_pitlord_2,
                    R.drawable.skill_pitlord_3,
                    R.drawable.skill_pitlord_4}, R.drawable.gif_pit_lord));

        heroes.add(new Hero(HeroContract.PUDGE_ID, "Pudge", "Butcher", R.drawable.hero_pudge,
                new int[]{R.drawable.skill_pudge_1,
                    R.drawable.skill_pudge_2,
                    R.drawable.skill_pudge_3,
                    R.drawable.skill_pudge_4}, R.drawable.gif_pudge));

        heroes.add(new Hero(HeroContract.SLARDAR_ID, "Slardar", "Slithereen Guard", R.drawable.hero_riba,
                new int[]{R.drawable.skill_slardar_1,
                    R.drawable.skill_slardar_2,
                    R.drawable.skill_slardar_3,
                    R.drawable.skill_slardar_4}, R.drawable.gif_slardar));

        heroes.add(new Hero(HeroContract.SCORPION_ID, "Sand King", "Crixalis", R.drawable.hero_skorp,
                new int[]{R.drawable.hero_skorp_1,
                    R.drawable.hero_skorp_2,
                    R.drawable.hero_skorp_3,
                    R.drawable.hero_skorp_4}, R.drawable.gif_sand_king));

        heroes.add(new Hero(HeroContract.TIDEHUNTER_ID, "Tidehunter", "Leviathan", R.drawable.hero_tide,
                new int[]{R.drawable.skill_tide_1,
                    R.drawable.skill_tide_2,
                    R.drawable.skill_tide_3,
                    R.drawable.skill_tide_4}, R.drawable.gif_tidehunter));

        heroes.add(new Hero(HeroContract.CHAOS_KNIGHT, "Chaos Knight", "Nessaj", R.drawable.hero_chaos,
                new int[]{R.drawable.skill_chaos_1,
                    R.drawable.skill_chaos_2,
                    R.drawable.skill_chaos_3,
                    R.drawable.skill_chaos_4}, R.drawable.gif_chaos_knight));

        heroes.add(new Hero(HeroContract.NERUBIAN_ID, "Nerubian Assasin", "Anub'arak", R.drawable.hero_nerub,
                new int[]{R.drawable.skill_nerubian_0,
                    R.drawable.skill_nerubian_1,
                    R.drawable.skill_nerubian_2,
                    R.drawable.skill_nerubian_3}, R.drawable.gif_nerubian_assasin));

        heroes.add(new Hero(HeroContract.BLOODSEECKER_ID, "Bloodseecker", "Strygwyr", R.drawable.hero_siker,
                new int[]{R.drawable.skill_bloodseeker_1,
                    R.drawable.skill_bloodseeker_2,
                    R.drawable.skill_bloodseeker_3,
                    R.drawable.skill_bloodseeker_4}, R.drawable.gif_bloodseeker));

        heroes.add(new Hero(HeroContract.BONE_PHLETCHER_ID, "Bone Fletcher", "Clinkz", R.drawable.hero_bon,
                new int[]{R.drawable.skill_bone_1,
                    R.drawable.skill_bone_2,
                    R.drawable.skill_bone_3,
                    R.drawable.skill_bone_4}, R.drawable.gif_bone_phletcher));

        heroes.add(new Hero(HeroContract.BROODMOTHER_ID, "Broodmother", "Black Arachnia", R.drawable.hero_brood,
                new int[]{R.drawable.skill_brood_0,
                    R.drawable.skill_brood_1,
                    R.drawable.skill_brood_2,
                    R.drawable.skill_brood_3}, R.drawable.gif_broodmother));

        heroes.add(new Hero(HeroContract.VIPER_ID, "Viper", "Netherdrake", R.drawable.hero_viper,
                new int[]{R.drawable.skill_viper_1,
                    R.drawable.skill_viper_2,
                    R.drawable.skill_viper_3,
                    R.drawable.skill_viper_4}, R.drawable.gif_viper));

        heroes.add(new Hero(HeroContract.VENOMANCER_ID, "Venomancer", "Lesale Deathbringer", R.drawable.hero_venom,
                new int[]{R.drawable.skill_venom_1,
                    R.drawable.skill_venom_2,
                    R.drawable.skill_venom_3,
                    R.drawable.skill_venom_4}, R.drawable.gif_venomancer));

        heroes.add(new Hero(HeroContract.VIVER_ID, "Nerubian Weaver", "Anubseran", R.drawable.hero_viver,
                new int[]{R.drawable.skill_viver_1,
                    R.drawable.skill_viver_2,
                    R.drawable.skill_viver_3,
                    R.drawable.skill_viver_4}, R.drawable.gif_nerubian_weaver));

        heroes.add(new Hero(HeroContract.VOID_ID, "Faceless Void", "Darkterror", R.drawable.hero_void,
                new int[]{R.drawable.skill_void_1,
                        R.drawable.skill_void_2,
                        R.drawable.skill_void_3,
                        R.drawable.skill_void_4}, R.drawable.gif_void));

        heroes.add(new Hero(HeroContract.GEOMANCER_ID, "Geomancer", "Meepo", R.drawable.hero_meepo,
                new int[]{R.drawable.skill_geomancer_1,
                    R.drawable.skill_geomancer_2,
                    R.drawable.skill_geomancer_3,
                    R.drawable.skill_geomancer_4}, R.drawable.gif_geomancer));

        heroes.add(new Hero(HeroContract.MEDUSA_ID, "Medusa", "Gorgon", R.drawable.hero_meduza,
                new int[]{R.drawable.skill_medusa_1,
                    R.drawable.skill_medusa_2,
                    R.drawable.skill_medusa_3,
                    R.drawable.skill_medusa_4}, R.drawable.gif_medusa));

        heroes.add(new Hero(HeroContract.MORTRED_ID, "Mortred", "Phantom Assasin", R.drawable.hero_morta,
                new int[]{R.drawable.skill_mortra_0,
                    R.drawable.skill_mortra_1,
                    R.drawable.skill_mortra_2,
                    R.drawable.skill_mortra_3}, R.drawable.gif_mortred));

        heroes.add(new Hero(HeroContract.NEVERMORE_ID, "Shadow Fiend", "Nevermore", R.drawable.hero_never,
                new int[]{R.drawable.skill_never_1,
                    R.drawable.skill_never_2,
                    R.drawable.skill_never_3,
                    R.drawable.skill_never_4}, R.drawable.gif_shadow_fiend));

        heroes.add(new Hero(HeroContract.RAZORE_ID, "Razor", "Lighting Revenant", R.drawable.hero_razor,
                new int[]{R.drawable.skill_razor_1,
                    R.drawable.skill_razor_2,
                    R.drawable.skill_razor_3,
                    R.drawable.skill_razor_4}, R.drawable.gif_razor));

        heroes.add(new Hero(HeroContract.SLARK_ID, "Slark", "Murloc Nightcrawler", R.drawable.hero_slark,
                new int[]{R.drawable.skill_slark_0,
                    R.drawable.skill_slark_1,
                    R.drawable.skill_slark_2,
                    R.drawable.skill_slark_3}, R.drawable.gif_slark));

        heroes.add(new Hero(HeroContract.SOUL_KEEPER, "Soul Keeper", "Terrorblade", R.drawable.hero_teror,
                new int[]{R.drawable.skill_terror_0,
                    R.drawable.skill_terror_1,
                    R.drawable.skill_terror_2,
                    R.drawable.skill_terror_3}, R.drawable.gif_terrorblade));

        heroes.add(new Hero(HeroContract.SPECTER_ID, "Spectre", "Mercurial", R.drawable.hero_spectre,
                new int[]{R.drawable.skill_spectra_1,
                    R.drawable.skill_spectra_2,
                    R.drawable.skill_spectra_3,
                    R.drawable.skill_spectra_4}, R.drawable.gif_spectre));

        heroes.add(new Hero(HeroContract.ZET_ID, "Zet", "Arc Warden", R.drawable.hero_zet,
                new int[]{R.drawable.skill_zet_1,
                    R.drawable.skill_zet_2,
                    R.drawable.skill_zet_3,
                    R.drawable.skill_zet_4}, R.drawable.gif_zet));

        heroes.add(new Hero(HeroContract.AKASHA_ID, "Queen of Pain", "Akasha", R.drawable.hero_akasha,
                new int[]{R.drawable.skill_akasha_0,
                    R.drawable.skill_akasha_1,
                    R.drawable.skill_akasha_2,
                    R.drawable.skill_akasha_3}, R.drawable.gif_queen_of_pain));

        heroes.add(new Hero(HeroContract.ANCIENT_APPARATION_ID, "Ancient Apparition", "Kaldr", R.drawable.hero_aa,
                new int[]{R.drawable.skill_ancient_1,
                    R.drawable.skill_ancient_2,
                    R.drawable.skill_ancient_3,
                    R.drawable.skill_ancient_4}, R.drawable.gif_ancient_apparation));

        heroes.add(new Hero(HeroContract.ATROPOS_ID, "Bane Elemental", "Atropos", R.drawable.hero_atropos,
                new int[]{R.drawable.skill_atropos_0,
                    R.drawable.skill_atropos_1,
                    R.drawable.skill_atropos_2,
                    R.drawable.skill_atropos_3}, R.drawable.gif_bane));

        heroes.add(new Hero(HeroContract.KROBELUS_ID, "Death Prophet", "Krobelus", R.drawable.hero_krobelus,
                new int[]{R.drawable.skill_bansha_0,
                    R.drawable.skill_bansha_1,
                    R.drawable.skill_bansha_2,
                    R.drawable.skill_bansha_3}, R.drawable.gif_death_prophet));

        heroes.add(new Hero(HeroContract.BATRIDER_ID, "Batrider", "Jin'zakk", R.drawable.hero_br,
                new int[]{R.drawable.skill_bat_1,
                    R.drawable.skill_bat_2,
                    R.drawable.skill_bat_3,
                    R.drawable.skill_bat_4}, R.drawable.gif_batrider));

        heroes.add(new Hero(HeroContract.WARLOCK_ID, "Warlock", "Demnok Lannik", R.drawable.hero_wl,
                new int[]{R.drawable.skill_warlock_1,
                    R.drawable.skill_warlock_2,
                    R.drawable.skill_warlock_3,
                    R.drawable.skill_warlock_4}, R.drawable.gif_warlock));

        heroes.add(new Hero(HeroContract.VISAGE_ID, "Visage", "Necro'lic", R.drawable.hero_vizaj,
                new int[]{R.drawable.skill_visaj_1,
                    R.drawable.skill_visaj_2,
                    R.drawable.skill_visaj_3,
                    R.drawable.skill_visaj_4}, R.drawable.gif_visage));

        heroes.add(new Hero(HeroContract.WINTER_VIVERN_ID, "Winter Vivern", "Auroth", R.drawable.hero_winter_wyvern,
                new int[]{R.drawable.skill_vivern_1,
                    R.drawable.skill_vivern_2,
                    R.drawable.skill_vivern_3,
                    R.drawable.skill_vivern_4}, R.drawable.gif_winter_vivern));

        heroes.add(new Hero(HeroContract.WITCH_DOCTOR_ID, "Witch Doctor", "Vol'jin", R.drawable.hero_wd,
                new int[]{R.drawable.skill_witch_1,
                    R.drawable.skill_witch_2,
                    R.drawable.skill_witch_3,
                    R.drawable.skill_witch_4}, R.drawable.gif_witch_doctor));

        heroes.add(new Hero(HeroContract.DUZZLE_ID, "Dazzle", "The Shadow Priest", R.drawable.hero_dazle,
                new int[]{R.drawable.skill_dazzle_1,
                    R.drawable.skill_dazzle_2,
                    R.drawable.skill_dazzle_3,
                    R.drawable.skill_dazzle_4}, R.drawable.gif_shadow_priest));

        heroes.add(new Hero(HeroContract.DARK_SEER_ID, "Dark Seer", "Ish'kafel", R.drawable.hero_dark_seer,
                new int[]{R.drawable.skill_dark_seer_1,
                    R.drawable.skill_dark_seer_2,
                    R.drawable.skill_dark_seer_3,
                    R.drawable.skill_dark_seer_4}, R.drawable.gif_dark_seer));

        heroes.add(new Hero(HeroContract.DESTROYER_ID, "Obsidian Destroyer", "Harbringer", R.drawable.hero_od,
                new int[]{R.drawable.skill_destroyer_1,
                    R.drawable.skill_destroyer_2,
                    R.drawable.skill_destroyer_3,
                    R.drawable.skill_destroyer_4}, R.drawable.gif_obsidian));

        heroes.add(new Hero(HeroContract.INVOCKER_ID, "Invoker", "Kael", R.drawable.hero_invoker,
                new int[]{R.drawable.skill_invoker_1,
                    R.drawable.skill_invoker_2,
                    R.drawable.skill_invoker_3,
                    R.drawable.skill_invoker_4}, R.drawable.gif_invoker));

        heroes.add(new Hero(HeroContract.LESHRACK_ID, "Leshrac", "The Tormented Soul", R.drawable.hero_leshrak,
                new int[]{R.drawable.skill_leshrak_1,
                    R.drawable.skill_leshrak_2,
                    R.drawable.skill_leshrak_3,
                    R.drawable.skill_leshrak_4}, R.drawable.gif_leshrak));

        heroes.add(new Hero(HeroContract.LION_ID, "Lion", "Demon Witch", R.drawable.hero_lion,
                new int[]{R.drawable.skill_lion_1,
                    R.drawable.skill_lion_2,
                    R.drawable.skill_lion_3,
                    R.drawable.skill_lion_4}, R.drawable.gif_lion));

        heroes.add(new Hero(HeroContract.LICH_ID, "Lich", "Kel'Thuzad", R.drawable.hero_lich,
                new int[]{R.drawable.skill_lich_1,
                    R.drawable.skill_lich_2,
                    R.drawable.skill_lich_3,
                    R.drawable.skill_lich_4}, R.drawable.gif_lich));

        heroes.add(new Hero(HeroContract.NECKROLIT_ID, "Necrolyte", "Rotund'jere", R.drawable.hero_nekr,
                new int[]{R.drawable.skill_necro_1,
                    R.drawable.skill_necro_2,
                    R.drawable.skill_necro_3,
                    R.drawable.skill_necro_4}, R.drawable.gif_necrolyte));

        heroes.add(new Hero(HeroContract.PUGNA_ID, "Pugna", "Oblivion", R.drawable.hero_pugna,
                new int[]{R.drawable.skill_pugna_1,
                    R.drawable.skill_pugna_2,
                    R.drawable.skill_pugna_3,
                    R.drawable.skill_pugna_4}, R.drawable.gif_pugna));

        heroes.add(new Hero(HeroContract.SHADOW_DEMON_ID, "Shadow Demon", "Eredar", R.drawable.hero_eredar,
                new int[]{R.drawable.skill_shadow_demon_1,
                    R.drawable.skill_shadow_demon_2,
                    R.drawable.skill_shadow_demon_3,
                    R.drawable.skill_shadow_demon_4}, R.drawable.gif_eredar));

        heroes.add(new Hero(HeroContract.ENIGMA_ID, "Enigma", "Darchrow", R.drawable.hero_enigma,
                new int[]{R.drawable.skill_enigma_1,
                    R.drawable.skill_enigma_2,
                    R.drawable.skill_enigma_3,
                    R.drawable.skill_enigma_4}, R.drawable.gif_enigma));


        return heroes;

    }
}
