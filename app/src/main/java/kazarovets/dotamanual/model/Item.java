package kazarovets.dotamanual.model;

import java.util.ArrayList;

public class Item {
    private int id;
    private int pictureResource;
    private String name;
    private String russianName;
    private String costWithTotal;
    private ArrayList<String> mainProperties;
    private ArrayList<Item> needForRecipeItems;
    private Shop shop;
    private ArrayList<Item> usedInItems;
    private boolean needRecipe;


    public Item(int id, int pictureResource, String name, String russianName, String costWithTotal){
        this.id = id;
        this.name = name;
        this.russianName = russianName;
        this.costWithTotal = costWithTotal;
        this.pictureResource = pictureResource;

        this.mainProperties = new ArrayList<>();
        this.needForRecipeItems = new ArrayList<>();
        this.usedInItems = new ArrayList<>();
        this.needRecipe = false;
    }

    public Item(int id, int pictureResource, String name, String russianName, String costWithTotal, boolean needRecipe){
        this(id, pictureResource, name, russianName, costWithTotal);
        this.needRecipe = needRecipe;

    }

    public Item addProperty(String property){
        this.mainProperties.add(property);
        return this;
    }

    public Item addNeedForRecipeItem(int id){
        this.needForRecipeItems.add(ItemLab.getItemById(id));
        return this;
    }

    public Item addNeedForRecipeItem(Item item){
        this.needForRecipeItems.add(item);
        return this;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPictureResource() {
        return pictureResource;
    }

    public void setPictureResource(int pictureResource) {
        this.pictureResource = pictureResource;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRussianName() {
        return russianName;
    }

    public void setRussianName(String russianName) {
        this.russianName = russianName;
    }

    public String getCostWithTotal() {
        return costWithTotal;
    }

    public void setCostWithTotal(String costWithTotal) {
        this.costWithTotal = costWithTotal;
    }

    public ArrayList<String> getMainProperties() {
        return mainProperties;
    }

    public void setMainProperties(ArrayList<String> mainProperties) {
        this.mainProperties = mainProperties;
    }

    public ArrayList<Item> getNeedForRecipeItems() {
        return needForRecipeItems;
    }

    public void setNeedForRecipeItems(ArrayList<Item> needForRecipeItems) {
        this.needForRecipeItems = needForRecipeItems;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public void addUsedInItem(Item i){
        if(!this.usedInItems.contains(i)) {
            this.usedInItems.add(i);
        }
    }

    public ArrayList<Item> getUsedInItems() {
        return usedInItems;
    }

    public boolean isNeedRecipe() {
        return needRecipe;
    }
}
