package kazarovets.dotamanual.model;

import java.util.ArrayList;
import java.util.List;

public class Shop {
    private int id;
    private List<Item> items;
    private String name;
    private int radianceImageRes;
    private int diresImageRes;

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public Shop(String name, int radianceImageRes, int diresImageRes){
        this.name = name;
        this.radianceImageRes = radianceImageRes;
        this.diresImageRes = diresImageRes;
        items = new ArrayList<Item>();
    }

    public void addItem(Item item){
        items.add(item);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRadianceImageRes() {
        return radianceImageRes;
    }

    public void setRadianceImageRes(int radianceImageRes) {
        this.radianceImageRes = radianceImageRes;
    }

    public int getDiresImageRes() {
        return diresImageRes;
    }

    public void setDiresImageRes(int diresImageRes) {
        this.diresImageRes = diresImageRes;
    }
}
