package kazarovets.dotamanual.ui.activities.item;

import android.content.Intent;
import android.os.Bundle;

import kazarovets.dotamanual.R;
import kazarovets.dotamanual.model.Shop;
import kazarovets.dotamanual.ui.activities.drawer.NavigationDrawerActivity;
import kazarovets.dotamanual.ui.fragments.item.ShopsFragment;


public class ShopsActivity extends NavigationDrawerActivity implements ShopsFragment.ShopsCallback{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getFragmentManager().findFragmentById(R.id.content_frame) == null) {
            ShopsFragment shopsFragment = new ShopsFragment();
            shopsFragment.setArguments(getIntent().getExtras());
            getFragmentManager().beginTransaction()
                    .add(R.id.content_frame, shopsFragment)
                    .commit();
        }
    }


    @Override
    public void openShop(Shop shop) {
        Intent i = new Intent(this, ShopPagerActivity.class);
        i.putExtra("id", shop.getId());
        startActivity(i);
    }

}
