package kazarovets.dotamanual.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import kazarovets.dotamanual.R;
import kazarovets.dotamanual.db.HeroDbSkillsLab;
import kazarovets.dotamanual.model.HeroLab;
import kazarovets.dotamanual.model.ItemLab;


public class InitActivity extends Activity {

    private ProgressBar progressBar;
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        InitializeAsyncTask initTask = new InitializeAsyncTask();
        setContentView(R.layout.activity_init);
        progressBar = (ProgressBar)findViewById(R.id.progress_bar);
        initTask.execute();
    }

    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }


    public void hideProgressBar() {
        progressBar.setVisibility(View.INVISIBLE);
    }


    private void finishInit(){
        Intent i = new Intent(this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
    }

    class InitializeAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            showProgressBar();
        }

        @Override
        protected Void doInBackground(Void... params) {
            HeroLab.getHeroes();
            ItemLab.getItems();
            new HeroDbSkillsLab(getApplicationContext()).addHeroSkillsIfNotExist();

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            hideProgressBar();
            finishInit();
        }
    }
}
