package kazarovets.dotamanual.ui.activities.hero;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.astuetz.PagerSlidingTabStrip;

import kazarovets.dotamanual.R;
import kazarovets.dotamanual.model.HeroLab;
import kazarovets.dotamanual.model.Item;
import kazarovets.dotamanual.ui.activities.drawer.NavigationDrawerActivity;
import kazarovets.dotamanual.ui.activities.item.ItemActivity;
import kazarovets.dotamanual.ui.fragments.hero.HeroAbilitiesTabFragment;
import kazarovets.dotamanual.ui.fragments.hero.HeroDescTabFragment;
import kazarovets.dotamanual.ui.fragments.hero.HeroItemsTabFragment;


public class HeroActivity extends NavigationDrawerActivity implements HeroItemsTabFragment.HeroCallback {

    private FragmentTabHost mTabHost;
    ViewPager pager;
    PagerAdapter pagerAdapter;
    final int TABS_COUNT = 3;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = getLayoutInflater().inflate(R.layout.activity_pager_with_tabs, contentView, true);

        int id = getIntent().getExtras().getInt("id");


        pager = (ViewPager) rootView.findViewById(R.id.pager);
        pagerAdapter = new HeroFragmentPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            int previousState;
            int currentState = ViewPager.SCROLL_STATE_IDLE;

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                int currentPage = pager.getCurrentItem();
                if (currentPage == TABS_COUNT - 1 || currentPage == 0) {
                    previousState = currentState;
                    currentState = state;
                    if (previousState == ViewPager.SCROLL_STATE_DRAGGING && currentState == ViewPager.SCROLL_STATE_IDLE) {
                        pager.setCurrentItem(currentPage == 0 ? TABS_COUNT - 1 : 0);
                    }
                }
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }
        });

        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setViewPager(pager);

        getActionBar().setTitle(HeroLab.getHeroById(id).getFirstName());
    }


    @Override
    public void openItem(Item item) {
        Intent i = new Intent(this, ItemActivity.class);
        i.putExtra("id", item.getId());
        startActivity(i);
    }


    private class HeroFragmentPagerAdapter extends FragmentPagerAdapter {

        private String[] titles = {
                getString(R.string.hero_description),
                getString(R.string.skills),
                getString(R.string.item_build)};

        public HeroFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch(position){
                case 0:{
                    Fragment fragment = new HeroDescTabFragment();
                    fragment.setArguments(getIntent().getExtras());
                    return fragment;
                }
                case 1:{
                    Fragment fragment = new HeroAbilitiesTabFragment();
                    fragment.setArguments(getIntent().getExtras());
                    return fragment;
                }
                case 2:{
                    Fragment fragment = new HeroItemsTabFragment();
                    fragment.setArguments(getIntent().getExtras());
                    return fragment;
                }
            }
            return null;
        }

        @Override
        public int getCount() {
            return titles.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }

    }

}
