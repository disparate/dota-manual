package kazarovets.dotamanual.ui.activities.drawer;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import kazarovets.dotamanual.ui.activities.search.SearchActivity;
import kazarovets.dotamanual.ui.activities.hero.HeroesPagerActivity;
import kazarovets.dotamanual.R;
import kazarovets.dotamanual.ui.activities.item.ShopsActivity;

public class NavigationDrawerActivity extends SearchActivity {

    private List<DrawerItem> drawerItems;
    private DrawerLayout drawerLayout;
    private ListView drawerList;
    protected ViewGroup contentView;
    private ActionBarDrawerToggle drawerToggle;

    public final int ALL_HEROES = 0;
    public final int ALL_ITEMS = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_with_drawer);

        contentView = (ViewGroup)findViewById(R.id.content_frame);
        drawerItems = new ArrayList<DrawerItem>();
        drawerItems.add(new DrawerItem(getResources().getString(R.string.all_heroes), R.drawable.ic_person_white_18dp));
        drawerItems.add(new DrawerItem(getResources().getString(R.string.all_items), R.drawable.ic_book_white_18dp));

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, 0, 0) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        drawerLayout.setDrawerListener(drawerToggle);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        drawerList = (ListView) findViewById(R.id.navigation_drawer);

        drawerList.setAdapter(new DrawerAdapter(this,
                R.layout.drawer_item, drawerItems));

        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = null;
                switch (position) {
                    case ALL_HEROES: {
                        i = new Intent(getApplicationContext(), HeroesPagerActivity.class);
                        startActivity(i);
                        break;
                    }
                    
                    case ALL_ITEMS: {
                        i = new Intent(getApplicationContext(), ShopsActivity.class);
                        startActivity(i);
                        break;
                    }
                }
            }
        });
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
