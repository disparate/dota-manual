package kazarovets.dotamanual.ui.activities.hero;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.viewpagerindicator.CirclePageIndicator;

import kazarovets.dotamanual.R;
import kazarovets.dotamanual.model.Hero;
import kazarovets.dotamanual.ui.activities.drawer.NavigationDrawerActivity;
import kazarovets.dotamanual.ui.fragments.hero.HeroesCategoryFragment;


public class HeroesPagerActivity extends NavigationDrawerActivity implements HeroesCategoryFragment.HeroesCallback{

    final static int CATEGORIES_COUNT = 6;

    ViewPager pager;
    PagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = getLayoutInflater().inflate(R.layout.activity_view_pager, contentView, true);

        pager = (ViewPager) rootView.findViewById(R.id.pager);
        pagerAdapter = new HeroesFragmentPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            int previousState;
            int currentState = ViewPager.SCROLL_STATE_IDLE;
            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                int currentPage = pager.getCurrentItem();
                if (currentPage == CATEGORIES_COUNT - 1 || currentPage == 0) {
                    previousState = currentState;
                    currentState = state;
                    if (previousState == ViewPager.SCROLL_STATE_DRAGGING && currentState == ViewPager.SCROLL_STATE_IDLE) {
                        pager.setCurrentItem(currentPage == 0 ? CATEGORIES_COUNT - 1 : 0);
                    }
                }
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }
        });

        CirclePageIndicator indicator = (CirclePageIndicator)rootView.findViewById(R.id.indicator);
        indicator.setViewPager(pager);

    }

    @Override
    public void openHero(Hero hero) {
        Intent i = new Intent(this, HeroActivity.class);
        i.putExtra("id", hero.getId());
        startActivity(i);
    }

    private class HeroesFragmentPagerAdapter extends FragmentPagerAdapter {

        public HeroesFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return HeroesCategoryFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            return CATEGORIES_COUNT;
        }

    }
}
