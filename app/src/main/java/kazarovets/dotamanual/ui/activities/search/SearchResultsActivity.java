package kazarovets.dotamanual.ui.activities.search;

import android.app.SearchManager;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import kazarovets.dotamanual.R;
import kazarovets.dotamanual.model.Hero;
import kazarovets.dotamanual.model.HeroLab;
import kazarovets.dotamanual.model.Item;
import kazarovets.dotamanual.model.ItemLab;
import kazarovets.dotamanual.ui.activities.hero.HeroActivity;
import kazarovets.dotamanual.ui.activities.item.ItemActivity;
import kazarovets.dotamanual.ui.activities.drawer.NavigationDrawerActivity;

public class SearchResultsActivity extends NavigationDrawerActivity {

    private List<SearchResultsItem> results;
    private ViewGroup containerView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ViewGroup rootView = (ViewGroup)getLayoutInflater().inflate(R.layout.activity_search_results, contentView, true);
        containerView = (ViewGroup)rootView.findViewById(R.id.results_container);

        handleIntent(getIntent());
    }

    @Override
    public void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        results = new ArrayList<SearchResultsItem>();

        String search = intent.getStringExtra(SearchManager.QUERY);
        for (Hero h : HeroLab.getHeroes()) {
            int imageRes = h.getPictureResource();
            boolean isHero = true;
            if (match(h.getFirstName(), search)) {
                results.add(new SearchResultsItem(imageRes, h.getFirstName(), isHero, h.getId()));
            } else if(match(h.getSecondName(), search)) {
                results.add(new SearchResultsItem(imageRes, h.getSecondName(), isHero, h.getId()));
            }
        }

        for (Item i : ItemLab.getItems()) {
            int imageRes = i.getPictureResource();
            boolean isHero = false;
            if (match(i.getName(),search)) {
                results.add(new SearchResultsItem(imageRes, i.getName(), isHero, i.getId()));
            } else if(match(i.getRussianName(),search)) {
                results.add(new SearchResultsItem(imageRes, i.getRussianName(), isHero, i.getId()));
            }
        }

        addResultsToUi(results);

        if(results.size() == 0){
            View noResultsView = getLayoutInflater().inflate(R.layout.view_no_results_found, containerView, false);
            containerView.addView(noResultsView);
        }
    }

    void addResultsToUi(List<SearchResultsItem> results){

        int position = 0;
        for (final SearchResultsItem item : results){
            View itemView = getLayoutInflater().inflate(R.layout.view_search_result_item, containerView, false);
            TextView nameView = (TextView) itemView.findViewById(R.id.search_res_name);
            nameView.setText(item.getNameToShow());

            ImageView imageView = (ImageView) itemView.findViewById(R.id.search_res_icon);
            imageView.setImageResource(item.getImageRes());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setOnClickListener(item);
                }
            });

            containerView.addView(itemView);
            Point size = new Point();
            getWindowManager().getDefaultDisplay().getSize(size);
            TranslateAnimation animation = new TranslateAnimation(size.x + position*70 , 0, 50, 0);
            animation.setDuration(1000 + position*70);
            animation.setFillAfter(true);
            itemView.startAnimation(animation);

            position++;
        }
    }

    void setOnClickListener(SearchResultsItem item){
        Class type = null;
        if(item.isHero()){
            type = HeroActivity.class;
        } else {
            type = ItemActivity.class;
        }
        Intent i = new Intent(this, type);
        i.putExtra("id", item.getId());
        startActivity(i);
    }

    private boolean match(String string, String search){
        for (String s : string.toLowerCase().split(" ")) {
            if (s.startsWith(search.toLowerCase())) {
                return true;
            }
        }
        return false;
    }
}
