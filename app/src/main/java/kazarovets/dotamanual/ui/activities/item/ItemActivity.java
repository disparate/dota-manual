package kazarovets.dotamanual.ui.activities.item;

import android.content.Intent;
import android.os.Bundle;

import kazarovets.dotamanual.R;
import kazarovets.dotamanual.model.Item;
import kazarovets.dotamanual.model.Shop;
import kazarovets.dotamanual.ui.activities.drawer.NavigationDrawerActivity;
import kazarovets.dotamanual.ui.fragments.item.ItemFragment;

public class ItemActivity extends NavigationDrawerActivity implements ItemFragment.ItemCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getFragmentManager().findFragmentById(R.id.content_frame) == null) {
            ItemFragment itemFragment = new ItemFragment();
            itemFragment.setArguments(getIntent().getExtras());
            getFragmentManager().beginTransaction()
                    .add(R.id.content_frame, itemFragment)
                    .commit();
        }
    }


    @Override
    public void openItem(Item item) {
        Intent i = new Intent(this, ItemActivity.class);
        i.putExtra("id", item.getId());
        startActivity(i);
    }



    @Override
    public void setActionBarTitle(String newTitle){
        getActionBar().setTitle(newTitle);
    }

    @Override
    public void openShop(Shop shop) {
        Intent i = new Intent(this, ShopPagerActivity.class);
        i.putExtra("id", shop.getId());
        startActivity(i);
    }
}
