package kazarovets.dotamanual.ui.activities.item;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.viewpagerindicator.CirclePageIndicator;

import kazarovets.dotamanual.R;
import kazarovets.dotamanual.model.Item;
import kazarovets.dotamanual.model.ShopLab;
import kazarovets.dotamanual.ui.activities.drawer.NavigationDrawerActivity;
import kazarovets.dotamanual.ui.fragments.item.SingleShopFragment;


public class ShopPagerActivity extends NavigationDrawerActivity implements SingleShopFragment.ShopCallback {

    static final int SHOPS_COUNT = 11;

    ViewPager pager;
    PagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = getLayoutInflater().inflate(R.layout.activity_view_pager, contentView, true);

        pager = (ViewPager) rootView.findViewById(R.id.pager);
        pagerAdapter = new ShopFragmentPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    int previousState;
                    int currentState = ViewPager.SCROLL_STATE_IDLE;
                    @Override
                    public void onPageSelected(int position) {
                        getActionBar().setTitle(ShopLab.getShops().get(position).getName());
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {
                        int currentPage = pager.getCurrentItem();
                        if (currentPage == SHOPS_COUNT -1 || currentPage == 0) {
                            previousState = currentState;
                            currentState = state;
                            if (previousState == ViewPager.SCROLL_STATE_DRAGGING && currentState == ViewPager.SCROLL_STATE_IDLE) {
                                pager.setCurrentItem(currentPage == 0 ? SHOPS_COUNT - 1 : 0);
                            }
                        }
                    }

                    @Override
                    public void onPageScrolled(int arg0, float arg1, int arg2) {
                    }
                });

        CirclePageIndicator indicator = (CirclePageIndicator)rootView.findViewById(R.id.indicator);
        indicator.setViewPager(pager);

        pager.setCurrentItem(getIntent().getExtras().getInt("id"));
    }


    @Override
    public void openItem(Item item) {
        Intent i = new Intent(this, ItemActivity.class);
        i.putExtra("id", item.getId());
        startActivity(i);
    }


    private class ShopFragmentPagerAdapter extends FragmentPagerAdapter {

        public ShopFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return SingleShopFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            return SHOPS_COUNT;
        }

    }
}