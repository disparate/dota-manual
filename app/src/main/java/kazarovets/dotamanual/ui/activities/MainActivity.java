package kazarovets.dotamanual.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import kazarovets.dotamanual.R;
import kazarovets.dotamanual.ui.activities.drawer.NavigationDrawerActivity;
import kazarovets.dotamanual.ui.activities.hero.HeroesPagerActivity;
import kazarovets.dotamanual.ui.activities.item.ShopsActivity;


public class MainActivity extends NavigationDrawerActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View rootView = getLayoutInflater().inflate(R.layout.activity_main, contentView, true);


        View cardAllHeroesView = rootView.findViewById(R.id.all_heroes);
        View cardAllItemsView = rootView.findViewById(R.id.all_items);

        cardAllItemsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), ShopsActivity.class);
                startActivity(i);
            }
        });

        cardAllHeroesView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), HeroesPagerActivity.class);
                startActivity(i);
            }
        });

        ((ImageView)cardAllHeroesView.findViewById(R.id.card_image)).
                setImageResource(R.drawable.ic_hero_menu);
        ((ImageView)cardAllItemsView.findViewById(R.id.card_image)).
                setImageResource(R.drawable.ic_book_menu);

        ((TextView)cardAllHeroesView.findViewById(R.id.card_text)).
                setText(getResources().getString(R.string.all_heroes));
        ((TextView)cardAllItemsView.findViewById(R.id.card_text)).
                setText(getResources().getString(R.string.all_items));

    }
}
