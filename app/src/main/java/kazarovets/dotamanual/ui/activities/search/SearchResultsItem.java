package kazarovets.dotamanual.ui.activities.search;

/**
 * Created by user on 22.09.2015.
 */
public class SearchResultsItem {
    private int imageRes;
    private String nameToShow;
    private boolean isHero;
    private int id;

    public SearchResultsItem(int imageRes, String nameToShow, boolean isHero, int id){
        this.imageRes = imageRes;
        this.nameToShow = nameToShow;
        this.isHero = isHero;
        this.id = id;
    }

    public int getImageRes() {
        return imageRes;
    }

    public void setImageRes(int imageRes) {
        this.imageRes = imageRes;
    }

    public String getNameToShow() {
        return nameToShow;
    }

    public void setNameToShow(String nameToShow) {
        this.nameToShow = nameToShow;
    }

    public boolean isHero() {
        return isHero;
    }

    public void setIsHero(boolean isHero) {
        this.isHero = isHero;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}