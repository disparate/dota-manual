package kazarovets.dotamanual.ui.fragments.item;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import kazarovets.dotamanual.R;
import kazarovets.dotamanual.model.Shop;
import kazarovets.dotamanual.model.ShopLab;


public class ShopsFragment extends Fragment {
    private ShopsCallback callback;
    private ListView listView;

    @Override
    public void onAttach(Activity activity) {
        callback = (ShopsCallback) activity;
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        listView = (ListView) inflater.inflate(R.layout.fragment_all_shops, container, false);
        listView.setAdapter(new ShopsAdapter(getActivity(), R.layout.view_shop_passport, ShopLab.getShops()));
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
               updateViewScale(view, visibleItemCount);
            }
        });
        return listView;
    }

    private void updateViewScale(AbsListView view, int visibleCount) {
        int height = view.getHeight();

        for (int i = 0; i < visibleCount; i++) {
            float scale = 1f;
            View v = view.getChildAt(i);
            int viewTop = v.getTop();
            if (viewTop >= height - v.getHeight()) {
                int delta = viewTop - (height - v.getHeight());
                scale = (height - delta) / (float) height;
                scale = Math.max(scale, 0);
            }
            if(viewTop <= 0){
                scale = (height + viewTop) / (float) height;
                scale = Math.max(scale, 0);
            }

            v.setPivotX(v.getWidth() / 2);
            v.setPivotY(v.getHeight() / -2);
            v.setScaleX(scale);
            v.setScaleY(scale);
        }
    }

    public interface ShopsCallback {
        void openShop(Shop shop);
    }

    public class ShopsAdapter extends ArrayAdapter<Shop> {
        Context context;
        List<Shop> shopList;
        int layoutResID;

        public ShopsAdapter(Context context, int layoutResourceID,
                            List<Shop> listItems) {
            super(context, layoutResourceID, listItems);
            this.context = context;
            this.shopList = listItems;
            this.layoutResID = layoutResourceID;

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final Shop shop = this.shopList.get(position);
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            ShopHolder shopHolder;
            View view = convertView;

            if (view == null) {
                shopHolder = new ShopHolder();

                view = inflater.inflate(layoutResID, parent, false);
                shopHolder.shopName = (TextView) view
                        .findViewById(R.id.shop_name);
                shopHolder.icon_rad = (ImageView) view.findViewById(R.id.shop_rad_image);
                shopHolder.icon_dires = (ImageView) view.findViewById(R.id.shop_dire_image);

                view.setTag(shopHolder);

            } else {
                shopHolder = (ShopHolder) view.getTag();
            }

            shopHolder.icon_rad.setImageDrawable(
                    ContextCompat.getDrawable(getContext(), shop.getRadianceImageRes()));
            shopHolder.icon_dires.setImageDrawable(
                    ContextCompat.getDrawable(getContext(), shop.getDiresImageRes()));
            shopHolder.shopName.setText(shop.getName());

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.openShop(shop);
                }
            });

            return view;
        }

        private class ShopHolder {
            TextView shopName;
            ImageView icon_rad;
            ImageView icon_dires;
        }
    }
}
