package kazarovets.dotamanual.ui.fragments.item;

import android.app.Activity;
import android.app.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import kazarovets.dotamanual.R;
import kazarovets.dotamanual.model.Item;
import kazarovets.dotamanual.model.ItemLab;
import kazarovets.dotamanual.model.Shop;

public class ItemFragment extends Fragment {
    private ViewGroup rootView;
    private ViewGroup container;
    private Item item;
    private ItemCallback callback;
    private LayoutInflater inflater;

    @Override
    public void onAttach(Activity activity){
        callback = (ItemCallback)activity;
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.inflater = inflater;
        this.container = container;
        rootView = (ViewGroup) inflater.inflate(R.layout.fragment_item, container, false);
        int itemId = getArguments().getInt("id");
        item = ItemLab.getItemById(itemId);

        showShortDescOfItem();
        showShopsForItem();
        showPropertiesOfItem();
        showNeedForRecipeItems();
        showUsedInItems();

        return rootView;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        callback.setActionBarTitle(item.getName());
    }

    private void showShortDescOfItem(){
        ImageView itemImageView = (ImageView)rootView.findViewById(R.id.item_image);
        itemImageView.setImageResource(item.getPictureResource());

        TextView nameView = (TextView)rootView.findViewById(R.id.name);
        nameView.setText(item.getName());

        TextView russianNameView = (TextView)rootView.findViewById(R.id.russian_name);
        russianNameView.setText(item.getRussianName());

        TextView costView = (TextView)rootView.findViewById(R.id.cost);
        costView.setText(" " + item.getCostWithTotal());
    }

    private void showShopsForItem(){
        TextView shopNameView = (TextView) rootView.findViewById(R.id.shop_name);
        shopNameView.setText(item.getShop().getName());

        ImageView radianceShopView = (ImageView)rootView.findViewById(R.id.shop_rad_image);
        radianceShopView.setImageResource(item.getShop().getRadianceImageRes());

        ImageView diresShopView = (ImageView) rootView.findViewById(R.id.shop_dire_image);
        diresShopView.setImageResource(item.getShop().getDiresImageRes());

        View shopContainer = rootView.findViewById(R.id.shop_container);
        shopContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.openShop(item.getShop());
            }
        });
    }

    private void showPropertiesOfItem(){
        ArrayList<String> properties = item.getMainProperties();
        ViewGroup propertiesRoot = (ViewGroup)rootView.findViewById(R.id.properties_container);

        for(String p : properties){
            TextView propertyView = new TextView(getActivity());
            propertyView.setText("\u2022 " + p);
            propertiesRoot.addView(propertyView);
        }
    }

    private void showNeedForRecipeItems(){
        ArrayList<Item> needForRecipeItems = item.getNeedForRecipeItems();
        ViewGroup needForRecipeItemsContainerView = (ViewGroup)rootView.findViewById(R.id.need_for_recipe_items_container);
        if(needForRecipeItems.size() == 0){
            rootView.findViewById(R.id.need_for_recipe_items_layout).setVisibility(View.GONE);

        }

        for(Item i : needForRecipeItems){
            View shortDescOfItemView = getActivity().getLayoutInflater().inflate(R.layout.view_short_item_desc_compact, needForRecipeItemsContainerView, false);

            ((ImageView)shortDescOfItemView.findViewById(R.id.item_image)).setImageResource(i.getPictureResource());
            ((TextView)shortDescOfItemView.findViewById(R.id.name)).setText(i.getName());

            final Item itemForCallback = i;
            shortDescOfItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.openItem(itemForCallback);
                }
            });
            needForRecipeItemsContainerView.addView(shortDescOfItemView);
        }

        if(item.isNeedRecipe()){
            View shortDescOfItemView = getActivity().getLayoutInflater().inflate(R.layout.view_short_item_desc_compact, needForRecipeItemsContainerView, false);
            ((ImageView)shortDescOfItemView.findViewById(R.id.item_image)).setImageResource(R.drawable.recipe);
            ((TextView)shortDescOfItemView.findViewById(R.id.name)).setText(getString(R.string.recipe));
            needForRecipeItemsContainerView.addView(shortDescOfItemView);
        }
    }

    private void showUsedInItems(){
        ArrayList<Item> usedInItems = item.getUsedInItems();
        ViewGroup usedInItemsContainerView = (ViewGroup)rootView.findViewById(R.id.used_in_items_container);
        if(usedInItems.size() == 0){
            rootView.findViewById(R.id.used_in_items_layout).setVisibility(View.GONE);

        }

        for(Item i : usedInItems){
            View shortDescOfItemView = getActivity().getLayoutInflater().inflate(R.layout.view_short_item_desc_compact, usedInItemsContainerView, false);

            ((ImageView)shortDescOfItemView.findViewById(R.id.item_image)).setImageResource(i.getPictureResource());
            ((TextView)shortDescOfItemView.findViewById(R.id.name)).setText(i.getName());

            final Item itemForCallback = i;
            shortDescOfItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.openItem(itemForCallback);
                }
            });
            usedInItemsContainerView.addView(shortDescOfItemView);

        }
    }

    public interface ItemCallback{
        void openItem(Item item);
        void setActionBarTitle(String newTitle);
        void openShop(Shop shop);
    }
}
