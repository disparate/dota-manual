package kazarovets.dotamanual.ui.fragments.hero;

import android.app.ActionBar;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import kazarovets.dotamanual.R;
import kazarovets.dotamanual.model.Hero;
import kazarovets.dotamanual.model.HeroLab;
import kazarovets.dotamanual.model.Item;


public class HeroItemsTabFragment extends Fragment {
    private Hero hero;
    private View rootView;
    private HeroCallback callback;

    @Override
    public void onAttach(Activity activity){
        callback = (HeroCallback)activity;
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.view_item_build, container, false);
        int heroId = getArguments().getInt("id");
        hero = HeroLab.getHeroById(heroId);

        setItemsPics();

        return rootView;
    }


    private void setItemsPics(){
        View startItemsHost = rootView.findViewById(R.id.start_items);
        TextView startItemsCategoryView = (TextView)startItemsHost.findViewById(R.id.items_category_name);
        startItemsCategoryView.setText(R.string.start_items);
        GridLayout startItemsIcons = (GridLayout) startItemsHost.findViewById(R.id.items_icons);
        fillGridLayoutWithPics(startItemsIcons, hero.getStartItems());

        View primaryItemsHost = rootView.findViewById(R.id.primary_items);
        TextView primaryItemsCategoryView = (TextView)primaryItemsHost.findViewById(R.id.items_category_name);
        primaryItemsCategoryView.setText(R.string.primary_items);
        GridLayout primaryItemsIcons = (GridLayout) primaryItemsHost.findViewById(R.id.items_icons);
        fillGridLayoutWithPics(primaryItemsIcons, hero.getPrimaryItems());

        View mainItemsHost = rootView.findViewById(R.id.main_items);
        TextView mainItemsCategoryView = (TextView)mainItemsHost.findViewById(R.id.items_category_name);
        mainItemsCategoryView.setText(R.string.main_items);
        GridLayout mainItemsIcons = (GridLayout) mainItemsHost.findViewById(R.id.items_icons);
        fillGridLayoutWithPics(mainItemsIcons, hero.getMainItems());

        View situationalItemsHost = rootView.findViewById(R.id.situational_items);
        TextView situationalItemsCategoryView = (TextView)situationalItemsHost.findViewById(R.id.items_category_name);
        situationalItemsCategoryView.setText(R.string.situational_items);
        GridLayout situationalItemsIcons = (GridLayout) situationalItemsHost.findViewById(R.id.items_icons);
        fillGridLayoutWithPics(situationalItemsIcons, hero.getSituationalItems());
    }

    private void fillGridLayoutWithPics(GridLayout gl, ArrayList<Item> items){

        for(Item item : items){
            ImageView imageView = new ImageView(getActivity());
            imageView.setImageResource(item.getPictureResource());
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(100, 100);

            imageView.setLayoutParams(lp);
            final Item itemForCallback = item;
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.openItem(itemForCallback);
                }
            });

            gl.addView(imageView);
        }
    }

    public interface HeroCallback{
        void openItem(Item item);
    }
}