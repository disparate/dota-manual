package kazarovets.dotamanual.ui.fragments.hero;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import kazarovets.dotamanual.R;
import kazarovets.dotamanual.db.HeroSkills;
import kazarovets.dotamanual.db.HeroSkillsDbProvider;
import kazarovets.dotamanual.model.Hero;
import kazarovets.dotamanual.model.HeroLab;


public class HeroAbilitiesTabFragment  extends Fragment {
    private Hero hero;
    private View rootView;
    private HeroSkillsDbProvider provider;

    private ViewGroup[] skillGroups;

    private final int NUMBER_OF_HERO_SKILLS = 4;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.view_hero_skills, container, false);
        int heroId = getArguments().getInt("id");
        hero = HeroLab.getHeroById(heroId);

        skillGroups = new ViewGroup[4];
        skillGroups[0] = (ViewGroup)rootView.findViewById(R.id.skill1_group);
        skillGroups[1] = (ViewGroup)rootView.findViewById(R.id.skill2_group);
        skillGroups[2] = (ViewGroup)rootView.findViewById(R.id.skill3_group);
        skillGroups[3] = (ViewGroup)rootView.findViewById(R.id.skill4_group);

        provider = new HeroSkillsDbProvider(getActivity());

        setSkillsViews();


        return rootView;
    }


    private void setSkillsViews(){

        HeroSkills hs =  provider.readHeroSkills(hero.getId());
        List<String> skills = hs.getSkills();
        List<String> skillsNames = hs.getSkillsNames();

        for (int i = 0; i<NUMBER_OF_HERO_SKILLS; i++) {
            ((ImageView) skillGroups[i].findViewById(R.id.skill)).setImageResource(hero.getSkillsPictureIds()[i]);
            ((TextView) skillGroups[i].findViewById(R.id.skill_name)).setText(skillsNames.get(i));
            ((TextView) skillGroups[i].findViewById(R.id.skill_desc)).setText(skills.get(i));
        }
    }

}