package kazarovets.dotamanual.ui.fragments.hero;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageView;

import java.util.ArrayList;

import kazarovets.dotamanual.R;
import kazarovets.dotamanual.model.Hero;
import kazarovets.dotamanual.model.HeroLab;


public class HeroesCategoryFragment extends Fragment{

    static final String ARGUMENT_HEROES_NUMBER = "arg_heroes_number";

    public final static int[] HEROES_CATEGORIES = new int[]{R.drawable.sentinel_strength,
        R.drawable.sentinel_agility,
        R.drawable.sentinelintelligence,
        R.drawable.scourage_strength,
        R.drawable.scourage_agility,
        R.drawable.scourageintelligence};

    int heroesCategoryRes;
    private HeroesCallback callback;
    private ArrayList<Hero> heroes;

    public static HeroesCategoryFragment newInstance(int heroesNumber) {
        HeroesCategoryFragment heroesFragment = new HeroesCategoryFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(ARGUMENT_HEROES_NUMBER, heroesNumber);
        heroesFragment.setArguments(arguments);
        heroesFragment.heroesCategoryRes = HEROES_CATEGORIES[heroesNumber];
        return heroesFragment;
    }

    @Override
    public void onAttach(Activity activity){
        callback = (HeroesCallback)activity;
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int heroesNumber;
        heroesNumber = getArguments().getInt(ARGUMENT_HEROES_NUMBER);
        heroes = HeroLab.getHeroesByCategory(heroesNumber);
        heroesCategoryRes = HEROES_CATEGORIES[heroesNumber];

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_hero_category, null);

        ImageView categoryNameView = (ImageView) rootView.findViewById(R.id.category);
        categoryNameView.setImageResource(heroesCategoryRes);
        GridLayout gridView = (GridLayout) rootView.findViewById(R.id.heroes_container);
        for(Hero h: heroes) {
            ImageView heroView = new ImageView(getActivity());
            heroView.setImageResource(h.getPictureResource());
            final Hero heroForCallback = h;
            heroView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.openHero(heroForCallback);
                }
            });
            heroView.setPadding(2, 2, 2, 2);
            gridView.addView(heroView);
        }

        return rootView;
    }

        public interface HeroesCallback{
        void openHero(Hero hero);
    }
}
