package kazarovets.dotamanual.ui.fragments.item;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import kazarovets.dotamanual.R;
import kazarovets.dotamanual.model.Item;
import kazarovets.dotamanual.model.Shop;
import kazarovets.dotamanual.model.ShopLab;


public class SingleShopFragment extends Fragment {

    static final String ARGUMENT_SHOP_NUMBER = "arg_shop_number";

    int shopNumber;
    private Shop shop;
    private ShopCallback callback;

    public static SingleShopFragment newInstance(int shop) {
        SingleShopFragment shopFragment = new SingleShopFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(ARGUMENT_SHOP_NUMBER, shop);
        shopFragment.setArguments(arguments);
        return shopFragment;
    }

    @Override
    public void onAttach(Activity activity){
        callback = (ShopCallback)activity;
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        shopNumber = getArguments().getInt(ARGUMENT_SHOP_NUMBER);
        shop = ShopLab.getShops().get(shopNumber);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_single_shop, null);

        View shopView = rootView.findViewById(R.id.shop_passport);

        ImageView shopRadImageView = (ImageView) shopView.findViewById(R.id.shop_rad_image);
        shopRadImageView.setImageResource(shop.getRadianceImageRes());

        ImageView shopDireImageView = (ImageView) shopView.findViewById(R.id.shop_dire_image);
        shopDireImageView.setImageResource(shop.getDiresImageRes());

        TextView shopTextView = (TextView) shopView.findViewById(R.id.shop_name);
        shopTextView.setText(shop.getName());

        GridLayout gridView = (GridLayout) rootView.findViewById(R.id.items_container);
        for(Item i: shop.getItems()) {
            ImageView itemView = new ImageView(getActivity());
            itemView.setImageResource(i.getPictureResource());
            final Item itemForCallback = i;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.openItem(itemForCallback);
                }
            });
            itemView.setPadding(2, 2, 2, 2);

            gridView.addView(itemView);
        }

        return rootView;
    }

    public interface ShopCallback{
        void openItem(Item item);
    }

}
