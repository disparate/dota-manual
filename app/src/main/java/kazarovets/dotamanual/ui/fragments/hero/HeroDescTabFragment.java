package kazarovets.dotamanual.ui.fragments.hero;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import kazarovets.dotamanual.R;
import kazarovets.dotamanual.model.Hero;
import kazarovets.dotamanual.model.HeroLab;
import kazarovets.dotamanual.ui.GifView;


public class HeroDescTabFragment extends Fragment {
    private Hero hero;
    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.view_hero_description, container, false);
        int heroId = getArguments().getInt("id");
        hero = HeroLab.getHeroById(heroId);

        ImageView heroImageView = (ImageView)rootView.findViewById(R.id.hero_image);
        heroImageView.setImageResource(hero.getPictureResource());

        TextView firstNameView = (TextView)rootView.findViewById(R.id.first_name);
        firstNameView.setText(hero.getFirstName());

        TextView secondNameView = (TextView)rootView.findViewById(R.id.second_name);
        secondNameView.setText(hero.getSecondName());

        GifView gifView = (GifView)rootView.findViewById(R.id.gif);
        gifView.setGifResource(hero.getGifResource());


        return rootView;
    }

}