package kazarovets.dotamanual.contracts;


public class ItemContract {
    public final static int DIVINE_RAPIRE_ID = 1;
    public final static int MONKEY_KING_BAR_ID = 2;
    public final static int RADIANCE_ID = 3;
    public final static int BUTTERFLY_ID = 4;
    public final static int BURIZE_ID = 5;
    public final static int BASHER_ID = 6;
    public final static int BATTLE_FURY_ID = 7;
    public final static int ABYSSAL_BLADE_ID = 8;
    public final static int CRYSTALYS_ID = 9;
    public final static int ARMLET_OF_MORDIGGIAN_ID = 10;
    public final static int LOTHARS_EDGE_ID = 11;
    public final static int EHTHEREAL_BLADE_ID = 12;

    public final static int SANGE_AND_YASHA_ID = 13;
    public final static int SATANIC_ID = 14;
    public final static int MJOLNIR_ID = 15;
    public final static int EYE_OF_SCADI_ID = 16;
    public final static int SANGE_ID = 17;
    public final static int HELM_OF_THE_DOMINATOR_ID = 18;
    public final static int MAELSTORM_ID = 19;
    public final static int STYGIAN_DESOLATOR_ID = 20;
    public final static int YASHA_ID = 21;
    public final static int MASK_OF_MADNESS_ID = 22;
    public final static int DIFFUSAL_BLADE_ID = 23;
    public final static int HEAVENS_HALLBERD_ID = 24;

    public final static int ASSAULT_CUIRAS_ID = 25;
    public final static int HEART_OF_TARRASQUE_ID = 26;
    public final static int BLACK_KING_BAR_ID = 27;
    public final static int AEGIS_OF_IMMORTAL_ID = 28;
    public final static int SHIVAS_GUARD_ID = 29;
    public final static int BLOODSTONE_ID = 30;
    public final static int LINKENS_SPHERE_ID = 31;
    public final static int VANGUARD_ID = 32;
    public final static int BLADE_MAIL_ID = 33;
    public final static int SOUL_BOOSTER_ID = 34;
    public final static int HOOD_OF_DEFIANCE_ID = 35;
    public final static int MANTA_STYLE_ID = 36;

    public final static int GUINSOOS_SCYTHE_OF_VYSE_ID = 37;
    public final static int ORCHID_MALEVOLENCE_ID = 38;
    public final static int EULS_SCEPTER_OF_DIVINITY_ID = 39;
    public final static int FORCE_STAFF_ID = 40;
    public final static int DAGON_ID = 41;
    public final static int NECROMICON_ID = 42;
    public final static int AGHANIMS_SCEPTER_ID = 43;
    public final static int REFRESHER_ORB_ID = 44;
    public final static int VEIL_OF_DISCORD_ID = 45;
    public final static int ROD_OF_ATOS_ID = 46;

    public final static int MEKANSM_ID = 47;
    public final static int VLADMIRS_OFFERING_ID = 48;
    public final static int ARCANE_BOOTS_ID = 49;
    public final static int RING_OF_AQUILA_ID = 50;
    public final static int NETHERIZM_BUCKLER_ID = 51;
    public final static int RING_OF_BASILIUS_ID = 52;
    public final static int KHADGARS_PIPE_OF_INSIGHT_ID = 53;
    public final static int URN_OF_SHADOWS_ID = 54;
    public final static int HEADDRESS_OF_REJUVENATION_ID = 55;
    public final static int MEDALLION_OF_COURAGE_ID = 56;
    public final static int ANCIENT_JANGGO_OF_ENDURANCE_ID = 57;
    public final static int TRANQUIL_BOOTS_ID = 58;

    public final static int BOOTS_OF_TRAVEL_ID = 59;
    public final static int PHASE_BOOTS_ID = 60;
    public final static int POWER_THREADS_ID = 61;
    public final static int SOUL_RING_ID = 62;
    public final static int HAND_OF_MIDAS_ID = 63;
    public final static int OBLIVION_STAFF_ID = 64;
    public final static int PERSEVERANCE_ID = 65;
    public final static int POOR_MANS_SHIELD_ID = 66;
    public final static int BRACER_ID = 67;
    public final static int WRAITH_BAND_ID = 68;
    public final static int NULL_TALISMAN_ID = 69;
    public final static int MAGIC_WAND_ID = 70;

    public final static int GLOVES_OF_HASTE_ID = 71;
    public final static int MASK_OF_DEATH_ID = 72;
    public final static int RING_OF_REGENERATION_ID = 73;
    public final static int KELENS_DAGGER_OF_ESCAPE_ID = 74;
    public final static int SOBI_MASK_ID = 75;
    public final static int BOOTS_OF_SPEED_ID = 76;
    public final static int GEM_OF_TRUE_SIGHT_ID = 77;
    public final static int PLANESWALKERS_CLOAK_ID = 78;
    public final static int MAGIC_STICK_ID = 79;
    public final static int TALISMAN_OF_EVASION_ID = 80;
    public final static int GHOST_SCEPTER_ID = 81;
    public final static int SHADOW_AMULET_ID = 82;

    public final static int DEMON_EDGE_ID = 83;
    public final static int EAGLEHORN_ID = 84;
    public final static int MESSERSHMIDTS_REAVER_ID = 85;
    public final static int SACRED_RELIC_ID = 86;
    public final static int HYPERSTONE_ID = 87;
    public final static int RING_OF_HEALTH_ID = 88;
    public final static int VOID_STONE_ID = 89;
    public final static int MYSTIC_STAFF_ID = 90;
    public final static int ENERGY_BOOSTER_ID = 91;
    public final static int POINT_BOOSTER_ID = 92;
    public final static int VITALITY_BOOSTER_ID = 93;
    public final static int ORB_OF_VENOM_ID = 94;

    public final static int CLARITY_POTION_ID = 95;
    public final static int HEALING_SALVE_ID = 96;
    public final static int ANCIENT_TANGO_OF_ESSIFATION_ID = 97;
    public final static int EMPTY_BOTTLE_ID = 98;
    public final static int OBSERVER_WARDS_ID = 99;
    public final static int SENTRY_WARDS_ID = 100;
    public final static int DUST_OF_APPEARANCE_ID = 101;
    public final static int ANIMAL_COURIER_ID = 102;
    public final static int SCROLL_OF_TOWN_PORTAL_ID = 103;
    public final static int SMOKE_OF_DECEIT_ID = 104;
    public final static int FLYING_COURIER_ID = 105;

    public final static int BLADES_OF_ATTACK_ID = 106;
    public final static int BROADSWORD_ID = 107;
    public final static int QUARTERSTAFF_ID = 108;
    public final static int CLAYMORE_ID = 109;
    public final static int RING_OF_PROTECTION_ID = 110;
    public final static int STOUT_SHIELD_ID = 111;
    public final static int JAVELIN_ID = 112;
    public final static int MITHRIL_HAMMER_ID = 113;
    public final static int CHAINMAIL_ID = 114;
    public final static int HELM_OF_IRON_WILL_ID = 115;
    public final static int PLATE_MAIL_ID = 116;
    public final static int QUELLING_BLADE_ID = 117;

    public final static int GAUNTLETS_OF_OGRE_STRENGTH_ID = 118;
    public final static int SLIPPERS_OF_AGILITY_ID = 119;
    public final static int MANTLE_OF_INTELLIGENCE_ID = 120;
    public final static int IRONWOOD_BRANCH_ID = 121;
    public final static int BELT_OF_GIANT_STRENGTH_ID = 122;
    public final static int BOOTS_OF_ELVENSKIN_ID = 123;
    public final static int ROBE_OF_THE_MAGI_ID = 124;
    public final static int CIRCLET_OF_NOBILITY_ID = 125;
    public final static int OGRE_AXE_ID = 126;
    public final static int BLADE_OF_ALACRITY_ID = 127;
    public final static int STAFF_OF_WIZARDY_ID = 128;
    public final static int ULTIMATE_ORB_ID = 129;

}
