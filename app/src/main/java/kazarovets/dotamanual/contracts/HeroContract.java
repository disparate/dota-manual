package kazarovets.dotamanual.contracts;


public class HeroContract {

    public final static int ALCHEMIC_ID = 0;
    public final static int BRISTLEBACK_ID = 1;
    public final static int BREWMASTER_ID = 2;
    public final static int WISP_ID = 3;
    public final static int TREANT_PROTECTOR_ID = 4;
    public final static int DRAGON_KNIGHT_ID = 5;
    public final static int CAOLIN_ID = 6;
    public final static int CENTAUR_ID = 7;
    public final static int CLOCKWERK_ID = 8;
    public final static int CUNKA_ID = 9;
    public final static int LEGION_COMMANDER_ID = 10;
    public final static int OMNIKNIGHT_ID = 11;
    public final static int REZAK_ID = 12;
    public final static int REXAR_ID = 13;
    public final static int SVEN_ID = 14;
    public final static int TAUREN_ID = 15;
    public final static int TINI_ID = 16;
    public final static int TUSKAR_ID = 17;
    public final static int PHOENIX_ID = 18;
    public final static int HUSCKAR_ID = 19;
    public final static int SHEIKER_ID = 20;

    public final static int BOUNTY_HUNTER_ID = 21;
    public final static int VENGEFUL_SPIRIT_ID = 22;
    public final static int GHYROCOPTER_ID = 23;
    public final static int JAGGERNAUT_ID = 24;
    public final static int EMBER_SPIRIT_ID = 25;
    public final static int LANAYA_ID = 26;
    public final static int SYLLABEAR_ID = 27;
    public final static int LUNA_ID = 28;
    public final static int MAGINA_ID = 29;
    public final static int MIRANA_ID = 30;
    public final static int MORPHLING_ID = 31;
    public final static int NAGA_ID = 32;
    public final static int RIKIMARU_ID = 33;
    public final static int SNIPER_ID = 34;
    public final static int TRAKSES_ID = 35;
    public final static int TROLL_ID = 36;
    public final static int URSA_ID = 37;
    public final static int PHANTOM_LANCER_ID = 38;

    public final static int WINDRUNNER_ID = 39;
    public final static int JAKIRO_ID = 40;
    public final static int DRAGONUS_ID = 41;
    public final static int ZEUS_ID = 42;
    public final static int LINA_ID = 43;
    public final static int MINER_ID = 44;
    public final static int ORACKLE_ID = 45;
    public final static int OGRE_ID = 46;
    public final static int PUCK_ID = 47;
    public final static int RHASTA_ID = 48;
    public final static int RUBICK_ID = 49;
    public final static int RULAY_ID = 50;
    public final static int SILENCER_ID = 51;
    public final static int TINKER_ID = 52;
    public final static int TRALL_ID = 53;
    public final static int PHURION_ID = 54;
    public final static int CHEN_ID = 55;
    public final static int SHTORM_SPIRIT_ID = 56;
    public final static int EZALOR_ID = 57;
    public final static int ENCHANTRES_ID = 58;

    public final static int ABBADON_ID = 59;
    public final static int AXE_ID = 60;
    public final static int BALANAR_ID = 61;
    public final static int BARATHRUM_ID = 62;
    public final static int WOLF_ID = 63;
    public final static int DOOM_ID = 64;
    public final static int ZOMBI_ID = 65;
    public final static int LEORIK_ID = 66;
    public final static int MAGNUS_ID = 67;
    public final static int NIXE_ID = 68;
    public final static int PIT_LORD_ID = 69;
    public final static int PUDGE_ID = 70;
    public final static int SLARDAR_ID = 71;
    public final static int SCORPION_ID = 72;
    public final static int TIDEHUNTER_ID = 73;
    public final static int CHAOS_KNIGHT = 74;

    public final static int NERUBIAN_ID = 75;
    public final static int BLOODSEECKER_ID = 76;
    public final static int BONE_PHLETCHER_ID = 77;
    public final static int BROODMOTHER_ID = 78;
    public final static int VIPER_ID = 79;
    public final static int VENOMANCER_ID = 80;
    public final static int VIVER_ID = 81;
    public final static int VOID_ID = 82;
    public final static int GEOMANCER_ID = 83;
    public final static int MEDUSA_ID = 84;
    public final static int MORTRED_ID = 85;
    public final static int NEVERMORE_ID = 86;
    public final static int RAZORE_ID = 87;
    public final static int SLARK_ID = 88;
    public final static int SOUL_KEEPER = 89;
    public final static int SPECTER_ID = 90;
    public final static int ZET_ID = 91;

    public final static int AKASHA_ID = 92;
    public final static int ANCIENT_APPARATION_ID = 93;
    public final static int ATROPOS_ID = 94;
    public final static int KROBELUS_ID = 95;
    public final static int BATRIDER_ID = 96;
    public final static int WARLOCK_ID = 97;
    public final static int VISAGE_ID = 98;
    public final static int WINTER_VIVERN_ID = 99;
    public final static int WITCH_DOCTOR_ID = 100;
    public final static int DUZZLE_ID = 101;
    public final static int DARK_SEER_ID = 102;
    public final static int DESTROYER_ID = 103;
    public final static int INVOCKER_ID = 104;
    public final static int LESHRACK_ID = 105;
    public final static int LION_ID = 106;
    public final static int LICH_ID = 107;
    public final static int NECKROLIT_ID = 108;
    public final static int PUGNA_ID = 109;
    public final static int SHADOW_DEMON_ID = 110;
    public final static int ENIGMA_ID = 111;

    public final static int LAST_HERO_ID = ENIGMA_ID;

    public final static int FIRST_SENTINEL_STRENGTH = ALCHEMIC_ID;
    public final static int LAST_SENTINEL_STRENGTH = SHEIKER_ID;

    public final static int FIRST_SENTINEL_AGILITY = BOUNTY_HUNTER_ID;
    public final static int LAST_SENTINEL_AGILITY = PHANTOM_LANCER_ID;

    public final static int FIRST_SENTINEL_INTELLIGENCE = WINDRUNNER_ID;
    public final static int LAST_SENTINEL_INTELLIGENCE = ENCHANTRES_ID;

    public final static int FIRST_SCOURGE_STRENGTH = ABBADON_ID;
    public final static int LAST_SCOURGE_STRENGTH = CHAOS_KNIGHT;

    public final static int FIRST_SCOURGE_AGILITY = NERUBIAN_ID;
    public final static int LAST_SCOURGE_AGILITY = ZET_ID;

    public final static int FIRST_SCOURGE_INTELLIGENCE = AKASHA_ID;
    public final static int LAST_SCOURGE_INTELLIGENCE = ENIGMA_ID;


    public  static final int SENTINEL_STRENGTH = 0;
    public  static final int SENTINEL_AGILITY = 1;
    public  static final int SENTINEL_INTELLIGENCE = 2;
    public  static final int SCOURGE_STRENGTH = 3;
    public  static final int SCOURGE_AGILITY = 4;
    public  static final int SCOURGE_INTELLIGENCE = 5;

}
