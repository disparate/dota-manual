package kazarovets.dotamanual.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;


public class HeroSkillsDbProvider {

    private HeroDbHelper heroDbHelper;
    private Context context;

    public HeroSkillsDbProvider(Context context){
        this.context = context;
        heroDbHelper = new HeroDbHelper(context);
    }

    public void addHeroSkills(int heroId, HeroSkills heroSkills){
        SQLiteDatabase writeableDB = heroDbHelper.getWritableDatabase();

        List<String> skills = heroSkills.getSkills();
        List<String> skillsNames = heroSkills.getSkillsNames();
        ContentValues contentValues = new ContentValues();
        contentValues.put(HeroDbContract.COLUMN_NAME_HERO_ID, heroId);
        contentValues.put(HeroDbContract.COLUMN_NAME_SKILL_1, skills.get(0));
        contentValues.put(HeroDbContract.COLUMN_NAME_SKILL_1_NAME, skillsNames.get(0));
        contentValues.put(HeroDbContract.COLUMN_NAME_SKILL_2, skills.get(1));
        contentValues.put(HeroDbContract.COLUMN_NAME_SKILL_2_NAME, skillsNames.get(1));
        contentValues.put(HeroDbContract.COLUMN_NAME_SKILL_3, skills.get(2));
        contentValues.put(HeroDbContract.COLUMN_NAME_SKILL_3_NAME, skillsNames.get(2));
        contentValues.put(HeroDbContract.COLUMN_NAME_SKILL_4, skills.get(3));
        contentValues.put(HeroDbContract.COLUMN_NAME_SKILL_4_NAME, skillsNames.get(3));

        writeableDB.insertWithOnConflict(HeroDbContract.TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
        writeableDB.close();
    }

    public HeroSkills readHeroSkills(int heroId){
        SQLiteDatabase db = heroDbHelper.getReadableDatabase();

        String[] projection = {
                HeroDbContract.COLUMN_NAME_SKILL_1,
                HeroDbContract.COLUMN_NAME_SKILL_1_NAME,
                HeroDbContract.COLUMN_NAME_SKILL_2,
                HeroDbContract.COLUMN_NAME_SKILL_2_NAME,
                HeroDbContract.COLUMN_NAME_SKILL_3,
                HeroDbContract.COLUMN_NAME_SKILL_3_NAME,
                HeroDbContract.COLUMN_NAME_SKILL_4,
                HeroDbContract.COLUMN_NAME_SKILL_4_NAME
        };

        String selection = HeroDbContract.COLUMN_NAME_HERO_ID + "=?";

        String[] selectionArgs = {String.valueOf(heroId)};

        Cursor c = db.query(
                HeroDbContract.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        if(c.getCount() == 0)
            return null;

        c.moveToFirst();

        List<String> skillsNames  = new ArrayList<>();
        List<String> skills  = new ArrayList<>();

        skills.add(c.getString(c.getColumnIndex(HeroDbContract.COLUMN_NAME_SKILL_1)));
        skillsNames.add(c.getString(c.getColumnIndex(HeroDbContract.COLUMN_NAME_SKILL_1_NAME)));
        skills.add(c.getString(c.getColumnIndex(HeroDbContract.COLUMN_NAME_SKILL_2)));
        skillsNames.add(c.getString(c.getColumnIndex(HeroDbContract.COLUMN_NAME_SKILL_2_NAME)));
        skills.add(c.getString(c.getColumnIndex(HeroDbContract.COLUMN_NAME_SKILL_3)));
        skillsNames.add(c.getString(c.getColumnIndex(HeroDbContract.COLUMN_NAME_SKILL_3_NAME)));
        skills.add(c.getString(c.getColumnIndex(HeroDbContract.COLUMN_NAME_SKILL_4)));
        skillsNames.add(c.getString(c.getColumnIndex(HeroDbContract.COLUMN_NAME_SKILL_4_NAME)));

        HeroSkills heroSkills = new HeroSkills(skills, skillsNames);
        db.close();
        return heroSkills;
    }
}
