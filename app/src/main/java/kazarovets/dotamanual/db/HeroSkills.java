package kazarovets.dotamanual.db;

import java.util.ArrayList;
import java.util.List;


public class HeroSkills {
    private List<String> skills;
    private List<String> skillsNames;

    public HeroSkills() {
        this.skills = new ArrayList<>();
        this.skillsNames = new ArrayList<>();
    }

    public HeroSkills(List<String> skills, List<String> skillsNames) {
        this.skills = skills;
        this.skillsNames = skillsNames;
    }

    public List<String> getSkills() {
        return skills;
    }

    public void setSkills(List<String> skills) {
        this.skills = skills;
    }

    public List<String> getSkillsNames() {
        return skillsNames;
    }

    public void setSkillsNames(List<String> skillsNames) {
        this.skillsNames = skillsNames;
    }

    public HeroSkills addSkill(String skillName, String skill){
        this.skills.add(skill);
        this.skillsNames.add(skillName);
        return this;
    }
}
