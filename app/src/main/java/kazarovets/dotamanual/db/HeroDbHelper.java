package kazarovets.dotamanual.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class HeroDbHelper  extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "DotaManual.db";

    private static final String TEXT_TYPE = " TEXT";
    private static final String SEPARATOR = ",";
    private static final String SQL_CREATE_DB =
            "CREATE TABLE " + HeroDbContract.TABLE_NAME + " (" +
                    HeroDbContract.COLUMN_NAME_HERO_ID + " INTEGER PRIMARY KEY," +
                    HeroDbContract.COLUMN_NAME_SKILL_1 + TEXT_TYPE + SEPARATOR +
                    HeroDbContract.COLUMN_NAME_SKILL_1_NAME + TEXT_TYPE + SEPARATOR +
                    HeroDbContract.COLUMN_NAME_SKILL_2 + TEXT_TYPE + SEPARATOR +
                    HeroDbContract.COLUMN_NAME_SKILL_2_NAME + TEXT_TYPE + SEPARATOR +
                    HeroDbContract.COLUMN_NAME_SKILL_3 + TEXT_TYPE + SEPARATOR +
                    HeroDbContract.COLUMN_NAME_SKILL_3_NAME + TEXT_TYPE + SEPARATOR +
                    HeroDbContract.COLUMN_NAME_SKILL_4 + TEXT_TYPE + SEPARATOR +
                    HeroDbContract.COLUMN_NAME_SKILL_4_NAME + " )";

    private static final String SQL_DELETE_HEROES =
            "DROP TABLE IF EXISTS " + HeroDbContract.TABLE_NAME;

    public HeroDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_DB);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_HEROES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}