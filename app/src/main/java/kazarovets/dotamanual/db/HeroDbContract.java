package kazarovets.dotamanual.db;


public class HeroDbContract {
    public static final String TABLE_NAME = "Heroes";
    public static final String COLUMN_NAME_HERO_ID = "heroId";
    public static final String COLUMN_NAME_SKILL_1 = "skill_1";
    public static final String COLUMN_NAME_SKILL_1_NAME = "skill_1_name";
    public static final String COLUMN_NAME_SKILL_2 = "skill_2";
    public static final String COLUMN_NAME_SKILL_2_NAME = "skill_2_name";
    public static final String COLUMN_NAME_SKILL_3 = "skill_3";
    public static final String COLUMN_NAME_SKILL_3_NAME = "skill_3_name";
    public static final String COLUMN_NAME_SKILL_4 = "skill_4";
    public static final String COLUMN_NAME_SKILL_4_NAME = "skill_4_name";


}
